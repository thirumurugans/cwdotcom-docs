﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using OfficeOpenXml;
using System.Configuration;
using PrivateEquityReportCreation.ErrorLog;
using CwAzureApplications.BAL.Emails;

namespace CwAzureApplications.BAL
{
    public class UmbTrialBalDataRawExcel
    {
        private static readonly string connectionStringAzure = ConfigurationManager.AppSettings["ConnectionString"];
        private readonly static string excelPath = ConfigurationManager.AppSettings["UmDataFilePath"];
        private readonly static string excelPathCELF = ConfigurationManager.AppSettings["CelfUmDataFilePath"];
        private readonly static string excelPathLocal = ConfigurationManager.AppSettings["UmDataFilePathLocal"];
        private readonly static string environment = ConfigurationManager.AppSettings["Environment"];
        //static string excelPath = @"D:\Umb\";
        static List<Tuple<string, int, int>> memoRowDetails = new List<Tuple<string, int, int>>();//1-> memo heading reference 2-> memo heading row, 3-> memo Sum row
        static string UmbDate = "05/29/2020";
        static LogManager _logManagerEvent = new LogManager("Event");
        static LogManager _logError = new LogManager("Error");
        public static void GenerateExcelwithUmbTrialBalData()
        {
            try
            {
                string fileSubFolder = string.Empty;
                string fileSubFoldercelf = string.Empty;
                string exportedFunds = string.Empty;
                string reportFileName = string.Empty;
                string reportLogName = string.Empty;
                string celfReportFileName = string.Empty;
                string celfReportLogName = string.Empty;
                using (var connection = new SqlConnection(connectionStringAzure))
                {
                    var activeFunds = connection.Query<string>("SELECT ShortFundName FROM umbTrialBalanceFundsScrapeConfig WHERE IsActive=1", commandTimeout: 19000).AsList();
                    var checkToExport = connection.Query<UmbDataImport>("SELECT * FROM v_CCLF_UploadUmbTrialBalData ORDER BY UploadId DESC", commandTimeout: 19000).AsList();
                    if (checkToExport.Count > 0)
                    {
                        _logManagerEvent.Splitter();
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "UMB Data Export process started..."));
                    }
                    foreach (var check in checkToExport.Select(x => x.Date).Distinct())
                    {
                        try
                        {

                            UmbDate = check.Date.ToShortDateString();
                            //int days = -1;
                            //if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
                            //    days = -3;
                            //else if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
                            //    days = -2;
                            //else if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
                            //    days = -1;

                            //string fileSubFolder = excelPath + DateTime.Now.AddDays(days).Year + "\\" + DateTime.Now.AddDays(days).ToString("MMM") + "\\Data\\";
                            //string fileName = DateTime.Now.AddDays(days).ToString("yyyyMMdd");

                            /* CCLF Schedule Position */
                            fileSubFolder = excelPath + check.Date.Year + "\\" + check.Date.ToString("MMMM") + "\\data\\";
                            string fileName = check.Date.ToString("yyyyMMdd");

                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "UMB Trial Balance Data Export process in the Folder of " + fileSubFolder));
                            ExportToCsv(fileSubFolder, fileName + " TrialBalance.txt", "vCCLF_TrialBalance");
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "UMB Trial Balance Data Export to Text file completed, The filename is " + fileName + ".txt"));
                            exportedFunds = UmbDataExportToExcel(fileSubFolder, fileName + " TrialBalance.xlsx", connection, "Trial Balance", "vCCLF_TrialBalance");
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "UMB Trial Balance Data Export to Excel file completed, The filename is " + fileName + ".xls"));
                            reportFileName = fileName + ".xlsx";
                            reportLogName = fileName + ".txt";

                            /* CELF Trial Balance */
                            fileSubFoldercelf = excelPathCELF + check.Date.Year + "\\" + check.Date.ToString("MM") + " - " + check.Date.ToString("MMMM") + "\\data\\";
                            string fileNamecelf = check.Date.ToString("yyyyMMdd") + " CELF";

                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "UMB CELF Trial Balance Data Export process in the Folder of " + fileSubFoldercelf));
                            ExportToCsv(fileSubFoldercelf, fileNamecelf + " TrialBalance.txt", "vCELF_TrialBalance");
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "UMB CELF Trial Balance Data Export to Text file completed, The filename is " + fileNamecelf + ".txt"));
                            exportedFunds = exportedFunds + "," + UmbDataExportToExcel(fileSubFoldercelf, fileNamecelf + " TrialBalance.xlsx", connection, "CELF Trial Balance", "vCELF_TrialBalance");
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "UMB CELF Trial Balance Data Export to Excel file completed, The filename is " + fileNamecelf + ".xls"));
                            exportedFunds = exportedFunds.TrimEnd(',');

                            celfReportFileName = fileNamecelf + ".xlsx";
                            celfReportLogName = fileNamecelf + ".txt";

                        }
                        catch (Exception ex)
                        {
                            _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                            _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-UMB CCLF & CELF Data Export process failed for the date " + check.Date.ToShortDateString()));
                        }
                    }
                    foreach (var check in checkToExport.Select(x => x.Date).Distinct())
                    {
                        connection.Query<UmbDataImport>("UPDATE CCLF_UploadUmbTrialBalData SET YetToExport=1 WHERE [Date]=@Date", new { check.Date }, commandTimeout: 19000).AsList();
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "UMB CCLF & CELF Data Export process complete status Update, The Date is " + check.Date.ToShortDateString()));
                    }

                    if (checkToExport.Count > 0)
                    {
                        var exportFund = exportedFunds.Split(',').ToList();
                        var queryMissingFunds = (from l1 in activeFunds
                                                 join l2 in exportFund on l1.ToLower().Trim() equals l2.ToLower().Trim()
                                                 into leftJ
                                                 from lj in leftJ.DefaultIfEmpty()
                                                 select new { Key = l1, Value = lj })
                                                  .Where(x => x.Value == null)
                                                  .Select(x => x.Key)
                                                  .ToList();
                        EmailMessages emailMessage = new EmailMessages();
                        emailMessage.Subject = (environment != null && environment.ToLower() == "qa" ? "QA - " : "") + "CCLF & CELF UMB Trial Balance Upload Status";
                        emailMessage.IsBodyHtml = true;
                        StringBuilder builder = new StringBuilder();
                        builder.Append("<!DOCTYPE html>");
                        builder.Append("<head></head><body>");
                        builder.Append("<p>Hi Team,</p>");
                        builder.Append("<p>I have generated the UMB Trial Balance Excel files for <span>" + checkToExport[0].Date.ToShortDateString() + "</span>,</p>");
                        builder.Append("<p>" + fileSubFolder + "</p>");
                        builder.Append("<p>" + fileSubFoldercelf + "</p>");
                        //builder.Append("<p>Please check the status <a href='https://cliffwater.com/Admin#CCLFUploadExcel'>https://cliffwater.com/Admin#CCLFUploadExcel</a></p></br>");
                        if (queryMissingFunds.Count > 0)
                            builder.Append("<p><span style='background-color:rgb(255,217,102)'>Note: The following funds excel(Trial Balance) are not available in UMB Site - <b>" + string.Join(",", queryMissingFunds.ToArray()) + "</b></span></p>");
                        builder.Append("<p>Please check the status, https://www.cliffwater.com/Admin#CCLFUploadTrialBalRpt</p></br>");

                        builder.Append("</body></html>");
                        emailMessage.Body = builder.ToString();
                        //add attachments if present

                        if (!string.IsNullOrEmpty(reportFileName) && File.Exists(fileSubFolder + reportFileName))
                        {
                            Byte[] bytes = File.ReadAllBytes(fileSubFolder + reportFileName);
                            string fileContent = Convert.ToBase64String(bytes);
                            emailMessage.AddAttachment(reportFileName, fileContent);
                        }
                        if (!string.IsNullOrEmpty(reportLogName) && File.Exists(fileSubFolder + reportLogName))
                        {
                            Byte[] bytes = File.ReadAllBytes(fileSubFolder + reportLogName);
                            string fileContent = Convert.ToBase64String(bytes);
                            emailMessage.AddAttachment(reportLogName, fileContent);
                        }
                        if (!string.IsNullOrEmpty(celfReportFileName) && File.Exists(fileSubFoldercelf + celfReportFileName))
                        {
                            Byte[] bytes = File.ReadAllBytes(fileSubFoldercelf + celfReportFileName);
                            string fileContent = Convert.ToBase64String(bytes);
                            emailMessage.AddAttachment(celfReportFileName, fileContent);
                        }
                        if (!string.IsNullOrEmpty(celfReportLogName) && File.Exists(fileSubFoldercelf + celfReportLogName))
                        {
                            Byte[] bytes = File.ReadAllBytes(fileSubFoldercelf + celfReportLogName);
                            string fileContent = Convert.ToBase64String(bytes);
                            emailMessage.AddAttachment(celfReportLogName, fileContent);
                        }
                        EmailSend.EmailTrigger(emailMessage);
                    }

                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-UMB Data Export process Finished!"));
            }
        }
        private static string UmbDataExportToExcel(string baseFolder, string fileName, SqlConnection connection, string sheetName, string viewSql)
        {

            FileInfo excelFile = CreateFolderForUmbExcel(baseFolder, fileName);

            //string fileName = Convert.ToDateTime(date).ToString("yyyyMMdd TrialBalance") + ".xlsx";
            var resultAll = connection.Query<UmbTrialBalance>("SELECT FUND,ACNO,ACCTDESC,OPEN_BAL,DEBITS,CREDITS,END_BAL FROM " + viewSql + " WHERE AsOfDate=@Date", new { Date = Convert.ToDateTime(UmbDate) }, commandTimeout: 19000).AsList();
            using (ExcelPackage package = new ExcelPackage(excelFile))
            {
                var ws_InvestmentSummary = package.Workbook.Worksheets.Add(sheetName);
                ws_InvestmentSummary.Cells["A1"].Value = "FUND";
                ws_InvestmentSummary.Cells["B1"].Value = "ACNO";
                ws_InvestmentSummary.Cells["C1"].Value = "ACCTDESC";
                ws_InvestmentSummary.Cells["D1"].Value = "OPEN_BAL";
                ws_InvestmentSummary.Cells["E1"].Value = "DEBITS";
                ws_InvestmentSummary.Cells["F1"].Value = "CREDITS";
                ws_InvestmentSummary.Cells["G1"].Value = "END_BAL";
                int row = 2;
                foreach (var list in resultAll)
                {
                    ws_InvestmentSummary.Cells["A" + row].Value = list.FUND;
                    ws_InvestmentSummary.Cells["B" + row].Value = list.ACNO;
                    ws_InvestmentSummary.Cells["C" + row].Value = list.ACCTDESC;
                    ws_InvestmentSummary.Cells["D" + row].Value = list.OPEN_BAL == "" ? 0 : Convert.ToDouble(list.OPEN_BAL.Replace(",", "").Replace("(", "-").Replace(")", ""));
                    ws_InvestmentSummary.Cells["D" + row].Style.Numberformat.Format = "#,##0.00_);[Red](#,##0.00)";
                    ws_InvestmentSummary.Cells["E" + row].Value = list.OPEN_BAL == "" ? 0 : Convert.ToDouble(list.DEBITS.Replace(",", "").Replace("(", "-").Replace(")", ""));
                    ws_InvestmentSummary.Cells["E" + row].Style.Numberformat.Format = "#,##0.00_);[Red](#,##0.00)";
                    ws_InvestmentSummary.Cells["F" + row].Value = list.OPEN_BAL == "" ? 0 : Convert.ToDouble(list.CREDITS.Replace(",", "").Replace("(", "-").Replace(")", ""));
                    ws_InvestmentSummary.Cells["F" + row].Style.Numberformat.Format = "#,##0.00_);[Red](#,##0.00)";
                    ws_InvestmentSummary.Cells["G" + row].Value = list.OPEN_BAL == "" ? 0 : Convert.ToDouble(list.END_BAL.Replace(",", "").Replace("(", "-").Replace(")", ""));
                    ws_InvestmentSummary.Cells["G" + row].Style.Numberformat.Format = "#,##0.00_);[Red](#,##0.00)";
                    row++;
                }
                ws_InvestmentSummary.Cells.AutoFitColumns();
                package.SaveAs(excelFile);
                package.Dispose();
            }

            return string.Join(",", resultAll.Select(x => x.FUND).Distinct().ToArray());
        }
        private static FileInfo CreateFolderForUmbExcel(string Folder, string FileName)
        {

            string excelFileName = FileName;
            string excelFinalPath = Path.Combine(Folder, excelFileName);

            if (!Directory.Exists(Folder))
            {
                Directory.CreateDirectory(Folder);
            }

            FileInfo excelFile = new FileInfo(excelFinalPath);

            if (File.Exists(excelFinalPath))
            {
                File.Delete(excelFinalPath);
            }
            return excelFile;
        }

        private static void ExportToCsv(string folder, string FileName, string viewSql)
        {

            FileInfo excelFile = CreateFolderForUmbExcel(folder, FileName);
            using (var connection = new SqlConnection(connectionStringAzure))
            {
                //var resultAll = connection.Query<UmbTrialBalance>("SELECT XTR_FNDDSC,XTR_TKR,XTR_DESC1,XTR_DESC2,XTR_MATDAT,XTR_PRICE,POS_INCOM,POS_QTY,POS_AMORT,RND_MKTVAL,POS_YIELD,POS_YTM FROM CCLF_PortfolioSchedulePositions WHERE AsOfDate=@Date ORDER BY XTR_FNDDSC DESC", new { Date = Convert.ToDateTime(UmbDate) }, commandTimeout: 19000).AsList();
                var resultAll = connection.Query<UmbTrialBalance>("SELECT FUND,ACNO,ACCTDESC,OPEN_BAL,DEBITS,CREDITS,END_BAL FROM " + viewSql + " WHERE AsOfDate=@Date", new { Date = Convert.ToDateTime(UmbDate) }, commandTimeout: 19000).AsList();

                var comlumHeadrs = new string[]
                {
                "FUND","ACNO","ACCTDESC","OPEN_BAL","DEBITS","CREDITS","END_BAL"

                 };

                var trialRecords = (from e in resultAll
                                    select new[]
                                       {
                                           e.FUND,
                                            e.ACNO,
                                            e.ACCTDESC,
                                            e.OPEN_BAL,
                                            e.DEBITS,
                                            e.CREDITS,
                                            e.END_BAL                                            
                                       }).ToList();

                // Build the file content
                var employeecsv = new StringBuilder();
                trialRecords.ForEach(line =>
                {
                    employeecsv.AppendLine(string.Join("\t", line));
                });
                File.AppendAllText(excelFile.FullName, string.Join("\t", comlumHeadrs) + "\r\n" + employeecsv.ToString());
                // byte[] buffer = Encoding.ASCII.GetBytes($"{string.Join(",", comlumHeadrs)}\r\n{employeecsv.ToString()}");
            }
        }
    }


    public class UmbTrialBalance
    {
        public string FUND { get; set; }
        public string ACNO { get; set; }
        public string ACCTDESC { get; set; }
        public string OPEN_BAL { get; set; }
        public string DEBITS { get; set; }
        public string CREDITS { get; set; }
        public string END_BAL { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public DateTime AsOfDate { get; set; }
    }
}
