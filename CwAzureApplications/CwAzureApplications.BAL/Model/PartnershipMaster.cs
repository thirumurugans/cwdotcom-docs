﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CwAzureApplications.BAL.Model
{
    public class PartnershipMaster
    {
        public int PartnershipId { get; set; }
        public string PartnershipName { get; set; }
        public string VintageYear { get; set; }
        public string FundName { get; set; }
        public int ProductId { get; set; }
        public string InvestmentType { get; set; }
        public string Sector { get; set; }
        public string Geography { get; set; }
        public string StageFocus { get; set; }
        public DateTime ClosingDate { get; set; }
        public int IsActive { get; set; }
        public DateTime LiquidationDate { get; set; }
        public string USNonUS { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime InceptionDate { get; set; }
        public string Strategy { get; set; }
        public string SubStrategy { get; set; }
        public string MonitorType { get; set; }
        public string HoldingMonitorType { get; set; }
        public string FundId { get; set; }
        public string AssetTypes { get; set; }
       
    }
}
