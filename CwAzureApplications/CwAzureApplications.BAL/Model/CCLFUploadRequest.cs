﻿using System;

namespace CwAzureApplications.BAL.Model
{

    public enum UMBReportType
    {
        SchedulePositions,
        TrialBalance
    }
    public class ResultClass
    {
        public string FundShort { get; set; }
        public int UploadRows { get; set; }
    }

    public class CCLFUploadRequest
    {
        public CCLFUploadRequest()
        {
            this.Date = DateTime.Now;
        }
        public string Title { get; set; }
        public string CloudSaveFileName { get; set; }
        public string FileOriginalName { get; set; }
        public DateTime Date { get; set; }  
        public string User { get; set; }
        public string LocalPath { get; set; }

        // public Microsoft.AspNetCore.Http.IFormFile FormFile { get; set; }
        public int RowCounts { get; set; }
        public string Message { get; set; }
        public string FundShort { get; set; }
    }
}
