﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace CwAzureApplications.BAL.Model
{

    public class CommonClass
    {
        private string authorizationToken = null;
        private string USER_NAME = "davetepper";
        private string PASS_WORD = "Reporting40!!!####";
        private string SERVICE_URL = "https://cliffwater.backstopsolutions.com";
        private string LOGIN_URL = "/backstop/api/login";
        private string HTTP_METHOD_POST = "POST";
        private string HTTP_METHOD_GET = "GET";
        private string CONTENT_TYPE_HEADER = "application/vnd.api+json";

        public string Login()
        {
            try
            {
                //objEventLog.CreateLog(string.Format("{0} - {1} - Cliffwater : Backstop Rest API - Login started for the user {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, USER_NAME), eventLogFilePath);
                DateTime startTime = DateTime.Now;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11;
                authorizationToken = sendHttpRequest(SERVICE_URL + LOGIN_URL, HTTP_METHOD_POST, "", false, "");

                double seconds = (DateTime.Now - startTime).TotalSeconds;
                //objEventLog.CreateLog(string.Format("{0} - {1} - Cliffwater : Backstop Rest API - Login completed for the user {2}. Took {3} seconds", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, USER_NAME, seconds.ToString()), eventLogFilePath);
            }
            catch (Exception ex)
            {
                //objEventLog.CreateLog(string.Format("\t Error - {0} - {1} - Cliffwater : Backstop Rest API - {2} - More details in error log file", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message), eventLogFilePath);
                //objErrorLog.LogServerError(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, errorLogFilePath);
            }

            return authorizationToken;
        }
        public string sendHttpRequest(string requestUrl, string httpMethod, string requestBody, Boolean useToken, string authorizationToken)
        {
            string responseResult = "";

            try
            {
                var request = new HttpRequestMessage
                {
                    Content = string.IsNullOrWhiteSpace(requestBody) ? null : new StringContent(requestBody, Encoding.UTF8, CONTENT_TYPE_HEADER),
                    Method = new HttpMethod(httpMethod),
                    RequestUri = new Uri(requestUrl)

                };

                request.Headers.Accept.Clear();
                string token;

                if (useToken)
                {
                    token = authorizationToken;
                    request.Headers.TryAddWithoutValidation("token", "true");
                }
                else
                {
                    token = PASS_WORD;
                }

                request.Headers.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes(USER_NAME + ":" + token)));
                request.Headers.TryAddWithoutValidation("Accept", CONTENT_TYPE_HEADER);
                request.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.3; Win64; x64)");
                HttpClient httpClient = new HttpClient();
                httpClient.Timeout = TimeSpan.FromMinutes(60);
                //var res = httpClient.SendAsync(request);
                var response = httpClient.SendAsync(request).Result;
                response.EnsureSuccessStatusCode();
                responseResult = response.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex)
            {
                //objEventLog.CreateLog(string.Format("\t Error - {0} - {1} - Cliffwater : Backstop Rest API - {2} - More details in error log file", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message), eventLogFilePath);
                //objErrorLog.LogServerError(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, errorLogFilePath);
            }

            return responseResult;
        }

        public string ObjectToByteArray(string Apiobj)//, string apiType, string description, string apiLink, TimeSpan timeTaken)
        {
            byte[] obj2Bytes = null;
            string apiSize = "";
            //string apiTiming = string.Format("Time taken {0}:{1}:{2}", timeTaken.Hours, timeTaken.Minutes, timeTaken.Seconds);

            if (Apiobj != "")
            {
                //BinaryFormatter bf = new BinaryFormatter();
                //MemoryStream ms = new MemoryStream();
                //bf.Serialize(ms, obj);

                obj2Bytes = Encoding.ASCII.GetBytes(Apiobj);// ms.ToArray();
                int counter = 0;
                decimal number = (decimal)obj2Bytes.Length;
                string[] suffixes = { "Bytes", "KB", "MB", "GB", "TB", "PB" };

                while (Math.Round(number / 1024) >= 1)
                {
                    number = number / 1024;
                    counter++;
                }

                apiSize = string.Format("{0:n2} {1}", number, suffixes[counter]);
            }
            return apiSize;
            //dtAPICalls.Rows.Add(apiType, description, apiLink, (decimal)obj2Bytes.Length, apiSize, apiTiming, DateTime.Now);
        }
    }


    public class RootObject
    {
        public Datum[] data { get; set; }
        public object[] included { get; set; }
        public Meta meta { get; set; }
        
    }
    public class RootObjectFund
    {
        
        public object[] included { get; set; }
        public Meta meta { get; set; }
        public Data data { get; set; }
    }
    public class Meta
    {
        public int totalResourceCount { get; set; }
    }

    public class Datum
    {
        public string id { get; set; }
        public string type { get; set; }
        public Attributes attributes { get; set; }
        public Relationships relationships { get; set; }
        public Links31 links { get; set; }
    }

    public class Data
    {
        public string id { get; set; }
        public string type { get; set; }
        public Attributes attributes { get; set; }
        public Links links { get; set; }
    }

    public class Attributes
    {
        public string inflowTiming { get; set; }
        public Fundthisholdingisinvestedin fundThisHoldingIsInvestedIn { get; set; }
        public float managementFeePercent { get; set; }
        public string dealCurrency { get; set; }
        public Regularcustomfieldvalue[] regularCustomFieldValues { get; set; }
        public DateTime createdTimestamp { get; set; }
        public DateTime investmentPeriodEndDate { get; set; }
        public DateTime inceptionDate { get; set; }
        public float incentiveFeePercent { get; set; }
        public string name { get; set; }
        public DateTime modifiedTimestamp { get; set; }
        public string currency { get; set; }
        public string outflowTiming { get; set; }
        public DateTime investmentCloseDate { get; set; }
        public string description { get; set; }
        public string region { get; set; }
    }

    public class Fundthisholdingisinvestedin
    {
        public string resourceType { get; set; }
        public string resourceId { get; set; }
        public string resourceLink { get; set; }
    }

    public class Regularcustomfieldvalue
    {
        public int definitionId { get; set; }
        public string name { get; set; }
        public object value { get; set; }
    }

    public class Relationships
    {
        public Totalmultiples totalMultiples { get; set; }
        public Pmebenchmark pmeBenchmark { get; set; }
        public Notes notes { get; set; }
        public Productowningthisholding productOwningThisHolding { get; set; }
        public Documents documents { get; set; }
        public Values values { get; set; }
        public Ltdcapitalcalls ltdCapitalCalls { get; set; }
        public Unrealizedmultiples unrealizedMultiples { get; set; }
        public Realizedmultiples realizedMultiples { get; set; }
        public Analytics analytics { get; set; }
        public Grossirrs grossIrrs { get; set; }
        public Managementcompany managementCompany { get; set; }
        public Tasks tasks { get; set; }
        public Timeseriescustomfieldvalues timeSeriesCustomFieldValues { get; set; }
        public Commitments commitments { get; set; }
        public Commitmenttransactions commitmentTransactions { get; set; }
        public Entityrelationships entityRelationships { get; set; }
        public Netirrs netIrrs { get; set; }
        public Unfundedcommitments unfundedCommitments { get; set; }
        public Assettype assetType { get; set; }
        public Notices notices { get; set; }
        public Ltdkstvpi ltdKsTvpi { get; set; }
        public Earnings earnings { get; set; }
        public Recallableamount recallableAmount { get; set; }
        public Activities activities { get; set; }
        public Ltdcapitaldistributions ltdCapitalDistributions { get; set; }
        public Costbasis costBasis { get; set; }
        public Calledfees calledFees { get; set; }
        public Returns returns { get; set; }
        public Ltddirectalpha ltdDirectAlpha { get; set; }
        public Permissionbucket permissionBucket { get; set; }
    }

    public class Totalmultiples
    {
        public Links links { get; set; }
    }

    public class Links
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Pmebenchmark
    {
        public Links1 links { get; set; }
    }

    public class Links1
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Notes
    {
        public Links2 links { get; set; }
    }

    public class Links2
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Productowningthisholding
    {
        public Links3 links { get; set; }
    }

    public class Links3
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Documents
    {
        public Links4 links { get; set; }
    }

    public class Links4
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Values
    {
        public Links5 links { get; set; }
    }

    public class Links5
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Ltdcapitalcalls
    {
        public Links6 links { get; set; }
    }

    public class Links6
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Unrealizedmultiples
    {
        public Links7 links { get; set; }
    }

    public class Links7
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Realizedmultiples
    {
        public Links8 links { get; set; }
    }

    public class Links8
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Analytics
    {
        public Links9 links { get; set; }
    }

    public class Links9
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Grossirrs
    {
        public Links10 links { get; set; }
    }

    public class Links10
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Managementcompany
    {
        public Links11 links { get; set; }
    }

    public class Links11
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Tasks
    {
        public Links12 links { get; set; }
    }

    public class Links12
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Timeseriescustomfieldvalues
    {
        public Links13 links { get; set; }
    }

    public class Links13
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Commitments
    {
        public Links14 links { get; set; }
    }

    public class Links14
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Commitmenttransactions
    {
        public Links15 links { get; set; }
    }

    public class Links15
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Entityrelationships
    {
        public Links16 links { get; set; }
    }

    public class Links16
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Netirrs
    {
        public Links17 links { get; set; }
    }

    public class Links17
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Unfundedcommitments
    {
        public Links18 links { get; set; }
    }

    public class Links18
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Assettype
    {
        public Links19 links { get; set; }
    }

    public class Links19
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Notices
    {
        public Links20 links { get; set; }
    }

    public class Links20
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Ltdkstvpi
    {
        public Links21 links { get; set; }
    }

    public class Links21
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Earnings
    {
        public Links22 links { get; set; }
    }

    public class Links22
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Recallableamount
    {
        public Links23 links { get; set; }
    }

    public class Links23
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Activities
    {
        public Links24 links { get; set; }
    }

    public class Links24
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Ltdcapitaldistributions
    {
        public Links25 links { get; set; }
    }

    public class Links25
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Costbasis
    {
        public Links26 links { get; set; }
    }

    public class Links26
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Calledfees
    {
        public Links27 links { get; set; }
    }

    public class Links27
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Returns
    {
        public Links28 links { get; set; }
    }

    public class Links28
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Ltddirectalpha
    {
        public Links29 links { get; set; }
    }

    public class Links29
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Permissionbucket
    {
        public Links30 links { get; set; }
    }

    public class Links30
    {
        public string self { get; set; }
        public string related { get; set; }
    }

    public class Links31
    {
        public string self { get; set; }
    }
}
