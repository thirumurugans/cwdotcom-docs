﻿using PrivateEquityReportCreation.ErrorLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.File;

namespace CwAzureApplications.BAL
{
    public class PrivateEquityExcelIrr
    {
        private static LogManager _logManagerEvent = new LogManager("Event");
        private static LogManager _logError = new LogManager("Error");
        static List<ProductDetails> productList = null;
        static string excelFilePath = string.Empty;
        static DateTime asofdate;
        static JsonSerializeObject array = null;
        static readonly string baseDirectory = ConfigurationManager.AppSettings["ExcelBasePath"];
        static readonly string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
        public static void CreateExcelWithIrr()
        {

            try
            {
                using (var conn = new SqlConnection(connectionString))
                {
                    _logManagerEvent.Splitter();
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Process Started => ******      Create Excel with Transaction Cash flow & Irr Calculation...     ******"));
                    using (StreamReader file = File.OpenText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "Config.json")))
                    {
                        array = JsonConvert.DeserializeObject<JsonSerializeObject>(file.ReadToEnd());
                        asofdate = Convert.ToDateTime(array.HoldingAsOfDate);
                        //excelFilePath = baseDirectory;
                        productList =
                           conn.Query<ProductDetails>(@"SELECT DISTINCT ProductId,ProductName,AsOfDate DateAs FROM [dbo].[GenerateQuarterDateRange](3,default) ORDER BY ProductName ASC,AsOfDate DESC").AsList();
                        if (productList.Count > 0)
                            CreateExcel();
                        else
                            _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "No Products available..."));
                    }
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Process Completed => Excel Creation with Transaction Cash flow & Irr Calculation details..."));
                    _logManagerEvent.Splitter();
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception => IRR Excel Create process at Main function...!"));
            }
        }

        /// <summary>
        /// Creating excel with data for all possible type of category(Strategy,Sector,Geography)
        /// </summary>
        private static void CreateExcel()
        {
            int productCnt = 1;
            foreach (var product in productList)
            {
                try
                {                    
                    using (XLWorkbook workbook = new XLWorkbook())
                    {
                        using (var conn = new SqlConnection(connectionString))
                        {
                            //var queryResult= conn.Query<string>("SELECT TOP 1 DateAs FROM vtbl_BackstopDataSync WHERE ProductId=" + product.ProductId).FirstOrDefault();
                            //if (queryResult.Any())
                                asofdate = Convert.ToDateTime(product.DateAs.ToString());
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, productCnt + "). Processing(Current Quarter) => The product name is : " + product.ProductName + " => As Of Date : " + asofdate.ToString("MM/dd/yyyy")));

                            List<TransactionCashflow> cashFlowList = conn.Query<TransactionCashflow>("exec Sync_PE_sp_GetPortfolioTransactionForExcelIrrRebuildSync " + product.ProductId + ",'" + asofdate + "'").AsList();
                            ExcelSheetIrr(workbook, cashFlowList.OrderBy(a => a.Date).ToList(), "Portfolio", asofdate, 1);

                            DateTime firstDayOfTheMonth = new DateTime(asofdate.AddMonths(-3).Year, asofdate.AddMonths(-3).Month, 1);
                            DateTime previousQtr = firstDayOfTheMonth.AddMonths(1).AddDays(-1);
                            List<TransactionCashflow> cashFlowListPrevQtr = conn.Query<TransactionCashflow>("exec Sync_PE_sp_GetPortfolioTransactionForExcelIrrRebuildSync " + product.ProductId + ",'" + previousQtr + "'").AsList();
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "    Processing(Previous Quarter) => The product name is : " + product.ProductName + " => As Of Date : " + asofdate.ToString("MM/dd/yyyy")));
                            ExcelSheetIrr(workbook, cashFlowListPrevQtr.OrderBy(a => a.Date).ToList(), "Portfolio", previousQtr, 1);
                            int categoryCnt = 1;
                            foreach (var list in cashFlowList.Where(a => a.Strategy != null).Select(a => a.Strategy).Distinct())
                            {
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "   " + categoryCnt + "). Strategy level Processing => The Strategy is : " + list));
                                ExcelSheetIrr(workbook, cashFlowList.Where(str => str.Strategy == list.ToString()).OrderBy(a => a.Date).ToList(), ("Strategy-" + list).Length > 30 ? ("Strategy-" + list).Remove(30) : ("Strategy-" + list), asofdate);
                                categoryCnt++;
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "       End"));
                            }
                            categoryCnt = 1;
                            foreach (var list in cashFlowList.Where(a => a.Sector != null).Select(a => a.Sector).Distinct())
                            {
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "   " + categoryCnt + "). Sector level Processing => The Sector is : " + list));
                                ExcelSheetIrr(workbook, cashFlowList.Where(str => str.Sector == list.ToString()).OrderBy(a => a.Date).ToList(), ("Sector-" + list).Length > 30 ? ("Sector-" + list).Remove(30) : ("Sector-" + list), asofdate);
                                categoryCnt++;
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "       End"));
                            }
                            categoryCnt = 1;
                            foreach (var list in cashFlowList.Where(a => a.Geography != null).Select(a => a.Geography).Distinct())
                            {
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "   " + categoryCnt + "). Geography level Processing => The Geography is : " + list));
                                ExcelSheetIrr(workbook, cashFlowList.Where(str => str.Geography == list.ToString()).OrderBy(a => a.Date).ToList(), ("Geography-" + list).Length > 30 ? ("Geography-" + list).Remove(30) : ("Geography-" + list), asofdate);
                                categoryCnt++;
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "       End"));
                            }
                            categoryCnt = 1;
                            foreach (var list in cashFlowList.Where(a => a.VintageYear != null).Select(a => a.VintageYear).Distinct())
                            {
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "   " + categoryCnt + "). Vintage level Processing => The Vintage year is : " + list));
                                ExcelSheetIrr(workbook, cashFlowList.Where(str => str.VintageYear == list.ToString()).OrderBy(a => a.Date).ToList(), ("Vintage-" + list).Length > 30 ? ("Vintage-" + list).Remove(30) : ("Vintage-" + list), asofdate);
                                categoryCnt++;
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "       End"));

                            }                            
                        }
                        excelFilePath = Path.Combine(baseDirectory + "\\" + product.ProductName.Replace("/", "-").Replace("&", " and ") + "_" + asofdate.Year + "-" + asofdate.Month + "-" + asofdate.Day + ".xlsx");
                        workbook.SaveAs(excelFilePath);
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "The Excel file saved into following location => " + excelFilePath));
                        //// Upload to azure
                        UploadIrrExcelToAzure(product.ProductName.Replace("/", "-").Replace("&", " and ") + "_" + asofdate.Year + "-" + asofdate.Month + "-" + asofdate.Day + ".xlsx", excelFilePath);
                        workbook.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                    _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception => IRR Excel Create process error for '" + product.ProductName + "' => ProductId : " + product.ProductId));
                }
                productCnt++;
            }

        }
        // IRR Excel Upload to azure for all products
        private static void UploadIrrExcelToAzure(string targetFileName, string excelFilePath)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(Settings.AzureConnectionString);
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Azure Upload => The Excel file uploading to azure ..... => " + targetFileName));               
                if (File.Exists(excelFilePath))
                {
                    CloudFileClient fileClient = storageAccount.CreateCloudFileClient();
                    var share = fileClient.GetShareReference(Settings.UploadFolder);
                    var cloudFile = share.GetRootDirectoryReference().GetFileReference(targetFileName);
                    cloudFile.Properties.ContentDisposition = "attachment; filename=" + targetFileName;
                    //try
                    //{
                    //    cloudFile.Delete();
                    //}
                    //catch
                    //{
                    //}
                    cloudFile.UploadFromFile(excelFilePath);
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Azure Upload => Exception => IRR Excel Upload to azure got failed for '" + targetFileName + "'"));
            }
        }

        /// <summary>
        /// Excel sheet creation & update the values in Excel for all category
        /// </summary>
        /// <param name="workbook"></param>
        /// <param name="listCashflow"></param>
        /// <param name="sheetName"></param>
        /// <param name="asofdateLocal"></param>
        /// <param name="flag"></param>
        private static void ExcelSheetIrr(XLWorkbook workbook, List<TransactionCashflow> listCashflow, string sheetName, DateTime asofdateLocal, int flag = 0)
        {
            try
            {
                using (var ws = workbook.AddWorksheet(sheetName + (flag == 1 ? "(" + asofdateLocal.Year + "-" + asofdateLocal.Month + "-" + asofdateLocal.Day + ")" : "")))
                {
                    int rowCnt = 2;
                    ws.Columns("A", "F").Style.Alignment.WrapText = true;
                    ws.Columns("A").Width = 40;
                    ws.Columns("B").Width = 15;
                    ws.Columns("C").Width = 20;
                    // ws.Columns("F").Width = 25;
                    ws.Cells("A1:E1").Style.Font.Bold = true;
                    ws.Cells("A1:E1").Style.Fill.BackgroundColor = XLColor.SkyBlue;
                    ws.Cells("A1:E1").Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                    ws.Cells("A1:E1").Style.Border.RightBorder = XLBorderStyleValues.Thin;
                    ws.Cells("A1:E1").Style.Border.TopBorder = XLBorderStyleValues.Thin;
                    ws.Cells("A1:E1").Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    ws.Cells("A1").Value = "Partnership Name";
                    ws.Cells("B1").Value = "Value";
                    ws.Cells("C1").Value = "Date";
                    
                    ws.Cells("D1").Value = "Type";
                    ws.Cells("E1").Value = "Type Name";

                    foreach (var item in listCashflow)
                    {
                        ws.Cell(rowCnt, 1).Value = Convert.ToString(item.Partnership);
                        ws.Cell(rowCnt, 2).Value = Convert.ToDecimal(item.Value);
                        ws.Cell(rowCnt, 2).Style.NumberFormat.Format = "$#,##0.00;[RED]-$#,##0.00";
                        ws.Cell(rowCnt, 3).Value = item.Date;
                        ws.Cell(rowCnt, 3).Style.DateFormat.Format = "MM/dd/yyyy";

                        ws.Cell(rowCnt, 4).Value = item.TransactionType;
                        ws.Cell(rowCnt, 5).Value = item.TransactionTypeName;
                        
                        ws.Cells("A" + rowCnt + ":E" + rowCnt).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        ws.Cells("A" + rowCnt + ":E" + rowCnt).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Cells("A" + rowCnt + ":E" + rowCnt).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Cells("A" + rowCnt + ":E" + rowCnt).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        rowCnt++;
                    }
                    ws.Cells("G2").Value = sheetName + " Net IRR" + (flag == 1 ? "(" + asofdateLocal.Year + "-" + asofdateLocal.Month + "-" + asofdateLocal.Day + ")" : "");                   
                    ws.Columns("G").Width = 30;                    
                    ws.Cells("G2:H2").Style.Font.Bold = true;
                    if (listCashflow.Count > 0)
                    {                       
                        ws.Cells("H2").FormulaA1 = "=ROUND(XIRR(B2:B" + (listCashflow.Count + 1) + ",C2:C" + (listCashflow.Count + 1) + ")*100,2)";
                    }                    
                    ws.Columns().AdjustToContents();
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception => Excel IRR data preparation error for " + sheetName));
            }
        }

        //public static void BdcsWebsiteSync()
        //{
        //    CallHttpClient("SaveCWBDCIndexCharacteristicsData");
        //    CallHttpClient("SavePremiumDiscountChartData");
        //}
        //private static void CallHttpClient(string param)
        //{
        //    string URL = "http://cliffwaterqa.azurewebsites.net/Cliffwater_V5/";
        //    using (HttpClient client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(URL);

        //        // Add an Accept header for JSON format.
        //        client.DefaultRequestHeaders.Accept.Add(
        //        new MediaTypeWithQualityHeaderValue("application/json"));

        //        // List data response.
        //        HttpResponseMessage response = client.GetAsync(param).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
        //        if (response.IsSuccessStatusCode)
        //        {

        //        }
        //    }
        //}
    }
    

   
}
