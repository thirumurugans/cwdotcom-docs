﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using OfficeOpenXml;
using System.Configuration;
using PrivateEquityReportCreation.ErrorLog;
using CwAzureApplications.BAL.Emails;

namespace CwAzureApplications.BAL
{
    public class UmbDataRawExcel
    {
        private static readonly string connectionStringAzure = ConfigurationManager.AppSettings["ConnectionString"];
        private readonly static string excelPath = ConfigurationManager.AppSettings["UmDataFilePath"];
        private readonly static string excelPathCELF = ConfigurationManager.AppSettings["CelfUmDataFilePath"];
        private readonly static string excelPathLocal = ConfigurationManager.AppSettings["UmDataFilePathLocal"];
        private readonly static string environment = ConfigurationManager.AppSettings["Environment"];
        //static string excelPath = @"D:\Umb\";
        static List<Tuple<string, int, int>> memoRowDetails = new List<Tuple<string, int, int>>();//1-> memo heading reference 2-> memo heading row, 3-> memo Sum row
        static string UmbDate = "05/29/2020";
        static LogManager _logManagerEvent = new LogManager("Event");
        static LogManager _logError = new LogManager("Error");
        public static void GenerateExcelwithUmbData()
        {
            try
            {
                string fileSubFolder = string.Empty;
                string fileSubFoldercelf = string.Empty;
                string exportedFunds = string.Empty;
                string reportFileName = string.Empty;
                string reportLogName = string.Empty;
                string celfReportFileName = string.Empty;
                string celfReportLogName = string.Empty;
                using (var connection = new SqlConnection(connectionStringAzure))
                {
                    var activeFunds = connection.Query<string>("SELECT ShortFundName FROM umbSchedulePositionFundsScrapeConfig WHERE IsActive=1", commandTimeout: 19000).AsList();
                    var checkToExport = connection.Query<UmbDataImport>("SELECT TOP 1 * FROM v_CCLF_UploadUmbData ORDER BY UploadId DESC", commandTimeout: 19000).AsList();
                    if (checkToExport.Count > 0)
                    {
                        _logManagerEvent.Splitter();
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "UMB Data Export process started..."));
                    }
                    foreach (var check in checkToExport.Select(x => x.Date).Distinct())
                    {
                        try
                        {

                            UmbDate = check.Date.ToShortDateString();
                            //int days = -1;
                            //if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
                            //    days = -3;
                            //else if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
                            //    days = -2;
                            //else if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
                            //    days = -1;

                            //string fileSubFolder = excelPath + DateTime.Now.AddDays(days).Year + "\\" + DateTime.Now.AddDays(days).ToString("MMM") + "\\Data\\";
                            //string fileName = DateTime.Now.AddDays(days).ToString("yyyyMMdd");

                            /* CCLF Schedule Position */
                            fileSubFolder = excelPath + check.Date.Year + "\\" + check.Date.ToString("MMMM") + "\\data\\";
                            string fileName = check.Date.ToString("yyyyMMdd");
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "UMB Data Export process in the Folder of " + fileSubFolder));
                            ExportToCsv(fileSubFolder, fileName + ".txt", "vCCLF_PortfolioSchedulePositions");
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "UMB Data Export to Text file completed, The filename is " + fileName + ".txt"));
                            exportedFunds = UmbDataExportToExcel(fileSubFolder, fileName + ".xlsx", connection, "UMB Data", "vCCLF_PortfolioSchedulePositions");
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "UMB Data Export to Excel file completed, The filename is " + fileName + ".xls"));
                            reportFileName = fileName + ".xlsx";
                            reportLogName = fileName + ".txt";

                            /* CELF Schedule Position */
                            fileSubFoldercelf = excelPathCELF + check.Date.Year + "\\" + check.Date.ToString("MM") + " - " + check.Date.ToString("MMMM") + "\\data\\";
                            string fileNamecelf = check.Date.ToString("yyyyMMdd") + " CELF";
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "UMB Data Export process in the Folder of " + fileSubFolder));
                            ExportToCsv(fileSubFoldercelf, fileNamecelf + ".txt", "vCELF_PortfolioSchedulePositions");
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "UMB Data Export to Text file completed, The filename is " + fileName + ".txt"));
                            exportedFunds = exportedFunds + "," + UmbDataExportToExcel(fileSubFoldercelf, fileNamecelf + ".xlsx", connection, "CELF UMB Data", "vCELF_PortfolioSchedulePositions");
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "UMB Data Export to Excel file completed, The filename is " + fileName + ".xls"));
                            exportedFunds = exportedFunds.TrimEnd(',');

                            celfReportFileName = fileNamecelf + ".xlsx";
                            celfReportLogName = fileNamecelf + ".txt";

                        }
                        catch (Exception ex)
                        {
                            _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                            _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-UMB Data Export process failed for the date " + check.Date.ToShortDateString()));
                        }
                    }
                    foreach (var check in checkToExport.Select(x => x.Date).Distinct())
                    {
                        connection.Query<UmbDataImport>("UPDATE CCLF_UploadUmbData SET YetToExport=1 WHERE [Date]=@Date", new { check.Date }, commandTimeout: 19000).AsList();
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "UMB Data Export process complete status Update, The Date is " + check.Date.ToShortDateString()));
                    }
                    if (checkToExport.Count > 0)
                    {
                        var exportFund = exportedFunds.Split(',').ToList();
                        var queryMissingFunds = (from l1 in activeFunds
                                                 join l2 in exportFund on l1.ToLower().Trim() equals l2.ToLower().Trim()
                                                 into leftJ
                                                 from lj in leftJ.DefaultIfEmpty()
                                                 select new { Key = l1, Value = lj })
                                                  .Where(x => x.Value == null)
                                                  .Select(x => x.Key)
                                                  .ToList();
                        EmailMessages emailMessage = new EmailMessages();
                        emailMessage.Subject = (environment.ToLower() == "qa" ? "QA - " : "") + "CCLF & CELF UMB Schedule Position Upload Status";
                        emailMessage.IsBodyHtml = true;
                        StringBuilder builder = new StringBuilder();
                        builder.Append("<!DOCTYPE html>");
                        builder.Append("<head></head><body>");
                        builder.Append("<p>Hi Team,</p>");
                        builder.Append("<p>I have generated the UMB Schedule Positions Excel files for <span>" + checkToExport[0].Date.ToShortDateString() + "</span>,</p>");
                        builder.Append("<p>" + fileSubFolder + "</p>");
                        builder.Append("<p>" + fileSubFoldercelf + "</p>");
                        //builder.Append("<p>Please check the status <a href='https://cliffwater.com/Admin#CCLFUploadExcel'>https://cliffwater.com/Admin#CCLFUploadExcel</a></p></br>");
                        if (queryMissingFunds.Count > 0)
                            builder.Append("<p><span style='background-color:rgb(255,217,102)'>Note: The following funds excel(Schedule Position) are not available in UMB Site - <b>" + string.Join(",", queryMissingFunds.ToArray()) + "</b></span></p>");
                        builder.Append("<p>Please check the status, https://cliffwater.com/Admin#CCLFUploadExcel</p>");

                        builder.Append("</body></html>");
                        emailMessage.Body = builder.ToString();
                        //add attachments if present

                        if (!string.IsNullOrEmpty(reportFileName) && File.Exists(fileSubFolder + reportFileName))
                        {
                            Byte[] bytes = File.ReadAllBytes(fileSubFolder + reportFileName);
                            string fileContent = Convert.ToBase64String(bytes);
                            emailMessage.AddAttachment(reportFileName, fileContent);
                        }
                        if (!string.IsNullOrEmpty(reportLogName) && File.Exists(fileSubFolder + reportLogName))
                        {
                            Byte[] bytes = File.ReadAllBytes(fileSubFolder + reportLogName);
                            string fileContent = Convert.ToBase64String(bytes);
                            emailMessage.AddAttachment(reportLogName, fileContent);
                        }
                        if (!string.IsNullOrEmpty(celfReportFileName) && File.Exists(fileSubFoldercelf + celfReportFileName))
                        {
                            Byte[] bytes = File.ReadAllBytes(fileSubFoldercelf + celfReportFileName);
                            string fileContent = Convert.ToBase64String(bytes);
                            emailMessage.AddAttachment(celfReportFileName, fileContent);
                        }
                        if (!string.IsNullOrEmpty(celfReportLogName) && File.Exists(fileSubFoldercelf + celfReportLogName))
                        {
                            Byte[] bytes = File.ReadAllBytes(fileSubFoldercelf + celfReportLogName);
                            string fileContent = Convert.ToBase64String(bytes);
                            emailMessage.AddAttachment(celfReportLogName, fileContent);
                        }
                        EmailSend.EmailTrigger(emailMessage);
                    }

                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-UMB Data Export process Finished!"));
            }
        }
        private static string UmbDataExportToExcel(string baseFolder, string fileName, SqlConnection connection, string sheetName, string sqlViewName)
        {

            FileInfo excelFile = CreateFolderForUmbExcel(baseFolder, fileName);

            var resultAll = connection.Query<UmbDataReal>("SELECT XTR_FUND,XTR_FNDDSC,XTR_TKR,XTR_DESC1,XTR_DESC2,XTR_MATDAT,XTR_PRICE,POS_INCOM,POS_QTY,POS_AMORT,RND_MKTVAL,POS_YIELD,POS_YTM FROM " + sqlViewName + " WHERE AsOfDate=@Date ORDER BY XTR_FNDDSC DESC", new { Date = Convert.ToDateTime(UmbDate) }, commandTimeout: 19000).AsList();

            using (ExcelPackage package = new ExcelPackage(excelFile))
            {
                var ws_InvestmentSummary = package.Workbook.Worksheets.Add(sheetName);
                ws_InvestmentSummary.Cells["A1"].Value = "XTR_FNDDSC";
                ws_InvestmentSummary.Cells["B1"].Value = "XTR_TKR";
                ws_InvestmentSummary.Cells["C1"].Value = "XTR_DESC1";
                ws_InvestmentSummary.Cells["D1"].Value = "XTR_DESC2";
                ws_InvestmentSummary.Cells["E1"].Value = "XTR_MATDAT";
                ws_InvestmentSummary.Cells["F1"].Value = "XTR_PRICE";
                ws_InvestmentSummary.Cells["G1"].Value = "POS_INCOM";
                ws_InvestmentSummary.Cells["H1"].Value = "POS_QTY";
                ws_InvestmentSummary.Cells["I1"].Value = "POS_AMORT";
                ws_InvestmentSummary.Cells["J1"].Value = "RND_MKTVAL";
                ws_InvestmentSummary.Cells["K1"].Value = "POS_YIELD";
                ws_InvestmentSummary.Cells["L1"].Value = "POS_YTM";
                int row = 2;
                foreach (var list in resultAll)
                {
                    ws_InvestmentSummary.Cells["A" + row].Value = list.XTR_FNDDSC;
                    ws_InvestmentSummary.Cells["B" + row].Value = list.XTR_TKR;
                    ws_InvestmentSummary.Cells["C" + row].Value = list.XTR_DESC1;
                    ws_InvestmentSummary.Cells["D" + row].Value = list.XTR_DESC2;
                    ws_InvestmentSummary.Cells["E" + row].Value = list.XTR_MATDAT;
                    ws_InvestmentSummary.Cells["F" + row].Value = Convert.ToDouble(list.XTR_PRICE);
                    ws_InvestmentSummary.Cells["G" + row].Value = Convert.ToDouble(list.POS_INCOM);
                    ws_InvestmentSummary.Cells["H" + row].Value = Convert.ToDouble(list.POS_QTY);
                    ws_InvestmentSummary.Cells["I" + row].Value = Convert.ToDouble(list.POS_AMORT);
                    ws_InvestmentSummary.Cells["J" + row].Value = Convert.ToDouble(list.RND_MKTVAL);
                    ws_InvestmentSummary.Cells["K" + row].Value = Convert.ToDouble(list.POS_YIELD);
                    ws_InvestmentSummary.Cells["L" + row].Value = Convert.ToDouble(list.POS_YTM);
                    row++;
                }
                ws_InvestmentSummary.Cells.AutoFitColumns();
                package.SaveAs(excelFile);
                package.Dispose();
            }

            return string.Join(",", resultAll.Select(x => x.XTR_FUND).Distinct().ToArray());
        }
        private static FileInfo CreateFolderForUmbExcel(string Folder, string FileName)
        {

            string excelFileName = FileName;
            string excelFinalPath = Path.Combine(Folder, excelFileName);

            if (!Directory.Exists(Folder))
            {
                Directory.CreateDirectory(Folder);
            }

            FileInfo excelFile = new FileInfo(excelFinalPath);

            if (File.Exists(excelFinalPath))
            {
                File.Delete(excelFinalPath);
            }
            return excelFile;
        }

        private static void ExportToCsv(string folder, string FileName, string viewSql)
        {

            FileInfo excelFile = CreateFolderForUmbExcel(folder, FileName);
            using (var connection = new SqlConnection(connectionStringAzure))
            {
                var resultAll = connection.Query<UmbDataReal>("SELECT XTR_FNDDSC,XTR_TKR,XTR_DESC1,XTR_DESC2,XTR_MATDAT,XTR_PRICE,POS_INCOM,POS_QTY,POS_AMORT,RND_MKTVAL,POS_YIELD,POS_YTM FROM " + viewSql + " WHERE AsOfDate=@Date ORDER BY XTR_FNDDSC DESC", new { Date = Convert.ToDateTime(UmbDate) }, commandTimeout: 19000).AsList();
                var comlumHeadrs = new string[]
            {
                "XTR_FNDDSC","XTR_TKR","XTR_DESC1","XTR_DESC2","XTR_MATDAT","XTR_PRICE","POS_INCOM","POS_QTY","POS_AMORT","RND_MKTVAL","POS_YIELD","POS_YTM"

            };

                var employeeRecords = (from e in resultAll
                                       select new[]
                                       {
                                           e.XTR_FNDDSC,
                                            e.XTR_TKR,
                                            e.XTR_DESC1,
                                            e.XTR_DESC2,
                                            e.XTR_MATDAT,
                                            e.XTR_PRICE,
                                            e.POS_INCOM,
                                            e.POS_QTY,
                                            e.POS_AMORT,
                                            e.RND_MKTVAL,
                                            e.POS_YIELD,
                                            e.POS_YTM
                                       }).ToList();

                // Build the file content
                var employeecsv = new StringBuilder();
                employeeRecords.ForEach(line =>
                {
                    employeecsv.AppendLine(string.Join("\t", line));
                });
                File.AppendAllText(excelFile.FullName, string.Join("\t", comlumHeadrs) + "\r\n" + employeecsv.ToString());
                // byte[] buffer = Encoding.ASCII.GetBytes($"{string.Join(",", comlumHeadrs)}\r\n{employeecsv.ToString()}");
                //string fund
            }
        }

        public static void EmailTriggerForSchedulePosition()
        {
            try
            {

            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Email triggerfor PA focus list funds new docs"));
            }
        }
    }
    public class UmbDataImport
    {
        public string Title { get; set; }
        public string CloudSaveFileName { get; set; }
        public string FileOriginalName { get; set; }
        public DateTime Date { get; set; }
        public string user { get; set; }
    }

    public class UmbDataReal
    {
        public DateTime? AsOfDate { get; set; }
        public string XTR_FUND { get; set; }
        public string XTR_FNDDSC { get; set; }
        public string XTR_TKR { get; set; }
        public string XTR_DESC1 { get; set; }
        public string XTR_DESC2 { get; set; }
        public string OCC_ID { get; set; }
        public string XTR_CUSIP { get; set; }
        public string XTR_GROUP { get; set; }
        public string XTR_GROUP1 { get; set; }
        public string XTR_GROUP2 { get; set; }
        public string XTR_CATGRY { get; set; }
        public string XTR_CATDSC { get; set; }
        public string XTR_SECTOR { get; set; }
        public string XTR_SECTR1 { get; set; }
        public string XTR_SECTR2 { get; set; }
        public string XTR_CONTRY { get; set; }
        public string XTR_CONDSC { get; set; }
        public string XTR_STATE { get; set; }
        public string XTR_STNAME { get; set; }
        public string XTR_CURNCY { get; set; }
        public string XTR_CURDSC { get; set; }
        public string XTR_MATDAT { get; set; }
        public string XTR_SUBUNT { get; set; }
        public string XTR_SUBDSC { get; set; }
        public string XTR_PRICE { get; set; }
        public string XTR_PRISRC { get; set; }
        public string SECTYPE { get; set; }
        public string SECTYPE2 { get; set; }
        public string RPT_ID { get; set; }
        public string INCLUD_CAT { get; set; }
        public string LEGAL1 { get; set; }
        public string LEGAL2 { get; set; }
        public string LEGAL3 { get; set; }
        public string LEGAL4 { get; set; }
        public string LEGAL5 { get; set; }
        public string LEGAL6 { get; set; }
        public string BAS_Q_DECS { get; set; }
        public string BAS_M_DECS { get; set; }
        public string LCL_P_DECS { get; set; }
        public string LCL_Q_DECS { get; set; }
        public string MULTICURR { get; set; }
        public string ISBASCURR { get; set; }
        public string POS_INCOM { get; set; }
        public string CLASS { get; set; }
        public string LONGSHORT { get; set; }
        public string MCSC { get; set; }
        public string QTYINDIC { get; set; }
        public string FAS_LEVEL { get; set; }
        public string SECTOR { get; set; }
        public string FUNDCURR { get; set; }
        public string BASECURR { get; set; }
        public string POS_QTY { get; set; }
        public string POS_ORGCST { get; set; }
        public string POS_AMORT { get; set; }
        public string POS_MKTVAL { get; set; }
        public string RND_MKTVAL { get; set; }
        public string POS_GL { get; set; }
        public string RNDPOS_GL { get; set; }
        public string POS_YIELD { get; set; }
        public string POS_YTM { get; set; }
        public string POS_YTM_NA { get; set; }
        public string POS_PC_PRT { get; set; }
        public string POS_PC_AST { get; set; }
        public string POS_PC_NAS { get; set; }
        public string POS_ORGFAC { get; set; }
    }
}
