﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using OfficeOpenXml;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CwAzureApplications.DAL;
using System.IO;
using System.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.File;
using PrivateEquityReportCreation.ErrorLog;
using OfficeOpenXml.Style;

namespace CwAzureApplications.BAL
{
    public class PeUploadHoldingFileToAzure
    {
        
        private readonly static string queuedFiles = ConfigurationManager.AppSettings["PEUploadQueuedPath"];
        private static string processedFiles = ConfigurationManager.AppSettings["PEUploadProcessedPath"];
        private static string unProcessedFiles = ConfigurationManager.AppSettings["PEUploadUnProcessedPath"];
        private readonly static LogManager logManagerEvent = new LogManager("Event");
        private readonly static LogManager logError = new LogManager("Error");
        private readonly static string spAzureUpload = "Website_PE_Admin_SaveUploadPEHoldings";
        private readonly static string spAzureUploadDocs = "EXEC usp_SaveUploadDocs @Xml,@Email";
        //private static readonly string connectionStringAzure = ConfigurationManager.AppSettings["ConnectionStringQA"];
        private static readonly string connectionStringAzure = ConfigurationManager.AppSettings["ConnectionString"];
        public static void PeUploadHoldingMain()
        {
            try
            {


                processedFiles = ConfigurationManager.AppSettings["PEUploadProcessedPath"];
                unProcessedFiles = ConfigurationManager.AppSettings["PEUploadUnProcessedPath"];

                DirectoryInfo info = new DirectoryInfo(queuedFiles);

                List<string> excelFileExtensions = new List<string> { ".xlsx", ".xlsm" };
                logManagerEvent.Splitter();
                logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Process Started => ******      PE Holding Excel Upload Process Start...     ******"));
                var allFiles = (from f in info.GetFiles() orderby f.CreationTime ascending select f).ToList();
                logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Total File Available in the Directory - " + allFiles.Count));
                List<string> dates = new List<string>();
                using (var conn = new SqlConnection(DataAccess.connectionStringAzure))
                {
                    var query = from c in allFiles
                                join d in excelFileExtensions on c.Extension.ToLower() equals d.ToString().ToLower()
                                select c;
                    logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Total Valid File Available in the Directory(.xlsx,.xlsm) - " + query.Count()));
                    processedFiles = processedFiles + DateTime.Now.ToString("yyyyMMdd") + "\\";
                    unProcessedFiles = unProcessedFiles + DateTime.Now.ToString("yyyyMMdd") + "\\";
                    if (query.Count() > 0)
                    {
                        if (!Directory.Exists(processedFiles))
                            Directory.CreateDirectory(processedFiles);
                        if (!Directory.Exists(unProcessedFiles))
                            Directory.CreateDirectory(unProcessedFiles);
                    }

                    foreach (var filePath in query)
                    {
                        string fileName = Path.GetFileNameWithoutExtension(filePath.Name).Replace("&", "_").Replace("'", "_").Trim();

                        string destFileProcessed = processedFiles + fileName + "_" + DateTime.Now.ToString("yyyyMMdd hh-mm-ss") + filePath.Extension;
                        string destUnProcessedFiles = Path.Combine(unProcessedFiles, fileName + "_" + DateTime.Now.ToString("yyyyMMdd hh-mm-ss") + Path.GetExtension(filePath.Name));

                        string destFileName = fileName + "_" + DateTime.Now.ToString("yyyyMMdd hh-mm-ss") + Path.GetExtension(filePath.FullName);

                        int docId = 0;


                        try
                        {

                            int holdingCount = 0;
                            string fundNameNotMatch = string.Empty;
                            int dateStart = -1;
                            if (filePath.Name.IndexOf("4Q") >= 0)
                                dateStart = filePath.Name.IndexOf("4Q") + 8;
                            else if (filePath.Name.IndexOf("3Q") >= 0)
                                dateStart = filePath.Name.IndexOf("3Q") + 8;
                            else if (filePath.Name.IndexOf("2Q") >= 0)
                                dateStart = filePath.Name.IndexOf("2Q") + 8;
                            else if (filePath.Name.IndexOf("1Q") >= 0)
                                dateStart = filePath.Name.IndexOf("1Q") + 8;
                            var queryResult = conn.Query("SELECT [dbo].udf_ExtractNumeric(@FileName) ExtractDate", new { FileName = filePath.Name.Substring(0, dateStart == -1 ? 10 : dateStart) }).SingleOrDefault();
                            logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Extract Anaysis date from File Name(ex. 1Q 2018-> 03/31/2018) - The date is " + queryResult.ExtractDate));
                            string resultOfDateTime = queryResult != null ? queryResult.ExtractDate : "01/01/1900";
                            ExcelPackage excel = new ExcelPackage(filePath);
                            if (resultOfDateTime == "01/01/1900")
                                resultOfDateTime = CheckUmbFileSheetForDate(excel, conn);
                            if (resultOfDateTime != "01/01/1900" && !CheckFileIsUmb(excel))
                            {
                                docId = UploadFileDetailsToDB(dates, conn, filePath, docId, resultOfDateTime);
                                ReadExcelDataProcess(conn, filePath, destFileProcessed, destUnProcessedFiles, destFileName, docId, ref holdingCount, ref fundNameNotMatch, resultOfDateTime);
                            }
                            // Check Umb file Date format from Sheet & Start to process
                            else if (resultOfDateTime != "01/01/1900" && CheckFileIsUmb(excel))
                            {
                                docId = UploadFileDetailsToDB(dates, conn, filePath, docId, resultOfDateTime);
                                ReadUmbExcelDataProcess(conn, filePath, destFileProcessed, destUnProcessedFiles, destFileName, docId, ref holdingCount, ref fundNameNotMatch, resultOfDateTime);
                            }
                            else
                            {
                                logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Fail - File Name Format is Invalid, The File is : " + filePath.Name));
                                conn.Execute("EXEC Website_PE_Admin_UploadHoldingLogs @DocId,@FileName,@Email,@Status,@UploadType,@UpdatedCount,@Message", new { DocId = docId, FileName = destFileName, Email = "Auto", Status = 0, UploadType = "Auto", UpdatedCount = 0, Message = "Upload failed - The File Name or Spread Sheet name may invalid, The file name format should be like  <Quarter> <Year> <Filename>.xlsx(i.e. 1Q 2021 <fundname>.xlsx" });
                                FileMove(filePath, destUnProcessedFiles);
                            }

                        }
                        catch (Exception ex)
                        {
                            logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                            logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PE Holding Excel Upload => Exception on Upload to Azure Container & DB"));
                            conn.Execute("EXEC Website_PE_Admin_UploadHoldingLogs @DocId,@FileName,@Email,@Status,@UploadType,@UpdatedCount,@Message", new { DocId = docId, FileName = destFileName, Email = "Auto", Status = 0, UploadType = "Auto", UpdatedCount = 0, Message = "Upload failed -" + ex.Message });
                            FileMove(filePath, destUnProcessedFiles);
                            UploadHoldingExcelToAzure(destFileName, destUnProcessedFiles);

                        }
                    }
                    UpdateSectorMapping(dates, conn);



                }
                try
                {
                    foreach (string str in dates.Distinct())
                    {
                        using (var conn = new SqlConnection(DataAccess.connectionStringAzure))
                        {
                            DateTime date = Convert.ToDateTime(str);
                            string TableName = "Website_PE_Fact_tblPortfolioHolding" + date.ToString("yyyy") + date.ToString("MM") + date.ToString("dd");
                            var result = conn.Query<PaHoldingExcelData>("EXEC Website_PE_Admin_UpdatePEHoldingsToCwcoin2 @TableName", new { TableName = TableName }).ToList();
                            var fundMapping = conn.Query<FundNameMapping>("SELECT * FROM tbl_PE_FundNameMapping").ToList();
                            using (var connCoin2 = new SqlConnection(DataAccess.connectionStringCoin2))
                            {
                                DynamicParameters param = new DynamicParameters();

                                param.Add("@HoldingXml", value: HoldingListToXml(result, DateTime.Now, "coin2"), dbType: DbType.String, direction: ParameterDirection.Input);
                                param.Add("@UploadedBy", value: "Auto", dbType: DbType.AnsiString, direction: ParameterDirection.Input);
                                var holdingSyncToCoin2 = connCoin2.ExecuteScalar<int>("PE.privateEquity_usp_UploadHoldingDataBySync", param, commandType: CommandType.StoredProcedure, commandTimeout: 1000);
                                if (holdingSyncToCoin2 == 0)
                                    logManagerEvent.LogWarningMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Azure DB To Azure VM - No PE Holding Data update from azure to Azure VM COIN DB - " + str));
                                //else
                                logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Azure DB To Azure VM - Process is done for Updating PE Holding Data from azure to Azure VM COIN DB - " + str + " - Updated Count is ->" + holdingSyncToCoin2));

                                /* Fund Name mapping */
                                var xmlMapping = new XElement("Root",
                                                from doc in fundMapping
                                                select new XElement("Mapping",
                                                             new XElement("cProductId", doc.cProductId),
                                                             new XElement("cPartnershipId", doc.cPartnershipId),
                                                             new XElement("cPartnershipName", doc.cPartnershipName),
                                                             new XElement("cInvestmentName", doc.cInvestmentName),
                                                             new XElement("cNewFundNameMapping", doc.cNewFundNameMapping)));
                                DynamicParameters paramMapping = new DynamicParameters();

                                paramMapping.Add("@FundMapping", value: xmlMapping.ToString(), dbType: DbType.String, direction: ParameterDirection.Input);
                                var holdingSyncMappingToCoin2 = connCoin2.ExecuteScalar<int>("PE.privateEquity_usp_UploadHoldingFundMappingBySync", paramMapping, commandType: CommandType.StoredProcedure, commandTimeout: 1000);
                                logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Azure DB To Azure VM - Fund Mapping from azure to VM Server Private Equity DB - " + str));
                                conn.Execute("UPDATE Pe_Report_VM_Fact_tblPortfolioHoldingUploadStatus SET IsUpdate=1 WHERE Dateasof=@Dateasof", new { Dateasof = date }, commandTimeout: 1000);
                                logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Azure DB To Azure VM Flag status- Holding Updated flag to be set '1' for  - " + date));
                                conn.Query("Website_PE_Admin_Monitor_GetPeUploadHoldingInvestmentComparison_Sync", new { Dateasof = str }, commandType: CommandType.StoredProcedure, commandTimeout: 100000);
                            }
                            /*
                            var fxRates = conn.Query<dynamic>("SELECT * FROM v_Pe_Report_AzureVM_tbl_TCDRS_AllTransUploadStatus").ToList();
                            if (fxRates.Count > 0)
                            {
                                logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Azure DB To Azure VM - Get the Fx rates Update from azure to Azure VM COIN DB"));
                                var xmlFxRates = new XElement("Root", from list in fxRates
                                                                      select new XElement("ExRates",
                                                                                new XElement("Currency", list.InvestmentCurrency),
                                                                                new XElement("ExRate", list.exMIN),
                                                                                new XElement("EffectiveDate", list.EffectiveDate)
                                                                            ));
                                using (var connCoin2 = new SqlConnection(DataAccess.connectionStringCoin2))
                                {
                                    var fxRatesSyncToAzureVM = connCoin2.Execute("PE.Azure_Reports_PE_SaveFxRates", new { SaveXml = xmlFxRates.ToString() }, commandType: CommandType.StoredProcedure, commandTimeout: 1000);
                                    if (fxRatesSyncToAzureVM > 0)
                                    {
                                        logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Azure DB To Azure VM -Updated the Fx rates from azure to Azure VM COIN DB"));
                                        var selectFxratesStatus = conn.Query<string>("SELECT DISTINCT EffectiveDate FROM v_Pe_Report_AzureVM_tbl_TCDRS_AllTransUploadStatus").ToList();
                                        foreach (var effectiveDate in selectFxratesStatus)
                                        {
                                            conn.Execute("UPDATE Pe_Report_AzureVM_tbl_TCDRS_AllTransUploadStatus SET IsUpdate=0 WHERE Dateasof=@Dateasof", new { Dateasof = effectiveDate }, commandTimeout: 1000);
                                        }
                                    }
                                }
                            }*/
                        }
                    }
                }
                catch (Exception ex)
                {
                    logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                    logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PE Holding Excel Upload Exception=> Upload sync to COIN2 Server from Azure"));
                }
                logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Process Ended => ******      PE Holding Excel Upload Process End...     ******"));
            }
            catch (Exception ex)
            {
                logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PE Holding Excel Upload Exception=> Upload sync to COIN2 Server from Azure Main Method"));
            }
            logManagerEvent.Splitter();
        }

        private static int UploadFileDetailsToDB(List<string> dates, SqlConnection conn, FileInfo filePath, int docId, string resultOfDateTime)
        {
            dates.Add(resultOfDateTime);
            var xml = new XElement("Root", new XElement("Document",
                                                          new XElement("FileName", filePath.Name),
                                                          new XElement("Protected", 1),
                                                          new XElement("Title", Path.GetFileNameWithoutExtension(filePath.Name)),
                                                          new XElement("Date", Convert.ToDateTime(resultOfDateTime)),
                                                          new XElement("Type", 12),
                                                          new XElement("ProductId", 0),
                                                          new XElement("PortfolioTools", 0),
                                                          new XElement("HomeSectionType", 0),
                                                          new XElement("SubLinkText", string.Empty)
                                                      ));
            var managerResult = conn.ExecuteScalar<int>(spAzureUploadDocs, new { Xml = xml.ToString(), Email = "Auto" }, commandTimeout: 360000);
            docId = (int)managerResult;
            return docId;
        }
        private static string CheckUmbFileSheetForDate(ExcelPackage package, SqlConnection conn)
        {
            string SheetNameDate = string.Empty;
            foreach (ExcelWorksheet workSheet in package.Workbook.Worksheets.Take(1))
            {
                SheetNameDate = workSheet.Name;
                var queryResult = conn.Query("SELECT [dbo].udf_ExtractNumericForDate(@FileName) ExtractDate", new { FileName = workSheet.Name.Trim() }).SingleOrDefault();
                SheetNameDate = queryResult != null ? queryResult.ExtractDate : "01/01/1900";
            }
            return SheetNameDate;
        }

        private static void ReadUmbExcelDataProcess(SqlConnection conn, FileInfo filePath, string destFileProcessed, string destUnProcessedFiles, string destFileName, int docId, ref int holdingCount, ref string fundNameNotMatch, string resultOfDateTime)
        {
            try
            {
                if (docId > 0)
                {
                    logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Sync PE UMB Excel Doc info Updated to Azure DB Table -(SELECT * FROM tblUploadResearch)"));
                    ExcelPackage excel = new ExcelPackage(filePath);
                    string resultXml = UmbHoldingToXml(excel, Convert.ToDateTime(resultOfDateTime), out holdingCount, out fundNameNotMatch, conn);
                    if (resultXml == string.Empty)
                    {
                        logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, fundNameNotMatch == string.Empty ? "File is Invalid(Error):-> Excel file has multiple funds" : fundNameNotMatch.Replace("@@", ",")));
                        conn.Execute("EXEC Website_PE_Admin_UploadHoldingLogs @DocId,@FileName,@Email,@Status,@UploadType,@UpdatedCount,@Message", new { DocId = docId, FileName = destFileName, Email = "Auto", Status = 0, UploadType = "Auto", UpdatedCount = 0, Message = fundNameNotMatch == string.Empty ? "File is Invalid:-> UMB Excel file has multiple funds, We won't allow multiple fund to upload in single spread sheet" : fundNameNotMatch.Replace("@@", ",") });
                        FileMove(filePath, destUnProcessedFiles);
                        UploadHoldingExcelToAzure(destFileName, destUnProcessedFiles);
                    }
                    else
                    {
                        logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Prepare UMB Excel data XML from List for Bulk Update to Azure"));
                        DynamicParameters param = new DynamicParameters();
                        param.Add("@HoldingXml", value: resultXml, dbType: DbType.String, direction: ParameterDirection.Input);
                        param.Add("@FileName", value: destFileName, dbType: DbType.AnsiString, direction: ParameterDirection.Input);
                        param.Add("@UploadedBy", value: "Auto", dbType: DbType.AnsiString, direction: ParameterDirection.Input);
                        param.Add("@DocId", value: docId, dbType: DbType.Int64, direction: ParameterDirection.Input);
                        var holdingExcelResult = conn.ExecuteScalar<int>(spAzureUpload, param, commandType: CommandType.StoredProcedure, commandTimeout: 3600000);

                        if (holdingExcelResult > 0)
                        {
                            FileMove(filePath, destFileProcessed);
                            UploadHoldingExcelToAzure(destFileName, destFileProcessed);
                        }
                        else if (resultXml.Contains("@@"))
                        {
                            logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Fail - Sync UMB PE Excel Columns are not valid format"));
                            conn.Execute("EXEC Website_PE_Admin_UploadHoldingLogs @DocId,@FileName,@Email,@Status,@UploadType,@UpdatedCount,@Message", new { DocId = docId, FileName = destFileName, Email = "Auto", Status = 0, UploadType = "Auto", UpdatedCount = 0, Message = resultXml.Replace("@@", ",") });
                            FileMove(filePath, destUnProcessedFiles);
                            UploadHoldingExcelToAzure(destFileName, destUnProcessedFiles);

                        }
                        else
                        {
                            logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Fail - No Updation on Sync UMB PE Excel Doc info to Azure DB Table"));
                            conn.Execute("EXEC Website_PE_Admin_UploadHoldingLogs @DocId,@FileName,@Email,@Status,@UploadType,@UpdatedCount,@Message", new { DocId = docId, FileName = destFileName, Email = "Auto", Status = 0, UploadType = "Auto", UpdatedCount = 0, Message = "Fail - No Updation on Sync UMB PE Excel details to Azure DB Table,Contact IT Team" });
                            FileMove(filePath, destUnProcessedFiles);
                            UploadHoldingExcelToAzure(destFileName, destUnProcessedFiles);
                        }
                    }
                }
                else
                {
                    logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Fail - Sync UMB PE Excel Doc info Not Updated to Azure DB Table -(SELECT * FROM tblUploadResearch)"));
                    conn.Execute("EXEC Website_PE_Admin_UploadHoldingLogs @DocId,@FileName,@Email,@Status,@UploadType,@UpdatedCount,@Message", new { DocId = docId, FileName = destFileName, Email = "Auto", Status = 0, UploadType = "Auto", UpdatedCount = 0, Message = "UMB Excel File Name not in correct Format" });
                    FileMove(filePath, destUnProcessedFiles);
                    UploadHoldingExcelToAzure(destFileName, destUnProcessedFiles);
                }
            }
            catch (Exception ex)
            {
                logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PE Holding Excel Upload Exception => Read UMB Excel and process the data"));
            }
        }

        private static void ReadExcelDataProcess(SqlConnection conn, FileInfo filePath, string destFileProcessed, string destUnProcessedFiles, string destFileName, int docId, ref int holdingCount, ref string fundNameNotMatch, string resultOfDateTime)
        {
            try
            {
                if (docId > 0)
                {
                    logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Sync PE Excel Doc info Updated to Azure DB Table -(SELECT * FROM tblUploadResearch)"));
                    ExcelPackage excel = new ExcelPackage(filePath);
                    string resultXml = HoldingToXml(excel, Convert.ToDateTime(resultOfDateTime), out holdingCount, out fundNameNotMatch, conn);
                    if (resultXml == string.Empty)
                    {
                        logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Fail - Sync PE Excel Columns are not valid format"));
                        conn.Execute("EXEC Website_PE_Admin_UploadHoldingLogs @DocId,@FileName,@Email,@Status,@UploadType,@UpdatedCount,@Message", new { DocId = docId, FileName = destFileName, Email = "Auto", Status = 0, UploadType = "Auto", UpdatedCount = 0, Message = fundNameNotMatch == string.Empty ? "File is Invalid:-> Excel file has multiple funds" : fundNameNotMatch.Replace("@@", ",") });
                        FileMove(filePath, destUnProcessedFiles);
                        UploadHoldingExcelToAzure(destFileName, destUnProcessedFiles);
                    }
                    else
                    {
                        logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Prepare XML from List for Bulk Update to Azure"));
                        DynamicParameters param = new DynamicParameters();
                        param.Add("@HoldingXml", value: resultXml, dbType: DbType.String, direction: ParameterDirection.Input);
                        param.Add("@FileName", value: destFileName, dbType: DbType.AnsiString, direction: ParameterDirection.Input);
                        param.Add("@UploadedBy", value: "Auto", dbType: DbType.AnsiString, direction: ParameterDirection.Input);
                        param.Add("@DocId", value: docId, dbType: DbType.Int64, direction: ParameterDirection.Input);
                        var holdingExcelResult = conn.ExecuteScalar<int>(spAzureUpload, param, commandType: CommandType.StoredProcedure, commandTimeout: 3600000);

                        if (holdingExcelResult > 0)
                        {
                            FileMove(filePath, destFileProcessed);
                            UploadHoldingExcelToAzure(destFileName, destFileProcessed);
                        }
                        else if (resultXml.Contains("@@"))
                        {
                            logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Fail - Sync PE Excel Columns are not valid format"));
                            conn.Execute("EXEC Website_PE_Admin_UploadHoldingLogs @DocId,@FileName,@Email,@Status,@UploadType,@UpdatedCount,@Message", new { DocId = docId, FileName = destFileName, Email = "Auto", Status = 0, UploadType = "Auto", UpdatedCount = 0, Message = resultXml.Replace("@@", ",") });
                            FileMove(filePath, destUnProcessedFiles);
                            UploadHoldingExcelToAzure(destFileName, destUnProcessedFiles);

                        }
                        else
                        {
                            logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Fail - No Updation on Sync PE Excel Doc info to Azure DB Table"));
                            conn.Execute("EXEC Website_PE_Admin_UploadHoldingLogs @DocId,@FileName,@Email,@Status,@UploadType,@UpdatedCount,@Message", new { DocId = docId, FileName = destFileName, Email = "Auto", Status = 0, UploadType = "Auto", UpdatedCount = 0, Message = "Fail - No Updation on Sync PE Excel Doc info to Azure DB Table" });
                            FileMove(filePath, destUnProcessedFiles);
                            UploadHoldingExcelToAzure(destFileName, destUnProcessedFiles);
                        }
                    }
                }
                else
                {
                    logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Fail - Sync PE Excel Doc info Not Updated to Azure DB Table -(SELECT * FROM tblUploadResearch)"));
                    conn.Execute("EXEC Website_PE_Admin_UploadHoldingLogs @DocId,@FileName,@Email,@Status,@UploadType,@UpdatedCount,@Message", new { DocId = docId, FileName = destFileName, Email = "Auto", Status = 0, UploadType = "Auto", UpdatedCount = 0, Message = "File Name not in correct Format" });
                    FileMove(filePath, destUnProcessedFiles);
                    UploadHoldingExcelToAzure(destFileName, destUnProcessedFiles);
                }
            }
            catch (Exception ex)
            {
                logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PE Holding Excel Upload Exception => Read Excel and process the data"));
            }
        }

        private static void UpdateSectorMapping(List<string> dates, SqlConnection conn)
        {
            try
            {

                foreach (string str in dates.Distinct())
                {
                    DateTime date = Convert.ToDateTime(str);
                    conn.Execute("EXEC Website_PE_Admin_UpdatePEHoldingsFundAndSectorMapping @TableName,@Dateasof", new { TableName = "Website_PE_Fact_tblPortfolioHolding" + date.Year + "" + date.Month.ToString().PadLeft(2, '0') + "" + date.Day.ToString().PadLeft(2, '0'), Dateasof = date });
                    logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Azure DB Update - Fund & Sector mapping for PA holding"));

                }
            }
            catch (Exception ex)
            {
                logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PE Holding Excel Upload => Fund Name & Sector mapping"));
            }
        }

        private static void FileMove(FileInfo filePath, string destFileProcessed)
        {
            logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PE Holding Excel File Move => File From => '" + filePath.FullName + "' <=> File To => " + destFileProcessed));
            File.Move(filePath.FullName, destFileProcessed);
        }


        private static void UploadHoldingExcelToAzure(string targetFileName, string excelFilePath)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(Settings.AzureConnectionString);
                logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PE Holding Excel Upload to Azure => " + targetFileName));

                if (File.Exists(excelFilePath))
                {
                    // var targetFileName = fileName;// $"{file.DocId}{fileInfo.Extension}";
                    //logger.Debug($"Uploading file for docid {file.DocId} to {targetFileName}...");
                    CloudFileClient fileClient = storageAccount.CreateCloudFileClient();
                    var share = fileClient.GetShareReference(Settings.UploadFolder);
                    var cloudFile = share.GetRootDirectoryReference().GetFileReference(targetFileName);
                    cloudFile.Properties.ContentDisposition = "attachment; filename=" + targetFileName;
                    try
                    {
                        cloudFile.Delete();
                    }
                    catch
                    {
                    }
                    cloudFile.UploadFromFile(excelFilePath);
                }
            }
            catch (Exception ex)
            {
                logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PE Holding Excel Upload => Exception on Upload to Azure"));
            }
        }

       

        public static string UmbHoldingToXml(ExcelPackage package, DateTime date, out int holdingDataCount, out string fundNameNotMatch, SqlConnection conn)
        {

            fundNameNotMatch = string.Empty;
            List<PaHoldingExcelData> holdingDataList = new List<PaHoldingExcelData>();
            holdingDataCount = 0;
            try
            {
                StringBuilder builder = new StringBuilder();
                foreach (ExcelWorksheet workSheet in package.Workbook.Worksheets.Take(1))
                {
                    var headerrow = workSheet.Cells[1, 1, 1, workSheet.Dimension.End.Column];
                    string checkValidColumns = CheckUmbValidColumn(headerrow);
                    if (checkValidColumns.Contains("@@"))
                    {
                        logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Check Excel file format is Valid or Not(May be In Order of Coulmn or Column Name wrong): The Failed Columns Follows -> " + checkValidColumns));
                        
                        return checkValidColumns;
                    }
                    for (var rowNumber = 2; rowNumber <= workSheet.Dimension.End.Row; rowNumber++)
                    {
                        try
                        {
                            var row = workSheet.Cells[rowNumber, 1, rowNumber, workSheet.Dimension.End.Column];
                            if (Convert.ToString(row[rowNumber, 1].Text).Length >= 2)
                            {
                                bool checkValidate = false;
                                if (Convert.ToString(row[rowNumber, 2].Text).Trim() == "")
                                {
                                    builder.Append("Investment Name is blank in Cell (B" + rowNumber+")");
                                    checkValidate = true;
                                }
                                if (Convert.ToString(row[rowNumber, 3].Text).Trim() == "")
                                {
                                    builder.Append("@@Entity is blank in Cell (C" + rowNumber + ")");
                                    checkValidate = true;
                                }
                                if (Convert.ToString(row[rowNumber, 11].Text).Trim() == "")
                                {
                                    builder.Append("@@Country is blank in Cell (K" + rowNumber + ")");
                                    checkValidate = true;
                                }
                                if (Convert.ToString(row[rowNumber, 13].Text).Trim() == "")
                                {
                                    builder.Append("@@Sector(Industry) is blank in Cell (M" + rowNumber + ")");
                                    checkValidate = true;
                                }
                                if (Convert.ToString(row[rowNumber, 14].Text).Trim() == "")
                                {
                                    builder.Append("@@Investment Stage is blank in Cell (N" + rowNumber + ")");
                                    checkValidate = true;
                                }
                               /* if (row[rowNumber, 7].Text.ToString().IsNumeric() == false)
                                {
                                    builder.Append("@@RemainingCost has alphanumeric characters in Column (G" + rowNumber + ")");
                                    checkValidate = true;
                                }
                                if (row[rowNumber, 8].Text.ToString().IsNumeric() == false)
                                {
                                    builder.Append("@@Market Value has alphanumeric characters in Column (H" + rowNumber + ")");
                                    checkValidate = true;
                                }
                                if (row[rowNumber, 9].Text.ToString().IsNumeric() == false)
                                {
                                    builder.Append("@@Total Invested has alphanumeric characters in Column (I" + rowNumber + ")");
                                    checkValidate = true;
                                }*/

                                if (row[rowNumber, 7].Text.ToString().Trim() != "-" && row[rowNumber, 7].Text.ToString().Trim() != "")
                                {
                                    //decimal val;
                                    //var dd = decimal.TryParse(row[rowNumber, 6].Text.ToString(), out val);
                                    if (row[rowNumber, 7].Text.ToString().Trim().IsNumeric() == false && !row[rowNumber, 7].Value.ToString().Trim().IsNumeric())
                                    {
                                        if (row[rowNumber, 7].Text.ToString().Contains(" "))
                                            builder.Append("@@Remaining Cost has space in the middle of #'s, the Cell is (G" + rowNumber + ")");
                                        else
                                            builder.Append("@@Remaining Cost has alphanumeric characters in Cell (G" + rowNumber + ")");
                                        checkValidate = true;
                                    }
                                }
                                if (row[rowNumber, 8].Text.ToString().Trim() != "-" && row[rowNumber, 8].Text.ToString().Trim() != "")
                                {
                                    if (row[rowNumber, 8].Text.ToString().Trim().IsNumeric() == false && !row[rowNumber, 8].Value.ToString().Trim().IsNumeric())
                                    {
                                        if (row[rowNumber, 8].Text.ToString().Contains(" "))
                                            builder.Append("@@Market Value(" + row[rowNumber, 8].Text + ") has space in the middle of #'s, the Cell is (H" + rowNumber + ")");
                                        else
                                            builder.Append("@@Market Value(" + row[rowNumber, 8].Text + ") has alphanumeric characters in Cell (H" + rowNumber + ")");
                                        checkValidate = true;
                                    }
                                }
                                if (row[rowNumber, 9].Text.ToString().Trim() != "-" && row[rowNumber, 9].Text.ToString().Trim() != "")
                                {
                                    if (row[rowNumber, 9].Text.ToString().Trim().IsNumeric() == false && !row[rowNumber, 9].Value.ToString().Trim().IsNumeric())
                                    {
                                        if (row[rowNumber, 9].Text.ToString().Contains(" "))
                                            builder.Append("@@Total Invested(" + row[rowNumber, 9].Text + ") has space in the middle of #'s, the Cell is  (I" + rowNumber + ")");
                                        else
                                            builder.Append("@@Total Invested(" + row[rowNumber, 9].Text + ") has alphanumeric characters in Cell (I" + rowNumber + ")");
                                        checkValidate = true;
                                    }
                                }
                                if (!checkValidate)
                                {                                                             
                                       string mvDate=Convert.ToString(row[rowNumber, 6].Value.ToString());
                                        if(mvDate.Replace("-","/").Count(x=>x=='/')<2)
                                            mvDate="01/01/1900";
                                        PaHoldingExcelData holdingData = new PaHoldingExcelData()
                                        {
                                            AsOfDate = date.ToString("MM/dd/yyyy"),
                                            Investment = Convert.ToString(row[rowNumber, 2].Text).Trim(),//.Replace("'", "@$"),
                                            Entity = Convert.ToString(row[rowNumber, 3].Text).Trim(),//.Replace("'", "@$"),
                                            Public = Convert.ToString(row[rowNumber, 4].Text).Trim(),
                                            BaseCurrency = Convert.ToString(row[rowNumber, 5].Text).Trim(),
                                            MarketValueDate = mvDate==""?"01/01/1900":mvDate,
                                            RemainingCost = row[rowNumber, 7].Value == null ? "0" : Convert.ToString(row[rowNumber, 7].Value).Trim(),
                                            MarketValue = row[rowNumber, 8].Value == null ? "0" : Convert.ToString(row[rowNumber, 8].Value).Trim(),
                                            TotalInvested = row[rowNumber, 9].Value == null ? "0" : Convert.ToString(row[rowNumber, 9].Value).Trim(),
                                            //&& row[rowNumber, 9].Text.Length>4 Included 03 JAN 2020
                                            InitialInvestmentDate = row[rowNumber, 10].Value == null ? "01/01/1900" : Convert.ToString(row[rowNumber, 10].Value.ToString()).Contains(@"/") && row[rowNumber, 10].Text.Contains(@"/") ? Convert.ToString(row[rowNumber, 10].Text.ToString()) : Convert.ToString(row[rowNumber, 10].Value.ToString()),
                                            Country = Convert.ToString(row[rowNumber, 11].Text).Trim(),
                                            State = Convert.ToString(row[rowNumber, 12].Text).Trim(),
                                            Industry = Convert.ToString(row[rowNumber, 13].Text).Trim(),
                                            InvestmentStage = Convert.ToString(row[rowNumber, 14].Text).Trim(),
                                            ExitDate = Convert.ToString(row[rowNumber, 15].Text),
                                            CurrentStatus = Convert.ToString(row[rowNumber, 16].Text)           //.Replace("'", "@$"),
                                        };
                                
                                holdingDataList.Add(holdingData);
                                }
                            }
                            else
                            {
                                bool flagRowEmpty = false;
                                int count = 1;
                                for (var column = 1; column <= workSheet.Dimension.End.Column; column++)
                                {
                                    if (row[rowNumber, column].Value != null)
                                    {
                                        flagRowEmpty = true;
                                        count++;
                                        if (count >= 5)
                                            break;
                                    }
                                }
                                if (!flagRowEmpty && count<5 && count>1)
                                {
                                    builder.Append("Investment Name is blank in Cell (B" + rowNumber + ")");
                                }
                                else
                                    break;
                            }
                                
                        }
                        catch (Exception ex)
                        {
                            logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                            logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Failed to get the UMB Excel Data - The Rwo Number is -> " + rowNumber));
                        }
                    }
                    // table.Rows.Add(newRow);
                }
                logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "UMB Holding Data Count from Excel to List : The Count is -> " + holdingDataList.Count));
                var holdingDataListCheck = holdingDataList.Where(x => x.Investment.Length > 2).Select(x => x.Investment.ToString()).Distinct().ToList();
                holdingDataCount = holdingDataList.Count;
                if (holdingDataListCheck.Count() > 0)
                {                    
                    var fundResult = conn.Query<dynamic>("EXEC Website_PE_Admin_UMBHoldingUploadFundNameMappingCheck @Funds", new { Funds = string.Join("|", holdingDataListCheck.ToArray()) });
                    if (fundResult.Count() > 0)
                    {
                        string resultCheck = fundResult.FirstOrDefault().FundNameCheck;
                        if (resultCheck == string.Empty)
                            fundNameNotMatch = "File is Invalid(Error) -> All Investment name positions(Cell B) not matched with Backstop Fund name. Please update correct fund name and reupload it";                      
                        else if (resultCheck == "Success")
                            fundNameNotMatch = "";
                        else
                            fundNameNotMatch = "The following Investment name(Cell B) positions in UMB Excel not matching with Backstop Fund Name, They are following " + resultCheck.Split('|')[1] + ". Please update correct fund name and reupload it";
                    }                    
                }
                if (builder.ToString() != "")
                    fundNameNotMatch = "File is Invalid-> " + builder.ToString().TrimStart('@').TrimStart('@') + ", Please correct these issues and reupload it.";

            }
            catch (Exception ex)
            {
                logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Failed to get or read the UMB Excel Data"));
            }
            return fundNameNotMatch != string.Empty ? string.Empty : HoldingListToXml(holdingDataList, date, "1");
        }

       

        public static string HoldingToXml(ExcelPackage package, DateTime date, out int holdingDataCount, out string fundNameNotMatch, SqlConnection conn)
        {
            try
            {
                fundNameNotMatch = string.Empty;
                List<PaHoldingExcelData> holdingDataList = new List<PaHoldingExcelData>();
                StringBuilder builder = new StringBuilder();
                foreach (ExcelWorksheet workSheet in package.Workbook.Worksheets.Take(1))
                {

                    var headerrow = workSheet.Cells[1, 1, 1, workSheet.Dimension.End.Column];
                    string checkValidColumns = CheckValidColumn(headerrow);
                    if (checkValidColumns.Contains("@@"))
                    {
                        logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "File format is InValid(May be the Order of Column or Column Name wrong): The Failed Columns Follows -> " + checkValidColumns));
                        holdingDataCount = 0;
                        return checkValidColumns;
                    }
                    for (var rowNumber = 2; rowNumber <= workSheet.Dimension.End.Row; rowNumber++)
                    {
                        try
                        {
                            var row = workSheet.Cells[rowNumber, 1, rowNumber, workSheet.Dimension.End.Column];
                            if (Convert.ToString(row[rowNumber, 1].Text).Length >= 2)
                            {
                                bool checkValidate = false;
                                if (Convert.ToString(row[rowNumber, 1].Text).Trim() == "")
                                {
                                    builder.Append("Name is blank in Cell (A" + rowNumber + ")");
                                    checkValidate = true;
                                }
                                if (Convert.ToString(row[rowNumber, 2].Text).Trim() == "")
                                {
                                    builder.Append("@@Entity is blank in Cell (B" + rowNumber + ")");
                                    checkValidate = true;
                                }
                                if (Convert.ToString(row[rowNumber, 10].Text).Trim() == "")
                                {
                                    builder.Append("@@Country is blank in Cell (J" + rowNumber + ")");
                                    checkValidate = true;
                                }
                                if (Convert.ToString(row[rowNumber, 12].Text).Trim() == "")
                                {
                                    builder.Append("@@Sector(Industry) is blank in Cell (L" + rowNumber + ")");
                                    checkValidate = true;
                                }
                                if (Convert.ToString(row[rowNumber, 13].Text).Trim() == "")
                                {
                                    builder.Append("@@Investment Stage is blank in Cell (M" + rowNumber + ")");
                                    checkValidate = true;
                                }
                                if (row[rowNumber, 6].Text.ToString().Trim() != "-" && row[rowNumber, 6].Text.ToString().Trim() != "")
                                {
                                   // decimal val;
                                    //var dd = decimal.TryParse(row[rowNumber, 6].Text.ToString(),out val);
                                    if (row[rowNumber, 6].Text.ToString().Trim().IsNumeric() == false && !row[rowNumber, 6].Value.ToString().Trim().IsNumeric())
                                    {
                                        if (row[rowNumber, 6].Text.ToString().Contains(" "))
                                            builder.Append("@@Remaining Cost(" + row[rowNumber, 6].Text + ") has space in the middle of #'s, the Cell (F" + rowNumber + ")");
                                        else
                                            builder.Append("@@Remaining Cost(" + row[rowNumber, 6].Text + ") has alphanumeric characters in Cell (F" + rowNumber + ")");
                                        checkValidate = true;
                                    }
                                }
                                if (row[rowNumber, 7].Text.ToString().Trim() != "-" && row[rowNumber, 7].Text.ToString().Trim() != "")
                                {
                                    if (row[rowNumber, 7].Text.ToString().Trim().IsNumeric() == false  && !row[rowNumber, 7].Value.ToString().Trim().IsNumeric())
                                    {
                                        if (row[rowNumber, 7].Text.ToString().Contains(" "))
                                            builder.Append("@@Market Value(" + row[rowNumber, 7].Text + ") has space in the middle of #'s, the Cell is (G" + rowNumber + ")");
                                        else
                                            builder.Append("@@Market Value(" + row[rowNumber, 7].Text + ") has alphanumeric characters in Cell (G" + rowNumber + ")");
                                        checkValidate = true;
                                    }
                                }
                                if (row[rowNumber, 8].Text.ToString().Trim() != "-" && row[rowNumber, 8].Text.ToString().Trim()!="")
                                {
                                    if (row[rowNumber, 8].Text.ToString().Trim().IsNumeric() == false && !row[rowNumber, 8].Value.ToString().Trim().IsNumeric())
                                    {
                                        if (row[rowNumber, 8].Text.ToString().Contains(" "))
                                            builder.Append("@@Total Invested(" + row[rowNumber, 8].Text + ") has space in the middle of #'s, the Cell is  (H" + rowNumber + ")");
                                        else
                                            builder.Append("@@Total Invested(" + row[rowNumber, 8].Text + ") has alphanumeric characters in Cell (H" + rowNumber + ")");
                                        checkValidate = true;
                                    }
                                }
                                if (!checkValidate)
                                {

                                    string mvDate = Convert.ToString(row[rowNumber, 6].Value.ToString());
                                    if (mvDate.Replace("-", "/").Count(x => x == '/') < 2)
                                        mvDate = "01/01/1900";
                                    PaHoldingExcelData holdingData = new PaHoldingExcelData()
                                        {
                                            AsOfDate = date.ToString("MM/dd/yyyy"),

                                            Investment = Convert.ToString(row[rowNumber, 1].Text).Trim(),//.Replace("'", "@$"),
                                            Entity = Convert.ToString(row[rowNumber, 2].Text).Trim(),//.Replace("'", "@$"),
                                            Public = Convert.ToString(row[rowNumber, 3].Text).Trim(),
                                            BaseCurrency = Convert.ToString(row[rowNumber, 4].Text).Trim(),
                                            MarketValueDate = Convert.ToString(row[rowNumber, 5].Value.ToString()),
                                            RemainingCost = row[rowNumber, 6].Value == null ? "0" : Convert.ToString(row[rowNumber, 6].Value).Trim(),
                                            MarketValue = row[rowNumber, 7].Value == null ? "0" : Convert.ToString(row[rowNumber, 7].Value).Trim(),
                                            TotalInvested = row[rowNumber, 8].Value == null ? "0" : Convert.ToString(row[rowNumber, 8].Value).Trim(),
                                            //&& row[rowNumber, 9].Text.Length>4 Included 03 JAN 2020
                                            InitialInvestmentDate = row[rowNumber, 9].Value == null ? "01/01/1900" : Convert.ToString(row[rowNumber, 9].Value.ToString()).Contains(@"/") && row[rowNumber, 9].Text.Contains(@"/") ? Convert.ToString(row[rowNumber, 9].Text.ToString()) : Convert.ToString(row[rowNumber, 9].Value.ToString()),
                                            Country = Convert.ToString(row[rowNumber, 10].Text).Trim(),
                                            State = Convert.ToString(row[rowNumber, 11].Text).Trim(),
                                            Industry = Convert.ToString(row[rowNumber, 12].Text).Trim(),
                                            InvestmentStage = Convert.ToString(row[rowNumber, 13].Text).Trim(),
                                            ExitDate = Convert.ToString(row[rowNumber, 14].Text),
                                            CurrentStatus = Convert.ToString(row[rowNumber, 15].Text)           //.Replace("'", "@$"),
                                        };
                                    holdingDataList.Add(holdingData);
                                }
                            }
                            else
                            {
                                bool flagRowEmpty=false;
                                int count = 1;
                                for (var column = 1; column <= workSheet.Dimension.End.Column; column++)
                                {
                                    if (row[rowNumber, column].Value != null)
                                    {
                                        flagRowEmpty = true;
                                        count++;
                                        if (count >= 5)
                                            break;
                                    }
                                }
                                if (!flagRowEmpty && count<5 && count>1)
                                {
                                    builder.Append("Name is blank in Cells (A" + rowNumber + ")");
                                    
                                }
                                else
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                            logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Failed to get the Excel Data - The Rwo Number is -> " + rowNumber));
                        }
                    }
                    // table.Rows.Add(newRow);
                }
                logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding Data Count from Excel to List : The Count is -> " + holdingDataList.Count));


                var holdingDataListCheck = holdingDataList.Where(x => x.Investment.Length > 2).Select(x => new { Investment = x.Investment }).Distinct();
                holdingDataCount = holdingDataList.Count;
                if (holdingDataListCheck.Count() == 1)
                {
                    //var fundResult= conn.Query<dynamic>("SELECT cFundName FROM vtbl_PE_PartnershipMaster WHERE cFundName=@Fund",new {Fund=holdingDataListCheck.FirstOrDefault().Investment}) ;
                    var fundResult = conn.Query<dynamic>("EXEC Website_PE_Admin_HoldingUploadFundNameMappingCheck @Fund", new { Fund = holdingDataListCheck.FirstOrDefault().Investment });
                    fundNameNotMatch = fundResult.Count() == 0 ? "File is Invalid-> Fund name position(Column A) not matched with Backstop Fund name" : "";

                }
                if (builder.ToString() != "" && fundNameNotMatch == "")
                    fundNameNotMatch = "File is Invalid-> " + builder.ToString().TrimStart('@').TrimStart('@') + ", Please correct these issues and reupload it.";
                return fundNameNotMatch != string.Empty ? string.Empty : HoldingListToXml(holdingDataList, date);
            }
            catch (Exception ex)
            {
                logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception => Failed to read the excle data"));
                fundNameNotMatch = "Exception => Failed to read the excle data";
                holdingDataCount = 0;
                return fundNameNotMatch;
            }
        }
       
        private static bool CheckFileIsUmb(ExcelPackage package)
        {
            bool checkUmbFlag=false;
            foreach (ExcelWorksheet workSheet in package.Workbook.Worksheets.Take(1))
            {
                var headerrow = workSheet.Cells[1, 1, 1, workSheet.Dimension.End.Column];
                if (Convert.ToString(headerrow[1, 2].Text).ToLower().Trim() == "investment name")
                    checkUmbFlag = true;
                else
                    checkUmbFlag = false;

            }
            return checkUmbFlag;
        }
        private static string CheckUmbValidColumn(ExcelRange range)
        {
            string messageValid = "Error : The UMB Excel column name or position of Column may be Invalid, (Invalid columns are) ";
            if (Convert.ToString(range[1, 2].Text).ToLower().Trim() != "investment name")
                messageValid = messageValid + "Column 'B1' Title must be 'Investment Name'@@";
            if (Convert.ToString(range[1, 3].Text).ToLower().Trim() != "holding name")
                messageValid = messageValid + "Column 'C1' Title must be 'Holding Name'@@";
            if (Convert.ToString(range[1, 4].Text).ToLower().Trim() != "public")
                messageValid = messageValid + "Column 'D1' Title must be 'Public'@@";
            if (Convert.ToString(range[1, 5].Text).ToLower().Trim() != "currency")
                messageValid = messageValid + "Column 'E1' Title must be 'Currency'@@";
            if (Convert.ToString(range[1, 6].Text).ToLower().Trim() != "market value date")
                messageValid = messageValid + "Column 'F1' Title must be 'Market Value Date'@@";
            if (Convert.ToString(range[1, 7].Text).ToLower().Trim() != "current remaining cost")
                messageValid = messageValid + "Column 'G1' Title must be 'Current Remaining Cost'@@";
            if (Convert.ToString(range[1, 8].Text).ToLower().Trim() != "current market value")
                messageValid = messageValid + "Column 'H1' Title must be 'Current Market Value'@@";
            if (Convert.ToString(range[1, 9].Text).ToLower().Trim() != "total invested")
                messageValid = messageValid + "Column 'I1' Title must be 'Total Invested'@@";
            if (Convert.ToString(range[1, 10].Text).ToLower().Trim() != "initial investment date")
            {
                if (Convert.ToString(range[1, 10].Text).ToLower().Trim() != "initital investment date")
                    messageValid = messageValid + "Column 'J1' Title must be 'Initial Investment Date'@@";
            }
            if (Convert.ToString(range[1, 10].Text).ToLower().Trim() != "initital investment date") 
            {
                if (Convert.ToString(range[1, 10].Text).ToLower().Trim() != "initial investment date")
                    messageValid = messageValid + "Column 'J1' Title must be 'Initial Investment Date'@@";
            }
            if (Convert.ToString(range[1, 11].Text).ToLower().Trim() != "country")
                messageValid = messageValid + "Column 'K1' Title must be 'Country'@@";
            if (Convert.ToString(range[1, 12].Text).ToLower().Trim() != "state")
                messageValid = messageValid + "Column 'L1' Title must be 'State'@@";
            if (Convert.ToString(range[1, 13].Text).ToLower().Trim() != "sector")
                messageValid = messageValid + "Column 'M1' Title must be 'Sector'@@";
            if (Convert.ToString(range[1, 14].Text).ToLower().Trim() != "investment stage")
                messageValid = messageValid + "Column 'N1' Title must be 'Investment Stage'@@";
            if (Convert.ToString(range[1, 15].Text).ToLower().Trim() != "exitdate")
            {
                if (Convert.ToString(range[1, 15].Text).ToLower().Trim() != "exit date")
                    messageValid = messageValid + "Column 'O1' Title must be 'Exit Date'@@";
            }
            if (Convert.ToString(range[1, 15].Text).ToLower().Trim() != "exit date")
            {
                if (Convert.ToString(range[1, 15].Text).ToLower().Trim() != "exitdate")
                    messageValid = messageValid + "Column 'O1' Title must be 'Exit Date'@@";
            }
            if (Convert.ToString(range[1, 16].Text).ToLower().Trim() != "status")
                messageValid = messageValid + "Column 'P1' Title must be 'Status'@@";
            return messageValid;
        }

        private static string CheckValidColumn(ExcelRange range)
        {
            string messageValid = "Error : The Excel column name or position of Column may be Invalid, (Invalid columns are) ";
            if (Convert.ToString(range[1, 1].Text).ToLower().Trim() != "fund name")
                messageValid = messageValid + "Column 'A1' Title must be 'Fund Name'@@";
            if (Convert.ToString(range[1, 2].Text).ToLower().Trim() != "holding name")
                messageValid = messageValid + "Column 'B1' Title must be 'Holding Name'@@";
            if (Convert.ToString(range[1, 3].Text).ToLower().Trim() != "public")
                messageValid = messageValid + "Column 'C1' Title must be 'Public'@@";
            if (Convert.ToString(range[1, 4].Text).ToLower().Trim() != "currency")
                messageValid = messageValid + "Column 'D1' Title must be 'Currency'@@";
            if (Convert.ToString(range[1, 5].Text).ToLower().Trim() != "market value date")
                messageValid = messageValid + "Column 'E1' Title must be 'Market Value Date'@@";
            if (Convert.ToString(range[1, 6].Text).ToLower().Trim() != "current remaining cost")
                messageValid = messageValid + "Column 'F1' Title must be 'Current Remaining Cost'@@";
            if (Convert.ToString(range[1, 7].Text).ToLower().Trim() != "current market value")
                messageValid = messageValid + "Column 'G1' Title must be 'Current Market Value'@@";
            if (Convert.ToString(range[1, 8].Text).ToLower().Trim() != "total invested")
                messageValid = messageValid + "Column 'H1' Title must be 'Total Invested'@@";
            if (Convert.ToString(range[1, 9].Text).ToLower().Trim() != "initial investment date")
            {
                if (Convert.ToString(range[1, 9].Text).ToLower().Trim() != "initital investment date")
                    messageValid = messageValid + "Column 'I1' Title must be 'Initial Investment Date'@@";
            }
            if (Convert.ToString(range[1, 9].Text).ToLower().Trim() != "initital investment date")
            {
                if (Convert.ToString(range[1, 9].Text).ToLower().Trim() != "initial investment date")
                    messageValid = messageValid + "Column 'I1' Title must be 'Initial Investment Date'@@";
            }
            if (Convert.ToString(range[1, 10].Text).ToLower().Trim() != "country")
                messageValid = messageValid + "Column 'J1' Title must be 'Country'@@";
            if (Convert.ToString(range[1, 11].Text).ToLower().Trim() != "state")
                messageValid = messageValid + "Column 'K1' Title must be 'State'@@";
            if (Convert.ToString(range[1, 12].Text).ToLower().Trim() != "sector")
                messageValid = messageValid + "Column 'L1' Title must be 'Sector'@@";
            if (Convert.ToString(range[1, 13].Text).ToLower().Trim() != "investment stage")
                messageValid = messageValid + "Column 'M1' Title must be 'Investment Stage'@@";
            if (Convert.ToString(range[1, 14].Text).ToLower().Trim() != "exitdate")
            {
                if (Convert.ToString(range[1, 14].Text).ToLower().Trim() != "exit date")
                    messageValid = messageValid + "Column 'N1' Title must be 'ExitDate'@@";
            }
            if (Convert.ToString(range[1, 14].Text).ToLower().Trim() != "exit date")
            {
                if (Convert.ToString(range[1, 14].Text).ToLower().Trim() != "exitdate")
                    messageValid = messageValid + "Column 'N1' Title must be 'ExitDate'@@";
            }
            if (Convert.ToString(range[1, 15].Text).ToLower().Trim() != "status")
                messageValid = messageValid + "Column 'O1' Title must be 'Status'@@";
            return messageValid;
        }

        private static string HoldingListToXml(List<PaHoldingExcelData> holdingDataList, DateTime date, string coinDbCheck = "")
        {
            var holdingDataListCheck = holdingDataList.Where(x => x.Investment.Length > 2).Select(x => new { Investment = x.Investment }).Distinct();
            if (holdingDataListCheck.Count() > 1 && coinDbCheck == "")
            {
                return "";
            }

            else
            {

                var holdingDataXml = new XElement("Root",
                                                from doc in holdingDataList
                                                select new XElement("Holding",
                                                             new XElement("AsOfDate", doc.AsOfDate),
                                                             new XElement("Investment", doc.Investment),
                                                             new XElement("BaseCurrency", doc.BaseCurrency),
                                                             new XElement("Entity", doc.Entity),
                                                             new XElement("InitialInvestmentDate", doc.InitialInvestmentDate),
                                                             new XElement("MarketValueDate", doc.MarketValueDate),
                                                             new XElement("TotalInvested", doc.TotalInvested),
                                                             new XElement("RemainingCost", doc.RemainingCost),
                                                             new XElement("MarketValue", doc.MarketValue),
                                                             new XElement("Country", doc.Country),
                                                             new XElement("State", doc.State),
                                                             new XElement("Industry", doc.Industry),
                                                             new XElement("InitialInvestmentStage", doc.InvestmentStage),
                                                             new XElement("Public", doc.Public),
                                                             new XElement("ExitDate", doc.ExitDate),
                                                             new XElement("CurrentStatus", doc.CurrentStatus)


                                                 ));
                return holdingDataXml.ToString();
            }
        }


    }
    public static class Extensions
    {
        public static bool IsNumeric(this string s)
        {
            var doubleValue = 0.0;

            //foreach (char c in s)
            //{
            //    if (!char.IsDigit(c) && c != '.')
            //    {
            //        return false;
            //    }
            //}

            return double.TryParse(s.Replace(",",""), out doubleValue);
        }
    }
    public class PaHoldingExcelData
    {
        public string PortfolioHoldingId { get; set; }
        public string AsOfDate { get; set; }
        public string Portfolio { get; set; }
        public string Investment { get; set; }
        public string Entity { get; set; }
        public string CurrentStatus { get; set; }
        public string InitialInvestmentDate { get; set; }
        public string MarketValueDate { get; set; }
        public string TotalInvested { get; set; }
        public string RemainingCost { get; set; }
        public string MarketValue { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string Industry { get; set; }
        public string InvestmentStage { get; set; }
        public string ExitDate { get; set; }
        public string Public { get; set; }
        public string BaseToLocal { get; set; }
        public string BaseCurrency { get; set; }
        //public string OwnerShip { get; set; }
    }
    public class FundNameMapping
    {
        public int cProductId { get; set; }
        public int cPartnershipId { get; set; }
        public string cPartnershipName { get; set; }
        public string cInvestmentName { get; set; }
        public string cNewFundNameMapping { get; set; }
    }
    public class PAIrrTvpi
    {
        public string AssetClass { get; set; }
        public string Strategy { get; set; }
        public string Analyst { get; set; }
        public string IRR { get; set; }
        public string TVPI { get; set; }
        public string DPI { get; set; }
        public DateTime EffectiveDate { get; set; }
        public int FundId { get; set; }
        public string FundName { get; set; }
        public int RowNumber { get; set; }
        public string Qtr { get; set; }
        public float FairValue { get; set; }
        public float QuarterlyContribution { get; set; }
        public float QuarterlyDistribution { get; set; }
    }

    public class PortfolioHoldingInvestment
    {
        public int Order { get; set; }
        public string Entity { get; set; }
        public string Industry { get; set; }
        public string Country { get; set; }
        public string Private { get; set; }
        public string InvestmentDate { get; set; }
        public string Invested { get; set; }
        public string Realized { get; set; }
        public string FairValue { get; set; }
        public string FairValuePrev { get; set; }
        public string DateasofCurr { get; set; }
        public string DateasofPrev { get; set; }
        public string FairValueDiff { get; set; }
        public string moic { get; set; }
        public string Total { get; set; }
        public string TotalPercent { get; set; }
        public string RemainingCost { get; set; }
    }

}
