﻿using MimeKit;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CwAzureApplications.BAL.Services
{
    public class EmailMessage
    {
        public List<MailboxAddress> MailTo { get; }
        public string Subject { get; }
        public string Content { get; }
        public List<Attachment> Attachments { get; }


        public EmailMessage(IEnumerable<string> to, string subject, string content)
        {
            //Attachments = new List<Attachment>();
            MailTo = new List<MailboxAddress>();
            MailTo.AddRange(to.Select(x => new MailboxAddress("test", x)));
            Subject = subject;
            Content = content;
        }
        /*
        public EmailMessage AddAttachment(string Name, string Content)
        {
            var fileType = Path.GetExtension(Name);
            if (fileType.Contains(".") == true)
                fileType = fileType.Replace(".", string.Empty);

            var AttchmentType = FileHelper.fileTypes.FirstOrDefault(s => s.Value.ToString() == fileType).Key;
            var mediaType = AttchmentType.Split('/')?[0] ?? "application";
            var subType = AttchmentType.Split('/')?[1] ?? "text";

            Attachments.Add(new Attachment { Content = Content, MediaType = mediaType, Name = Name, SubType = subType });
            return this;
        }
         */
    }

    public class Attachment
    {
        public string MediaType { get; set; }
        public string SubType { get; set; }
        public string Content { get; set; }
        public string Name { get; set; }
    }
}