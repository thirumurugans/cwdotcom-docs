﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using CwAzureApplications.BAL.Model;
using CwAzureApplications.BAL.Services;

namespace CwAzureApplications.BAL.Email
{
    public class BuildEmailMessage : IBuildEmailMessage
    {
        private readonly EmailSetting _emailConfig;
        private readonly string _templateFileDirectory;
        public BuildEmailMessage(EmailSetting emailConfig)
        {
            _emailConfig = emailConfig;
            _templateFileDirectory = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "EmailTemplates");

        }

        public EmailMessage UMBPowerStationReportMessage(UMBReportType report, List<CCLFUploadRequest> reportFiles)
        {
            string subject = "";//"UMB {report} Report {DateTime.Today.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture)}";

            StringBuilder reportTable = new StringBuilder();

            int i = 1;
            if (reportFiles.Count > 0)
            {
                foreach (var item in reportFiles)
                {
                    reportTable.Append("<tr><td>" + i + "</td><td>" + item.FileOriginalName + "</td><td>" + item.LocalPath + "</td></tr>\n");
                    i++;
                }
            }
            else
            {
                reportTable.Append("<tr><td  align='center'  colspan='3'>No data available</td><td></tr>\n");
            }

            string Content = @" <p>Hello ,</p>
                                <p>UMB Powerstation Report is generated and uploaded for following files.</p>";
            string reportLink = "";

            if (report == UMBReportType.SchedulePositions)
                reportLink = "https://www.cliffwater.com/Admin#CCLFUploadExcel";
            else reportLink = "https://www.cliffwater.com/Admin#CCLFUploadTrialBalRpt";

            string path = BuildTemplatePath("EmailTemplate.html");
            string html = File.ReadAllText(@path);
            html = html.Replace("{MessageContent}", Content);
            html = html.Replace("{ReportType}", report.ToString());
            html = html.Replace("{ReportLink}", reportLink);
            html = html.Replace("{ReportTable}", reportTable.ToString());
            html = html.Replace("{CurrentYear}", DateTime.Now.Year.ToString());
            var message = new EmailMessage(_emailConfig.ToEmail.Split(',').ToArray(), subject, html);
            return message;
        }

        public EmailMessage UMBPowerStationReportMessage(List<CCLFUploadRequest> schedulePostions, List<CCLFUploadRequest> trialBalance)
        {
            string subject = "UMB PowerStation Report " + DateTime.Today.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);

            StringBuilder scehdulePostionsTable = new StringBuilder();
            StringBuilder trialBalanceTable = new StringBuilder();

            int i = 1;
            if (schedulePostions.Count > 0)
            {
                foreach (var item in schedulePostions)
                {
                    scehdulePostionsTable.Append("<tr><td>" + i + "</td><td>" + item.FileOriginalName + "</td><td>" + item.LocalPath + "</td></tr>\n");
                    i++;
                }
            }
            else
            {
                scehdulePostionsTable.Append("<tr><td  align='center'  colspan='3'>No data available</td><td></tr>\n");
            }

            if (trialBalance.Count > 0)
            {
                int j = 1;
                foreach (var item in trialBalance)
                {
                    trialBalanceTable.Append("<tr><td>" + j + "</td><td>" + item.FileOriginalName + "</td><td>" + item.LocalPath + "</td></tr>\n");
                    j++;
                }
            }
            else
            {
                trialBalanceTable.Append("<tr><td align='center' colspan='3'>No data available</td><td></tr>\n");
            }


            string Content = @" <p>Hello ,</p>
                                <p>UMB Powerstation Report is generated and uploaded for following files.</p>";


            string path = BuildTemplatePath("EmailTemplate.html");
            string html = File.ReadAllText(@path);
            html = html.Replace("{MessageContent}", Content);
            html = html.Replace("{SchedulePostionsTable}", scehdulePostionsTable.ToString());
            html = html.Replace("{TrialBalanceTable}", trialBalanceTable.ToString());
            html = html.Replace("{CurrentYear}", DateTime.Now.Year.ToString());
            var message = new EmailMessage(_emailConfig.ToEmail.Split(',').ToArray(), subject, html);
            return message;
        }

        private string BuildTemplatePath(string fileName)
        {
            return Path.Combine(_templateFileDirectory, fileName);
        }

    }
}
