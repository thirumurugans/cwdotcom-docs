﻿namespace CwAzureApplications.BAL.Services
{
    public class EmailSetting 
    {
        public string ToEmail { get; set; }
        public string FromEmail { get; set; }

        public string SmtpServer { get; set; }

        public int SMTPPort { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public bool IsCCEnabled { get; set; }

        public bool IsBCCEnabled { get; set; }

        public string BCCEmail { get; set; }

        public string CCEmail { get; set; }

        public string EmailPrefix { get; set; }

        public string WebSiteUrl { get; set; }

    }
}