﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CwAzureApplications.BAL.Services;

namespace CwAzureApplications.BAL.Email
{
    public interface IEmailSender
    {
        Tuple<bool, string> SendEmail(EmailMessage emailMessage);
        Tuple<bool, string> SendEmailWithAttachment(EmailMessage emailMessage);
        Task<Tuple<bool, string>> SendEmailAsync(EmailMessage emailMessage);
    }
}
