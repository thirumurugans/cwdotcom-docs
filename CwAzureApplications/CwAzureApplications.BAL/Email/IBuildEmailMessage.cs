﻿using System.Collections.Generic;
using CwAzureApplications.BAL.Model;
using CwAzureApplications.BAL.Services;

namespace CwAzureApplications.BAL.Email
{
    public interface IBuildEmailMessage
    {
        EmailMessage UMBPowerStationReportMessage(UMBReportType report, List<CCLFUploadRequest> reportFiles);
    }

}