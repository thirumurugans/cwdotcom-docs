﻿using CwAzureApplications.BAL.Email;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UMBImportScrape.Email;

namespace CwAzureApplications.BAL.Services
{

    public class EmailSender : IEmailSender
    {
        private readonly EmailSetting _emailConfig;
        public EmailSender(EmailSetting emailSetting)
        {
            _emailConfig = emailSetting;
        }


        public Tuple<bool, string> SendEmail(EmailMessage message)
        {
            try
            {
                var emailMessage = CreateEmailMessage(message);
                Send(emailMessage);
                return Tuple.Create(true, "Email Sent Successfully.");
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return Tuple.Create(false, msg);
            }
        }

        public async Task<Tuple<bool, string>> SendEmailAsync(EmailMessage message)
        {
            try
            {
                var emailMessage = CreateEmailMessage(message);
                await SendAsync(emailMessage);
                return Tuple.Create(true, "Email Sent Successfully.");
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return Tuple.Create(false, msg);
            }
        }
        /*
        public Tuple<bool, string> SendEmailWithAttachment(EmailMessage emailMessage)
        {
            try
            {
                var mimeMsg = CreateEmailMessage(emailMessage);
                var bodyBuilder = new BodyBuilder { HtmlBody = emailMessage.Content };
                foreach (var attachment in emailMessage.Attachments)
                {
                    byte[] bytes = Encoding.ASCII.GetBytes(attachment.Content);
                    bodyBuilder.Attachments.Add(attachment.Name, bytes, new ContentType(attachment.MediaType, attachment.SubType));
                }
                mimeMsg.Body = bodyBuilder.ToMessageBody();
                //comment below lines once in production
                mimeMsg.Bcc.Clear();
                mimeMsg.Cc.Clear();

                Send(mimeMsg);
                return Tuple.Create(true, "Email Sent Successfully.");
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return Tuple.Create(false, msg);
            }
        }
         * */

        private MimeMessage CreateEmailMessage(EmailMessage message)
        {
            var emailMimeMessageMessage = new MimeMessage();

            emailMimeMessageMessage.From.Add(new MailboxAddress(null, _emailConfig.FromEmail));

            emailMimeMessageMessage.To.AddRange(message.MailTo);

            emailMimeMessageMessage.Subject = _emailConfig.EmailPrefix + message.Subject;

            var bodyBuilder = new BodyBuilder { HtmlBody = message.Content };

            emailMimeMessageMessage.Body = bodyBuilder.ToMessageBody();

            if (_emailConfig.IsCCEnabled)
            {
                emailMimeMessageMessage.Cc.Add(new MailboxAddress(null, _emailConfig.CCEmail));
            }

            if (_emailConfig.IsBCCEnabled)
            {
                emailMimeMessageMessage.Bcc.Add(new MailboxAddress(null, _emailConfig.BCCEmail));
            }

            return emailMimeMessageMessage;
        }

        private void Send(MimeMessage mailMessage)
        {
            //using (var client = new SmtpClient())
            //{
            //    try
            //    {
            //        client.ServerCertificateValidationCallback = (s, c, h, e) => true;
            //        client.Connect(_emailConfig.SmtpServer, _emailConfig.SMTPPort, false);
            //        client.Authenticate(_emailConfig.UserName, _emailConfig.Password);
            //        client.Send(mailMessage);

            //    }
            //    finally
            //    {
            //        client.Disconnect(true);
            //        client.Dispose();
            //    }
            //}

            using (var client = new System.Net.Mail.SmtpClient())
            {
                try
                {

                    client.Port = _emailConfig.SMTPPort;
                    client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    System.Net.NetworkCredential credential = new System.Net.NetworkCredential(_emailConfig.UserName, _emailConfig.Password);
                    client.Credentials = credential;
                    client.EnableSsl = true;
                    client.Host = _emailConfig.SmtpServer;
                    System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage(_emailConfig.FromEmail, _emailConfig.ToEmail);

                    mail.Subject = mailMessage.Subject;
                    mail.IsBodyHtml = true;
                    mail.Body = mailMessage.HtmlBody;
                    client.Send(mail);

                }
                finally
                {
                    //client.Disconnect(true);
                    client.Dispose();
                }
            }

        }

        private async Task SendAsync(MimeMessage mailMessage)
        {
            using (var client = new SmtpClient())
            {
                try
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    await client.ConnectAsync(_emailConfig.SmtpServer, _emailConfig.SMTPPort, false);
                    await client.AuthenticateAsync(_emailConfig.UserName, _emailConfig.Password);
                    await client.SendAsync(mailMessage);
                }
                finally
                {
                    await client.DisconnectAsync(true);
                    client.Dispose();
                }
            }
        }
    }
}
