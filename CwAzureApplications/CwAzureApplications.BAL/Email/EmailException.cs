﻿using System;
using System.Runtime.Serialization;

namespace CwAzureApplications.BAL.Email
{
    [Serializable]
    public class EmailException : Exception
    {
        public EmailException(string message)
            : base(message)
        {
        }
        public EmailException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
        public EmailException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
