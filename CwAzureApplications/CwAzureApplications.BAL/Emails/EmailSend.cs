﻿using PrivateEquityReportCreation.ErrorLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace CwAzureApplications.BAL.Emails
{
    class EmailSend
    {
        static LogManager _logManagerEvent = new LogManager("Event");
        static LogManager _logError = new LogManager("Error");
        public static void EmailTrigger(EmailMessages emailMessage)
        {
            try
            {

                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "*******      New Profile report updated PA Focus fund list in Website & Email functionalities   *******"));
                StringBuilder sb = new StringBuilder();
                int i = 1;
                MailMessage mail = new MailMessage(ConfigurationManager.AppSettings["MailFrom"], ConfigurationManager.AppSettings["MailTo"]);
                mail.Subject = emailMessage.Subject;
                mail.IsBodyHtml = emailMessage.IsBodyHtml;
                mail.Body = emailMessage.Body;
                foreach (var file in emailMessage.Attachments)
                {
                    System.Net.Mail.Attachment record = new System.Net.Mail.Attachment(
                        new MemoryStream(System.Convert.FromBase64String(file.Content)), file.Name);
                    mail.Attachments.Add(record);
                }


                //using (SmtpClient client = new SmtpClient())
                //{
                //    client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                //    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                //    client.UseDefaultCredentials = false;
                //    NetworkCredential credential = new NetworkCredential(ConfigurationManager.AppSettings["MailUsername"], ConfigurationManager.AppSettings["MailPassword"]);//, "email-smtp.us-west-2.amazonaws.com");
                //    client.Credentials = credential;
                //    client.EnableSsl = true;
                //    client.Host = ConfigurationManager.AppSettings["SMTPHost"];

                //    client.Send(mail);
                //}



                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Mail was sent for - Funds"));
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Email triggerfor PA focus list funds new docs"));
            }
        }
    }
    public class EmailMessages
    {
        public EmailMessages()
        {
            Attachments = new List<Attachment>();
        }
        public string Subject { get; set; }
        public bool IsBodyHtml { get; set; }
        public string Body { get; set; }
        public List<Attachment> Attachments { get; set; }

        public EmailMessages AddAttachment(string Name, string Content)
        {
            var fileType = Path.GetExtension(Name);
            if (fileType.Contains(".") == true)
                fileType = fileType.Replace(".", string.Empty);

            var AttachmentType = FileHelper.fileTypes.FirstOrDefault(s => s.Value.ToString() == fileType).Key;
            string mediaType = "application";
            string subType = "text";
            if (AttachmentType != null)
            {
                mediaType = AttachmentType.Split('/')[0] ?? "application";
                subType = AttachmentType.Split('/')[1] ?? "text";
            }


            Attachments.Add(new Attachment { Content = Content, MediaType = mediaType, Name = Name, SubType = subType });
            return this;
        }
    }


    public class Attachment
    {
        public string MediaType { get; set; }
        public string SubType { get; set; }
        public string Content { get; set; }
        public string Name { get; set; }
    }

}
