﻿using PrivateEquityReportCreation.ErrorLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.File;

namespace CwAzureApplications.BAL
{
    public class ExcelIrr
    {
        private static LogManager _logManagerEvent = new LogManager("Event");
        private static LogManager _logError = new LogManager("Error");
        static List<ProductDetails> productList = null;
        static string excelFilePath = string.Empty;
        static DateTime asofdate;
        static JsonSerializeObject array = null;
        static readonly string baseDirectory = ConfigurationManager.AppSettings["ExcelBasePath"];
        static readonly string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
        public static void CreateExcelWithIrr()
        {

            try
            {
                using (var conn = new SqlConnection(connectionString))
                {
                    _logManagerEvent.Splitter();
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Process Started => ******      Create Excel with Transaction Cash flow & Irr Calculation...     ******"));
                    using (StreamReader file = File.OpenText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "Config.json")))
                    {
                        array = JsonConvert.DeserializeObject<JsonSerializeObject>(file.ReadToEnd());
                        asofdate = Convert.ToDateTime(array.HoldingAsOfDate);
                        //excelFilePath = baseDirectory;
                        productList =
                           conn.Query<ProductDetails>(@"SELECT DISTINCT ProductId,ProductName FROM vPortfolioDetails WHERE Strategy IN ('Client Portfolio - Real Assets','Client Portfolio - Private Equity','Client Portfolio - Real Estate') ORDER BY ProductName").AsList();
                        if (productList.Count > 0)
                            CreateExcel();
                        else
                            _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "No Products available..."));
                    }
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Process Completed => Excel Creation with Transaction Cash flow & Irr Calculation details..."));
                    _logManagerEvent.Splitter();
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception => IRR Excel Create process at Main function...!"));
            }
        }

        /// <summary>
        /// Creating excel with data for all possible type category
        /// </summary>
        private static void CreateExcel()
        {
            int productCnt = 1;
            foreach (var product in productList)
            {
                try
                {
                    //product.ProductId = "1546517";
                    //product.ProductName = "University of Nebraska Foundation (Real Estate)";
                    

                    using (XLWorkbook workbook = new XLWorkbook())
                    {
                        using (var conn = new SqlConnection(connectionString))
                        {
                            var queryResult= conn.Query<string>("SELECT TOP 1 DateAs FROM vtbl_BackstopDataSync WHERE ProductId=" + product.ProductId).FirstOrDefault();
                            if (queryResult.Any())
                                asofdate = Convert.ToDateTime(queryResult.ToString());
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, productCnt + "). Processing(Current Quarter) => The product name is : " + product.ProductName + " => As Of Date : " + asofdate.ToString("MM/dd/yyyy")));

                            List<TransactionCashflow> cashFlowList = conn.Query<TransactionCashflow>("exec usp_GetPortfolioTransactionForExcelIrr " + product.ProductId + ",'" + asofdate + "'").AsList();
                            ExcelSheetIrr(workbook, cashFlowList.OrderBy(a => a.Date).ToList(), "Portfolio", asofdate, 1);

                            DateTime firstDayOfTheMonth = new DateTime(asofdate.AddMonths(-3).Year, asofdate.AddMonths(-3).Month, 1);
                            DateTime previousQtr = firstDayOfTheMonth.AddMonths(1).AddDays(-1);
                            List<TransactionCashflow> cashFlowListPrevQtr = conn.Query<TransactionCashflow>("exec usp_GetPortfolioTransactionForExcelIrr " + product.ProductId + ",'" + previousQtr + "'").AsList();
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "    Processing(Previous Quarter) => The product name is : " + product.ProductName + " => As Of Date : " + asofdate.ToString("MM/dd/yyyy")));
                            ExcelSheetIrr(workbook, cashFlowListPrevQtr.OrderBy(a => a.Date).ToList(), "Portfolio", previousQtr, 1);
                            int categoryCnt = 1;
                            foreach (var list in cashFlowList.Where(a => a.Strategy != null).Select(a => a.Strategy).Distinct())
                            {
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "   " + categoryCnt + "). Strategy level Processing => The Strategy is : " + list));
                                ExcelSheetIrr(workbook, cashFlowList.Where(str => str.Strategy == list.ToString()).OrderBy(a => a.Date).ToList(), ("Strategy-" + list).Length > 30 ? ("Strategy-" + list).Remove(30) : ("Strategy-" + list), asofdate);
                                categoryCnt++;
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "       End"));
                            }
                            categoryCnt = 1;
                            foreach (var list in cashFlowList.Where(a => a.Sector != null).Select(a => a.Sector).Distinct())
                            {
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "   " + categoryCnt + "). Sector level Processing => The Sector is : " + list));
                                ExcelSheetIrr(workbook, cashFlowList.Where(str => str.Sector == list.ToString()).OrderBy(a => a.Date).ToList(), ("Sector-" + list).Length > 30 ? ("Sector-" + list).Remove(30) : ("Sector-" + list), asofdate);
                                categoryCnt++;
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "       End"));
                            }
                            categoryCnt = 1;
                            foreach (var list in cashFlowList.Where(a => a.Geography != null).Select(a => a.Geography).Distinct())
                            {
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "   " + categoryCnt + "). Geography level Processing => The Geography is : " + list));
                                ExcelSheetIrr(workbook, cashFlowList.Where(str => str.Geography == list.ToString()).OrderBy(a => a.Date).ToList(), ("Geography-" + list).Length > 30 ? ("Geography-" + list).Remove(30) : ("Geography-" + list), asofdate);
                                categoryCnt++;
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "       End"));
                            }
                            categoryCnt = 1;
                            foreach (var list in cashFlowList.Where(a => a.VintageYear != null).Select(a => a.VintageYear).Distinct())
                            {
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "   " + categoryCnt + "). Vintage level Processing => The Vintage year is : " + list));
                                ExcelSheetIrr(workbook, cashFlowList.Where(str => str.VintageYear == list.ToString()).OrderBy(a => a.Date).ToList(), ("Vintage-" + list).Length > 30 ? ("Vintage-" + list).Remove(30) : ("Vintage-" + list), asofdate);
                                categoryCnt++;
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "       End"));

                            }                            
                        }
                        excelFilePath = Path.Combine(baseDirectory + "\\" + product.ProductName.Replace("/", "-").Replace("&", " and ") + "_" + asofdate.Year + "-" + asofdate.Month + "-" + asofdate.Day + ".xlsx");
                        workbook.SaveAs(excelFilePath);
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "The Excel file saved into following location => " + excelFilePath));
                        //// Upload to azure
                        UploadIrrExcelToAzure(product.ProductName.Replace("/", "-").Replace("&", " and ") + "_" + asofdate.Year + "-" + asofdate.Month + "-" + asofdate.Day + ".xlsx", excelFilePath);
                        workbook.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                    _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception => IRR Excel Create process error for '" + product.ProductName + "' => ProductId : " + product.ProductId));
                }
                productCnt++;
            }

        }
        // IRR Excel Upload to azure for all products
        private static void UploadIrrExcelToAzure(string targetFileName, string excelFilePath)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(Settings.AzureConnectionString);
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Azure Upload => The Excel file uploading to azure ..... => " + targetFileName));
                // var fileInfo = new FileInfo(excelFilePath);
                if (File.Exists(excelFilePath))
                {
                    // var targetFileName = fileName;// $"{file.DocId}{fileInfo.Extension}";
                    //logger.Debug($"Uploading file for docid {file.DocId} to {targetFileName}...");
                    CloudFileClient fileClient = storageAccount.CreateCloudFileClient();
                    var share = fileClient.GetShareReference(Settings.UploadFolder);
                    var cloudFile = share.GetRootDirectoryReference().GetFileReference(targetFileName);
                    cloudFile.Properties.ContentDisposition = "attachment; filename=" + targetFileName;
                    try
                    {
                        cloudFile.Delete();
                    }
                    catch
                    {
                    }
                    cloudFile.UploadFromFile(excelFilePath);
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Azure Upload => Exception => IRR Excel Upload to azure got failed for '" + targetFileName + "'"));
            }
        }

        /// <summary>
        /// Excel sheet creation & update the values in Excel for all category
        /// </summary>
        /// <param name="workbook"></param>
        /// <param name="listCashflow"></param>
        /// <param name="sheetName"></param>
        /// <param name="asofdateLocal"></param>
        /// <param name="flag"></param>
        private static void ExcelSheetIrr(XLWorkbook workbook, List<TransactionCashflow> listCashflow, string sheetName, DateTime asofdateLocal, int flag = 0)
        {
            try
            {
                using (var ws = workbook.AddWorksheet(sheetName + (flag == 1 ? "(" + asofdateLocal.Year + "-" + asofdateLocal.Month + "-" + asofdateLocal.Day + ")" : "")))
                {
                    int rowCnt = 2;
                    ws.Columns("A", "F").Style.Alignment.WrapText = true;
                    ws.Columns("A").Width = 40;
                    ws.Columns("B").Width = 15;
                    ws.Columns("C").Width = 20;
                    // ws.Columns("F").Width = 25;
                    ws.Cells("A1:E1").Style.Font.Bold = true;
                    ws.Cells("A1:E1").Style.Fill.BackgroundColor = XLColor.SkyBlue;
                    ws.Cells("A1:E1").Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                    ws.Cells("A1:E1").Style.Border.RightBorder = XLBorderStyleValues.Thin;
                    ws.Cells("A1:E1").Style.Border.TopBorder = XLBorderStyleValues.Thin;
                    ws.Cells("A1:E1").Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    ws.Cells("A1").Value = "Partnership Name";
                    ws.Cells("B1").Value = "Value";
                    ws.Cells("C1").Value = "Date";
                    
                    ws.Cells("D1").Value = "Type";
                    ws.Cells("E1").Value = "Type Name";

                    foreach (var item in listCashflow)
                    {
                        ws.Cell(rowCnt, 1).Value = Convert.ToString(item.Partnership);
                        ws.Cell(rowCnt, 2).Value = Convert.ToDecimal(item.Value);
                        ws.Cell(rowCnt, 2).Style.NumberFormat.Format = "$#,##0.00;[RED]-$#,##0.00";
                        ws.Cell(rowCnt, 3).Value = item.Date;
                        ws.Cell(rowCnt, 3).Style.DateFormat.Format = "MM/dd/yyyy";

                        ws.Cell(rowCnt, 4).Value = item.TransactionType;
                        ws.Cell(rowCnt, 5).Value = item.TransactionTypeName;
                        
                        ws.Cells("A" + rowCnt + ":E" + rowCnt).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        ws.Cells("A" + rowCnt + ":E" + rowCnt).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Cells("A" + rowCnt + ":E" + rowCnt).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Cells("A" + rowCnt + ":E" + rowCnt).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        rowCnt++;
                    }

                    //ws.Cells("E2").Value = sheetName + " Net IRR" + (flag == 1 ? "(" + asofdateLocal.Year + "-" + asofdateLocal.Month + "-" + asofdateLocal.Day + ")" : "");
                    ws.Cells("G2").Value = sheetName + " Net IRR" + (flag == 1 ? "(" + asofdateLocal.Year + "-" + asofdateLocal.Month + "-" + asofdateLocal.Day + ")" : "");
                    ////ws.Cells("F1").Value = "Without Guess Rate";

                    //// ws.Cells("F1").Value = "Without Guess Rate";
                    //ws.Columns("E").Width = 30;
                    ws.Columns("G").Width = 30;
                    //ws.Cells("E2:F2").Style.Font.Bold = true;
                    ws.Cells("G2:H2").Style.Font.Bold = true;
                    if (listCashflow.Count > 0)
                    {
                        ////ws.Cells("F2").FormulaA1 = "=ROUND(XIRR(B2:B" + (listCashflow.Count + 1) + ",C2:C" + (listCashflow.Count + 1) + ",-10%)*100,2)";
                        //ws.Cells("F2").FormulaA1 = "=ROUND(XIRR(B2:B" + (listCashflow.Count + 1) + ",C2:C" + (listCashflow.Count + 1) + ")*100,2)";
                        ws.Cells("H2").FormulaA1 = "=ROUND(XIRR(B2:B" + (listCashflow.Count + 1) + ",C2:C" + (listCashflow.Count + 1) + ")*100,2)";

                        //if (ws.Cell(2,6).GetFormattedString() == "")
                        //{
                        //    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Net IRR logic change => If Net IRR #'s '0' then we use to apply Guess Rate..... => "));
                        //    ws.Cells("F2").FormulaA1 = "=ROUND(XIRR(B2:B" + (listCashflow.Count + 1) + ",C2:C" + (listCashflow.Count + 1) + ",-10%)*100,2)";
                        //    ws.Cells("G2").FormulaA1 = "=ROUND(XIRR(B2:B" + (listCashflow.Count + 1) + ",C2:C" + (listCashflow.Count + 1) + ")*100,2)";
                        //}

                    }
                    //ws.Cells("F2").Style.Fill.BackgroundColor = XLColor.Yellow;
                    ws.Cells("H2").Style.Fill.BackgroundColor = XLColor.Yellow;

                    ws.Columns().AdjustToContents();
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception => Excel IRR data preparation error for " + sheetName));
            }
        }

    }
    public class ProductDetails
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public DateTime DateAs { get; set; }
        public DateTime InceptionDate { get; set; }

    }

    public class TransactionCashflow
    {
        public int PartnershipId { get; set; }
        public string Partnership { get; set; }
        public string Strategy { get; set; }
        public string VintageYear { get; set; }
        public string Sector { get; set; }
        public string Geography { get; set; }
        public float Value { get; set; }
        public DateTime Date { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string TransactionType { get; set; }
        public string TransactionTypeName { get; set; }
        public int FundId { get; set; }
        public float Contribution { get; set; }
        public float Distribution { get; set; }
    }
}
