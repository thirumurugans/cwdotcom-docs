﻿using CwAzureApplications.BAL.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CwAzureApplications.BAL
{
    public class BackstopRestAPI
    {
        private string SERVICE_URL = "https://cliffwater.backstopsolutions.com";
        private string HTTP_METHOD_GET = "GET";
        CommonClass objCommonClass = new CommonClass();
        string authorizationToken = "";
        string apiLink = "";
        public void CallAPIs()
        {
            authorizationToken=objCommonClass.Login();
            CallPEHoldingsAPI();
        }
        public void CallPEHoldingsAPI()
        {
            try
            {
               // objEventLog.CreateLog(string.Format("{0} - {1} - Cliffwater : Backstop Rest API - API to get all PE Holdings started.", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name), eventLogFilePath);
                StringBuilder stringAppennew=new StringBuilder();
                apiLink = SERVICE_URL + "/backstop/api/private-equity-holdings/?fields=id,name,createdTimestamp,inceptionDate,investmentCloseDate,fundThisHoldingIsInvestedIn,regularCustomFieldValues";
                DateTime startTime = DateTime.Now;

                string peHoldingsResponse = objCommonClass.sendHttpRequest(apiLink, HTTP_METHOD_GET, null, true, authorizationToken);
                TimeSpan peHoldingsAPITiming = (DateTime.Now - startTime);

                //objCommonClass.ObjectToByteArray(peHoldingsResponse, dtAPICallDetails, "PE Holdings", "Get All Holdings", apiLink, peHoldingsAPITiming);
                //objEventLog.CreateLog(string.Format("{0} - {1} - Cliffwater : Backstop Rest API - API to get all PE Holdings completed. Time taken {2}:{3}:{4}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, peHoldingsAPITiming.Hours.ToString(), peHoldingsAPITiming.Minutes.ToString(), peHoldingsAPITiming.Seconds.ToString()), eventLogFilePath);
                stringAppennew.Append(peHoldingsResponse);
                RootObject peHoldingsData = JsonConvert.DeserializeObject<RootObject>(peHoldingsResponse);
                int id=0;
                List<PartnershipMaster> list=new List<PartnershipMaster>();
                foreach(var main in peHoldingsData.data)
                {
                    
                    try
                    {
                        var vintage = main.attributes.regularCustomFieldValues.Where(x => x.name == "Vintage Year").ToList();
                        var investmentType = main.attributes.regularCustomFieldValues.Where(x => x.name == "Strategy").ToList();
                        var active = main.attributes.regularCustomFieldValues.Where(x => x.name == "Active/Passive").ToList();
                        var originator = main.attributes.regularCustomFieldValues.Where(x => x.name == "Originator").ToList();
                        PartnershipMaster partnership = new PartnershipMaster();
                        partnership.PartnershipId = Convert.ToInt32(main.id);
                        partnership.PartnershipName = main.attributes.name;
                        partnership.FundId = main.attributes.fundThisHoldingIsInvestedIn == null ? "0" : main.attributes.fundThisHoldingIsInvestedIn.resourceId;
                        partnership.FundName = "";
                        partnership.ClosingDate = Convert.ToDateTime(main.attributes.investmentCloseDate);
                        partnership.CreatedDate = Convert.ToDateTime(main.attributes.createdTimestamp);
                        partnership.VintageYear = vintage.Any() ? vintage[0].value.ToString() : "0";
                        partnership.InvestmentType = investmentType.Any() ? investmentType[0].value.ToString() : "";
                        if (active.Any())
                        {
                            if (active[0].value.ToString() == "Active")
                                partnership.IsActive = 1;
                            else
                                partnership.IsActive = 0;
                        }
                        partnership.HoldingMonitorType = originator.Any() ? originator[0].value.ToString() : "";
                        
                        list.Add(partnership);
                    }
                    catch(Exception ex)
                    {
                    }
                    id++;
                }


                foreach (var part in list.Where(x=>x.FundId!="0").Select(x => x.FundId).Distinct())
                {
                    try
                    {
                        var fund = list.Where(x => x.FundId == part).FirstOrDefault();
                        string apiLinkFund = SERVICE_URL + "/backstop/api/private-equity-funds/" + part + "/?fields=name,regularCustomFieldValues";
                        string fundResponse = objCommonClass.sendHttpRequest(apiLinkFund, HTTP_METHOD_GET, null, true, authorizationToken);
                        stringAppennew.Append(fundResponse);
                        RootObjectFund peFundsData = JsonConvert.DeserializeObject<RootObjectFund>(fundResponse);
                        var sector = peFundsData.data.attributes.regularCustomFieldValues.Where(x => x.name == "Industry/Sector Focus").ToList();
                        var geography = peFundsData.data.attributes.regularCustomFieldValues.Where(x => x.name == "Geographic Focus").ToList();
                        var monitoring = peFundsData.data.attributes.regularCustomFieldValues.Where(x => x.name == "Monitoring Type").ToList();
                        var UsNonUs = peFundsData.data.attributes.regularCustomFieldValues.Where(x => x.name == "US/Non-US").ToList();
                        fund.FundName = peFundsData.data.attributes.name;
                        fund.Sector = sector.Any() ? sector[0].value.ToString() : "";
                        fund.Geography = geography.Any() ? geography[0].value.ToString() : "";
                        fund.MonitorType = monitoring.Any() ? monitoring[0].value.ToString() : "";
                        fund.USNonUS = UsNonUs.Any() ? UsNonUs[0].value.ToString() : "";
                        string apiLinkStageFocus = SERVICE_URL + "/backstop/api/private-equity-funds/" + part + "/relationships/assetType";
                        string fundStageFocus = objCommonClass.sendHttpRequest(apiLinkStageFocus, HTTP_METHOD_GET, null, true, authorizationToken);
                        RootObjectFund peStageFocus = JsonConvert.DeserializeObject<RootObjectFund>(fundStageFocus);
                        fund.StageFocus = peStageFocus.data.attributes.name;
                        stringAppennew.Append(apiLinkStageFocus);
                        string apiLinkAssetType = SERVICE_URL + "/backstop/api/asset-types/" + peStageFocus.data.id + "/parent";
                        string fundAssetType = objCommonClass.sendHttpRequest(apiLinkAssetType, HTTP_METHOD_GET, null, true, authorizationToken);
                        RootObjectFund peAssetType = JsonConvert.DeserializeObject<RootObjectFund>(fundAssetType);
                        stringAppennew.Append(fundAssetType);
                        if (peAssetType.data != null)
                        {
                            if (peAssetType.data.attributes.name.EndsWith("PE"))
                                fund.AssetTypes = "Private Equity";
                            else if (peAssetType.data.attributes.name.EndsWith("RA"))
                                fund.AssetTypes = "Real Assets";
                            else if (peAssetType.data.attributes.name.EndsWith("RE"))
                                fund.AssetTypes = "Real Estate";
                            else if (peAssetType.data.attributes.name.EndsWith("HF"))
                                fund.AssetTypes = "Hedge Funds";
                            else
                                fund.AssetTypes = "Unspecified";
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
                 string apiSize= objCommonClass.ObjectToByteArray(stringAppennew.ToString());

                //int totalCount = (peHoldingsData == null) ? 0 : peHoldingsData.meta.totalResourceCount;
                //objEventLog.CreateLog(string.Format("{0} - {1} - Cliffwater : Backstop Rest API - Individual PE Holdings process started for {2} holdings.", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, peHoldingsData.data.Count().ToString()), eventLogFilePath);

                //if (peHoldingsData.links != null)
                //{
                //    CallPEHoldingsAPI_Next(peHoldingsData, authorizationToken, dtAPICallDetails, 1);
                //}
                //else
                //{
                //    GetPEHoldingsData(peHoldingsData, authorizationToken, dtAPICallDetails);
                //}

                //objEventLog.CreateLog(string.Format("{0} - {1} - Cliffwater : Backstop Rest API - Individual PE Holdings process completed for {2} holdings.", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, peHoldingsData.data.Count().ToString()), eventLogFilePath);
            }
            catch (Exception ex)
            {
                //objEventLog.CreateLog(string.Format("\t Error - {0} - {1} - Cliffwater : Backstop Rest API - {2} - More details in error log file", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message), eventLogFilePath);
                //objErrorLog.LogServerError(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, errorLogFilePath);
            }
        }
    }
}
