﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CwAzureApplications.BAL
{
    public enum FileType
    {
        none,
        xlsOrCsv,
        xlsx,
        xml,
        binaryStream,
        appStream,
        json
    }

    public enum FilePartType
    {
        mime,
        encoding,
        data
    }
    public static class FileHelper
    {
        public static IDictionary<string, FileType> fileTypes = new Dictionary<string, FileType>
        {
            {"application/vnd.ms-excel", FileType.xlsOrCsv },
            {"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", FileType.xlsx },
            {"application/xml", FileType.xml },
            {"text/xml", FileType.xml },
            {"binary/octet-stream", FileType.binaryStream },
            {"application/octet-stream", FileType.appStream },
            {"application/json", FileType.json }
        };

        static Regex regex = new Regex("data:(?<" + FilePartType.mime.ToString() + ">[\\w/\\-\\.]+);(?<" + FilePartType.encoding.ToString() + ">\\w+),(?<" + FilePartType.data.ToString() + ">.*)", RegexOptions.Compiled);
        public static IDictionary<FilePartType, string> GetFilePart(this string input)
        {
            var match = regex.Match(input);
            return new Dictionary<FilePartType, string>()
            {
                {FilePartType.mime, match.Groups[FilePartType.mime.ToString()].Value},
                {FilePartType.encoding, match.Groups[FilePartType.encoding.ToString()].Value},
                {FilePartType.data, match.Groups[FilePartType.data.ToString()].Value},
            };
        }
    }
}