﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CwAzureApplications.BAL
{
     public class MailSender
    {
        //static EventLog objEventLog = new EventLog();
        //static ErrorLog objErrorLog = new ErrorLog();
        static string eventLogFilePath = ConfigurationManager.AppSettings["EventLog"].ToString();
        static string errorLogFilePath = ConfigurationManager.AppSettings["ErrorLog"].ToString();

        private static string GetContent(MailInfo mailInfo)
        {
            var content = "";

            try
            {
                var path = mailInfo.TemplateFile;
                var template = File.ReadAllText(path);
                content = template;

                foreach (var parameter in mailInfo.Parameters)
                {
                    //content = content.Replace($"[{parameter.Key}]", parameter.Value);
                }
            }
            catch (Exception ex)
            {
                //objErrorLog.LogServerError(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, errorLogFilePath);
            }

            return content;
        }

        private static List<MailboxAddress> GetAddress(string mail)
        {
            try
            {
                if (!string.IsNullOrEmpty(mail))
                {
                    return mail.Split(new[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries).Select(t => new MailboxAddress(t)).ToList();
                }
            }
            catch (Exception ex)
            {
                //objErrorLog.LogServerError(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, errorLogFilePath);
            }

            return new List<MailboxAddress>();
        }

        private static MimeMessage GetMessage(MailboxAddress from, string subject, string content, int attachment, string docsPath, string fileName, List<MailboxAddress> to, List<MailboxAddress> cc = null, List<MailboxAddress> bcc = null)
        {
            var message = new MimeMessage();

            try
            {
                message.Subject = subject;

                var bodyBuilder = new BodyBuilder
                {
                    HtmlBody = content
                };

                if (attachment > 0 && Directory.Exists(docsPath))
                {
                    if (fileName != "")
                    {
                        bodyBuilder.Attachments.Add(fileName);
                    }
                    else
                    {
                        foreach (var file in Directory.GetFiles(docsPath))
                        {
                            bodyBuilder.Attachments.Add(file);
                        }
                    }
                }

                message.Body = bodyBuilder.ToMessageBody();
                message.From.Add(from);
                to.ForEach(r => message.To.Add(r));
                //cc?.ForEach(r => message.Cc.Add(r));
                //bcc?.ForEach(r => message.Bcc.Add(r));
                message.Headers.Add("X-SES-CONFIGURATION-SET", "EmailTracking");
                var trackingSource = ConfigurationManager.AppSettings["MailTrackingSource"].ToString();
                //message.Headers.Add("X-SES-MESSAGE-TAGS", $"AppSource={trackingSource}");
            }
            catch (Exception ex)
            {
                //objErrorLog.LogServerError(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, errorLogFilePath);
            }

            return message;
        }

        public static void SendMail(MailInfo mailInfo)
        {
            try
            {
                var content = mailInfo.Content ?? GetContent(mailInfo);
                var bcc = GetAddress(mailInfo.Bcc);
                var cc = GetAddress(mailInfo.Cc);
                var to = GetAddress(mailInfo.To);
                var from = mailInfo.From;

                EventTracker.TrackEvent("SendMailUsingAmazonSES", new Dictionary<string, string>
                {
                    {"From", from.Item2},{"CC", mailInfo.Cc},{"Bcc", mailInfo.Bcc}, {"To", mailInfo.To }, {"Subject", mailInfo.Subject }, {"Content" , content }
                });

                var bodyBuilder = new BodyBuilder
                {
                    HtmlBody = content
                };

                var fromAddress = new MailboxAddress(from.Item1, from.Item2);

                using (var client = new SmtpClient())
                {
                    client.Connect("email-smtp.us-west-2.amazonaws.com", 587, MailKit.Security.SecureSocketOptions.StartTlsWhenAvailable);
                    client.Authenticate("AKIAIFAJEKKREEH7SYRA", "AsxR6qaZrNKC0J6E70blgWW2sKLkzKtlx3yH+A/+D+dv");

                    if (!mailInfo.ShouldSendIndividually)
                    {
                        var message = GetMessage(fromAddress, mailInfo.Subject, bodyBuilder.HtmlBody, mailInfo.HasAttachment, mailInfo.AttachmentPath, mailInfo.AttachmentFileName, to, cc, bcc);
                        client.Send(message);
                        objEventLog.CreateLog(string.Format("\t{0} - {1} - Mail send to \"" + mailInfo.To + "\" from \"" + from.Item2 + "\" with event tracker to cliffwater application", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name), eventLogFilePath);
                    }
                    else
                    {
                        var recipients = bcc.Concat(cc).Concat(to).ToList();

                        recipients.ForEach(r =>
                        {
                            try
                            {
                                var message = GetMessage(fromAddress, mailInfo.Subject, content, mailInfo.HasAttachment, mailInfo.AttachmentPath, mailInfo.AttachmentFileName, new List<MailboxAddress> { r });
                                client.Send(message);
                            }
                            catch (Exception ex)
                            {
                                //ErrorLogger.Log(ex, $"Error sending mail to {mailInfo.To} from email subscribed fund docs sync");
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                //objErrorLog.LogServerError(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, errorLogFilePath);
            }
        }

        public static class EventTracker
        {
            public static void TrackEvent(string @event, Dictionary<string, string> parameters = null)
            {
                try
                {
                    var ai = new TelemetryClient();
                    ai.Context.User.AuthenticatedUserId = "System";
                    ai.Context.Properties["ErrorSource"] = "Server";

                    if (parameters != null)
                    {
                        ai.TrackEvent(@event, parameters);
                    }
                    else
                    {
                        ai.TrackEvent(@event);
                    }
                }
                catch (Exception ex)
                {
                    //objErrorLog.LogServerError(ex, System.Reflection.MethodBase.GetCurrentMethod().Name, errorLogFilePath);
                }
            }
        }

        public static class ErrorLogger
        {
            public static void Log(Exception ex, string operation)
            {
                try
                {
                    var ai = new TelemetryClient();
                    ai.Context.Operation.Name = operation;
                    ai.Context.User.AuthenticatedUserId = "System";
                    ai.Context.Properties["ErrorSource"] = "Server";
                    ai.TrackException(ex);
                }
                catch (Exception exc)
                {
                    //objErrorLog.LogServerError(exc, System.Reflection.MethodBase.GetCurrentMethod().Name, errorLogFilePath);
                }
            }
        }
    }
    public class MailInfo
    {
        public string To { get; set; }
        public string Bcc { get; set; }
        public string Cc { get; set; }
        public string Subject { get; set; }
        public Dictionary<string, string> Parameters { get; set; }
        public string TemplateFile { get; set; }
        public string Error { get; set; }
        public bool ShouldSendIndividually { get; set; }
        public string Content { get; set; }
        public int HasAttachment { get; set; }
        public string AttachmentPath { get; set; }
        public string AttachmentFileName { get; set; }
        public Tuple<string, string> From { get; set; }
    }
}
