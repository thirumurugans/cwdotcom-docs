﻿using Microsoft.WindowsAzure.Storage;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.IO;
using System.Net;
using Microsoft.WindowsAzure.Storage.File;
using PrivateEquityReportCreation.ErrorLog;
using System.Configuration;
using System.Net.Mail;
using System.Collections.Concurrent;
using CwAzureApplications.WebService.Entities;
using CwAzureApplications.WebService.Service;


namespace CwAzureApplications.BAL
{
    public class AzureUploader
    {
        private static CloudStorageAccount StorageAccount = CloudStorageAccount.Parse(Settings.AzureConnectionString);
        private static LogManager _logManagerEvent = new LogManager("Event");
        private static LogManager _logError = new LogManager("Error");
        private static string liqAltPath = ConfigurationManager.AppSettings["LiqAltFolder"];
        private static string docsDownload = ConfigurationManager.AppSettings["DocumentBaseUrl"];
        private static int uploadIncr = 1;
        private static int totalFilesTobeUploaded = 0;
        private readonly static string reservedCharacters = "!*'();:@&=+$,/?%#[]";

        public static void UploadAllFiles()
        {
            try
            {
                totalFilesTobeUploaded = 1;
                uploadIncr = 1;
                using (var conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    _logManagerEvent.Splitter();
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "******      Docs Upload to Azure Started...     ******"));
                    var fileEntries =

                        conn.Query<FileEntry>(@"SELECT DocId, cAuthor as Author, cFileName as Filename, cTitle as Title, cCreatedDate as CreatedDate,cFileNameOverride FileNameOverride from tblDocs where  IsUploaded=0 AND DocId NOT IN (SELECT DocId FROM tblDocsDeleted) ORDER BY cCreatedDate DESC").AsList();
                    //SELECT * FROM tblDOcs WHERE cTitle LIKE '%Performance%'
                    totalFilesTobeUploaded = fileEntries.Count;
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, ("Total Files : " + fileEntries.Count + " to be Uploded to Azure")));
                    fileEntries.ForEach(UploadToAzure);
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "******      Docs Upload to Azure Completed.!     ******"));
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Docs cannot be Uploaded into Azure"));
            }
        }
        public static void UploadAllFilesUsingBytes(List<BackstopCrmEntity> docsEntities)
        {
            try
            {
                //using (var imp = new Impersonation(null, impersonateUser, impersonatePass))
                //{
                totalFilesTobeUploaded = 1;
                uploadIncr = 1;
                using (var conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    _logManagerEvent.Splitter();
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "******      Docs Upload to Azure Started...     ******"));
                    var fileEntries =

                        conn.Query<FileEntry>(@"SELECT DocId, cAuthor as Author, cFileName as Filename, cTitle as Title, cCreatedDate as CreatedDate,cFileNameOverride FileNameOverride FROM tblDocs WHERE  IsUploaded=0 AND DocId NOT IN (SELECT DocId FROM tblDocsDeleted) ORDER BY cCreatedDate DESC").AsList();
                    //SELECT * FROM tblDOcs WHERE cTitle LIKE '%Performance%'
                    totalFilesTobeUploaded = fileEntries.Count;
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, ("Total Files : " + fileEntries.Count + " to be Uploded to Azure")));
                    List<FileDocsEntry> listFiles = new List<FileDocsEntry>();
                    using (var conn2 = new SqlConnection(ConfigurationManager.AppSettings["ConnectionStringCoin2"]))
                    {
                        foreach (var docsDownFile in fileEntries)
                        {
                            // fileEntries.ForEach(UploadToAzure);
                            try
                            {
                                //BackstopCrmEntity crmEntity = docsEntities.Where(x => x.DocID == docsFile.DocId).FirstOrDefault();
                                var binary = BackstopCrmService.GetBinaryDocument(Convert.ToInt32(docsDownFile.DocId));
                                listFiles.Add(new FileDocsEntry() { DocId = docsDownFile.DocId, FileName = docsDownFile.FileName, FileDocBytes = binary, FileNameOverride = docsDownFile.FileNameOverride });
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, " Downloaded Successfully(The file size is - " + Convert.ToInt64((binary.Length / 1024)) + " KB). The Doc ID is " + docsDownFile.DocId + " and File Name is - " + docsDownFile.FileNameOverride + ""));
                                conn2.Execute(@"EXEC Cwcom_Sync_usp_InsertBackstopCallLogs @ServerPath,@ServerMethod,@InputFilter,@InputJson,@AsOfDate,@ByteLength,@ProcedureName,@Description",
                                                                                           new
                                                                                           {
                                                                                               ServerPath = "C# SOAP API- BackstopCrmService_1_6PortTypeClient", 
                                                                                                 ServerMethod = "GetBinaryDocument(DocId)", 
                                                                                                 InputFilter = "The Doc ID is - " + docsDownFile.DocId, 
                                                                                                 InputJson = "", 
                                                                                                 AsOfDate = docsDownFile.CreatedDate, 
                                                                                                 ByteLength = binary.Length, 
                                                                                                 ProcedureName = "C# Docs", 
                                                                                                 Description = "Downloaded Successfully(The file size is - " + Convert.ToInt64((binary.Length / 1024)) + " KB). The Doc ID is " + docsDownFile.DocId + " and File Name is - " + docsDownFile.FileNameOverride + "" 
                                                                                           });
                                conn2.Execute(@"EXEC PE.Cwcom_Sync_usp_InsertLogs @ProcedureName,@Dateasof,@ID,@Status,@Description", new { ProcedureName = "C# Docs", Dateasof = docsDownFile.CreatedDate, ID = docsDownFile.DocId, Status = "", Description = "Downloaded Successfully(The file size is - " + Convert.ToInt64((binary.Length / 1024)) + " KB). The Doc ID is " + docsDownFile.DocId + " and File Name is - " + docsDownFile.FileNameOverride + "" });

                            }
                            catch (Exception ex)
                            {
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, " Downloaded Failed.The Doc ID is " + docsDownFile.DocId + " and File Name is - " + docsDownFile.FileNameOverride + ""));
                                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                                conn2.Execute(@"EXEC PE.Cwcom_Sync_usp_InsertLogs @ProcedureName,@Dateasof,@ID,@Status,@Description,", new { ProcedureName = "C# Docs sync-UploadAllFilesUsingBytes()", Dateasof = docsDownFile.CreatedDate, ID = docsDownFile.DocId, Status = "Exception", Description = "Downloaded Failed.The Doc ID is " + docsDownFile.DocId + " and File Name is - " + docsDownFile.FileNameOverride + "" });
                            }

                        }
                        foreach (var docFileUpload in listFiles)
                        {
                            if (docFileUpload.FileDocBytes != null)
                            {
                                if (docFileUpload.FileDocBytes.Length > 0)
                                    UploadDocsStreamToAzure(docFileUpload);
                                else
                                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, ("File cannot be Uploded to Azure " + docFileUpload.FileName + ", Due to stream of bytes got empty")));
                            }
                            else
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, ("File cannot be Uploded to Azure " + docFileUpload.FileName + ", Due to stream of bytes got empty")));

                        }

                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "******      Docs Upload to Azure Completed.!     ******"));
                    }
                }
                //}
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Docs cannot be Uploaded into Azure"));
            }
        }

        private static void UploadToAzure(FileEntry file)
        {
            try
            {
                var sourcePath = Path.Combine(docsDownload, file.CreatedDate.Year.ToString(), file.CreatedDate.ToString("MMM"), file.CreatedDate.Day.ToString());
                var filePath = Path.Combine(docsDownload, file.CreatedDate.Year.ToString(), file.CreatedDate.ToString("MMM"), file.CreatedDate.Day.ToString(), file.FileName);
                var filePathExtOverride = Path.Combine(docsDownload, file.CreatedDate.Year.ToString(), file.CreatedDate.ToString("MMM"), file.CreatedDate.Day.ToString(), file.FileNameOverride);
                file.Path = filePath;
                Alphaleonis.Win32.Filesystem.FileInfo fileInfo1 = null;
                var fileInfo = new FileInfo(filePath);
                try
                {
                    fileInfo1 = new Alphaleonis.Win32.Filesystem.FileInfo(filePathExtOverride);
                }
                catch
                {
                }
                if (File.Exists(filePath) || File.Exists(Path.Combine(sourcePath, file.DocId + fileInfo1.Extension)))
                {
                    var targetFileName = file.DocId + fileInfo1.Extension;// $"{file.DocId}{fileInfo.Extension}";
                    //logger.Debug($"Uploading file for docid {file.DocId} to {targetFileName}...");
                    CloudFileClient fileClient = StorageAccount.CreateCloudFileClient();
                    var share = fileClient.GetShareReference(Settings.UploadFolder);
                    var cloudFile = share.GetRootDirectoryReference().GetFileReference(targetFileName);
                    cloudFile.Properties.ContentDisposition = "attachment; filename=" + targetFileName;
                    try
                    {
                        cloudFile.Delete();
                    }
                    catch
                    {
                    }
                    if (File.Exists(filePath))
                    {
                        File.Move(filePath, Path.Combine(sourcePath, targetFileName));
                        cloudFile.UploadFromFile(Path.Combine(sourcePath, targetFileName));

                    }
                    else if (File.Exists(Path.Combine(sourcePath, targetFileName)))
                    {
                        cloudFile.UploadFromFile(Path.Combine(sourcePath, targetFileName));

                    }
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, string.Format("{0} of {1} files Uploaded - {2} - {3}", uploadIncr, totalFilesTobeUploaded, file.DocId, file.FileNameOverride)));
                    uploadIncr++;
                    if (cloudFile.Exists() && File.Exists(Path.Combine(sourcePath, targetFileName)))
                    {
                        using (var conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                        {
                            conn.Execute(@"UPDATE tblDocs SET IsUploaded = 1,cFileName=@filename WHERE DocId = @DocId", new { filename = targetFileName, DocId = file.DocId });
                        }
                    }
                }
                else
                {

                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, string.Format("File does not exist {0} for doc {1}", filePath, file.DocId)));
                    uploadIncr++;
                    // logger.Error($"File does not exist {filePath} for doc {file.DocId}");
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, string.Format("Exception - ({0}) Document cannot be Uploaded into Azure", file.DocId)));
                // logger.Error($"File upload failed {file.DocId}", ex);
            }
        }

        private static void UploadDocsStreamToAzure(FileDocsEntry Upload)
        {
            try
            {

                var targetFileName = Upload.FileName;//file.DocId + fileInfo1.Extension;// $"{file.DocId}{fileInfo.Extension}";
                //logger.Debug($"Uploading file for docid {file.DocId} to {targetFileName}...");
                CloudFileClient fileClient = StorageAccount.CreateCloudFileClient();
                var share = fileClient.GetShareReference(Settings.UploadFolder);
                var cloudFile = share.GetRootDirectoryReference().GetFileReference(targetFileName);
                cloudFile.Properties.ContentDisposition = "attachment; filename=" + targetFileName;
                //try
                //{
                //    cloudFile.Delete();
                //}
                //catch
                //{
                //}
                cloudFile.UploadFromByteArray(Upload.FileDocBytes, 0, Upload.FileDocBytes.Length - 1);
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, string.Format("{0} of {1} files Uploaded - {2} - {3}", uploadIncr, totalFilesTobeUploaded, Upload.DocId, Upload.FileNameOverride)));
                uploadIncr++;
                if (cloudFile.Exists())
                {
                    using (var conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                    {
                        conn.Execute(@"UPDATE tblDocs SET IsUploaded = 1,cFileName=@filename WHERE DocId = @DocId", new { filename = targetFileName, DocId = Upload.DocId });
                    }
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, string.Format("Exception - ({0}) Document cannot be Uploaded into Azure", Upload.DocId)));
                // logger.Error($"File upload failed {file.DocId}", ex);
            }
        }

        static IList<string> getFiles = null;
        public static void UploadLiqAltAllFiles()
        {
            try
            {
                uploadIncr = 1;
                totalFilesTobeUploaded = 1;
                using (var conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    _logManagerEvent.Splitter();
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "*******      LiqAlt Docs Upload to Azure Started...    *******"));
                    var fileEntries = conn.Query<LiqAltFileEntry>(@"SELECT FundId,FundName, NewFileName from vDocsManagerResearchFilters_LiqAlt ORDER BY FundName").AsList();
                    totalFilesTobeUploaded = fileEntries.Count;
                    getFiles = Directory.GetFiles(liqAltPath).ToList();
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, ("Total Liq Alt docs : " + totalFilesTobeUploaded + " to be Uploded to Azure - from (Source Path - " + liqAltPath + ")")));
                    fileEntries.ForEach(UploadLiqAltDocsToAzure); //fileEntries.ForAll(UploadToAzure);
                    if (uploadIncr > 1)
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "******      Liq Alt Docs sync process completed.!     ******"));
                    else
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "No Liq Alt Docs available to upload.!"));
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - LiqAlt cannot be Uploaded into Azure"));
            }
        }

        private static void UploadLiqAltDocsToAzure(LiqAltFileEntry file)
        {
            try
            {
                var linqResult = getFiles.Where(fil => new FileInfo(fil.ToString()).Name.Contains(file.NewFileName));
                string filename = linqResult.Any() ? linqResult.FirstOrDefault() : "";
                var sourcePath = Path.Combine(liqAltPath, filename);

                var fileInfo = new FileInfo(sourcePath);

                if (File.Exists(sourcePath))
                {
                    var targetFileName = UrlEncode(fileInfo.Name);// $"{file.DocId}{fileInfo.Extension}";

                    CloudFileClient fileClient = StorageAccount.CreateCloudFileClient();
                    var share = fileClient.GetShareReference(Settings.LiqAltUploadFolder);
                    var cloudFile = share.GetRootDirectoryReference();
                    cloudFile.CreateIfNotExists();
                    var cloudFileC = cloudFile.GetFileReference(targetFileName);
                    cloudFileC.Properties.ContentDisposition = "attachment; filename=" + targetFileName;
                    try
                    {
                        cloudFileC.Delete();
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Previous LiqAlt docs deleted from Cloud Azure, The File name is : " + targetFileName));
                    }
                    catch (Exception ex)
                    {
                    }
                   
                    if (File.Exists(sourcePath))
                    {

                        cloudFileC.UploadFromFile(Path.Combine(sourcePath));
                        string fileWithoutExt = Path.GetFileNameWithoutExtension(sourcePath).GetLast(7).Replace("_", "");
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, uploadIncr + " - LiqAlt Docs uploaded , File name is : " + targetFileName));
                        uploadIncr++;
                        using (var conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                        {
                            conn.Execute(@"INSERT INTO tblDocsLiqAlt(FundId,cLiqFileName,cAsOfDate) VALUES (@FundId,@LiqFileName,@AsOfDate)", new { FundId = file.FundId, LiqFileName = targetFileName, AsOfDate = fileWithoutExt.Contains('.') ? new DateTime(Convert.ToInt16(fileWithoutExt.Split('.')[1]), Convert.ToInt16(fileWithoutExt.Split('.')[0]), 1).AddMonths(1).AddDays(-1).ToShortDateString() : new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1).ToShortDateString() });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - LiqAlt cannot be Uploaded into Azure, Partial Filename is :" + file.NewFileName));
            }
        }

        


        private static string UrlEncode(string value)
        {
            if (String.IsNullOrEmpty(value))
                return String.Empty;

            var sb = new StringBuilder();

            foreach (char @char in value)
            {
                if (reservedCharacters.IndexOf(@char) == -1)
                    sb.Append(@char);

            }
            return sb.ToString().Replace("__", "_");
        }

        public static void EmailTriggerForNewDocsPrivateAssetsFocusList()
        {
            try
            {
                _logManagerEvent.Splitter();
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "*******      New Profile report updated PA Focus fund list in Website & Email functionalities   *******"));
                using (var conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    var docList = conn.Query<FileEntry>(@"Exec usp_AlertForNewProfileReport 0", commandTimeout: 900).AsList();
                    if (docList.Count > 0)
                    {
                        StringBuilder sb = new StringBuilder();

                        int i = 1;
                        foreach (var item in docList)
                        {
                            sb.Append("<tr><td>" + i + "</td><td>" + item.FundName + "</td><td>" + item.Path + "</td></tr>\n");
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, i + ") Fund Name is - " + item.FundName + "(" + item.FundId + ") - Url is - " + item.Path));
                            i++;

                        }

                        MailMessage mail = new MailMessage(ConfigurationManager.AppSettings["MailFrom"], ConfigurationManager.AppSettings["MailTo"]);
                        SmtpClient client = new SmtpClient();
                        client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.UseDefaultCredentials = false;
                        NetworkCredential credential = new NetworkCredential(ConfigurationManager.AppSettings["MailUsername"], ConfigurationManager.AppSettings["MailPassword"]);//, "email-smtp.us-west-2.amazonaws.com");
                        client.Credentials = credential;
                        client.EnableSsl=true;
                        client.Host = ConfigurationManager.AppSettings["SMTPHost"];
                        mail.Subject = "Admin: Backstop - PA Focus List PDF Updates";
                        mail.IsBodyHtml = true;
                        mail.Body = "<html><body><p>Hi Team,</p> <p>The Following funds are updated with new doc(pdf) in PA focus list, For more details, please refer the website <a href='https://cliffwater.com/ManagerLibrary/FocusList' target='_blank'>here</a>.</p> <table cellpadding='3' border='1' cellspacing='0' style='width: 850px;border: 1px solid #ccc;'> <thead> <tr><th>S.No</th> <th>Fund</th> <th>Doc Link</th> </tr></thead> <tbody>" + sb.ToString() + " </tbody> </table> <br/>Thanks!</body></html>";
                        client.Send(mail);
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Mail was sent for - " + docList.Count + " Funds"));

                    }
                    else
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "No new docs updated for any PA Focus list funds"));
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Email triggerfor PA focus list funds new docs"));
            }
        }
    }
    public class FileEntry
    {
        public int DocId { get; set; }
        public int FundId { get; set; }
        public string ManagementCompany { get; set; }
        public string Title { get; set; }
        public string FundName { get; set; }
        public string FileName { get; set; }
        public string Author { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Path { get; set; }
        public string FileNameOverride { get; set; }
        public string Type { get; set; }
    }

    public class LiqAltFileEntry
    {
        public int FundId { get; set; }
        public string FundName { get; set; }
        public string NewFileName { get; set; }       
    }

    public class FileDocsEntry
    {
        public int DocId { get; set; }
        public string FileName { get; set; }
        public byte[] FileDocBytes { get; set; }
        public string FileNameOverride { get; set; }
    }

    public static class StringExtension
    {
        public static string GetLast(this string source, int tail_length)
        {
            if (tail_length >= source.Length)
                return source;
            return source.Substring(source.Length - tail_length);
        }
    }
}
