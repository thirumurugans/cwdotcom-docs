﻿using CwAzureApplications.DAL;
using Newtonsoft.Json;
using PrivateEquityReportCreation.ErrorLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Threading;
using System.Xml.Linq;
using BackstopWebService;

namespace CwAzureApplications.BAL
{
    public class PEHoldingBackstopSync
    {
        static LogManager _logManagerEvent = new LogManager("Event");
        static LogManager _logError = new LogManager("Error");


        /* Product Master */
        static readonly string spGetProductBackstop = "[PE].Cwcom_Backstop_Sync_sp_getProductsFromBackstop";

        /* Holding Master */
        static readonly string spGetHoldingBackstop = "[PE].Cwcom_Backstop_Sync_sp_getHoldingMaster";
        static readonly string spSaveHoldingmatserInfoSaveAsXml = "Sync_PE_sp_SaveHoldingMasterAsXML";
        /* Holding Balance */
        static readonly string spGetHoldingBalanceFromAzure = "Sync_PE_GetBalancesForLocalDB";
        static readonly string spGetHoldingBalances = "SyncCoin_PE_sp_SaveHoldingBalanceAsXML";
        /* Holding Summary */
        static readonly string viewDateForRecent = "SELECT * FROM v_Cwcom_Sync_RecentQuarterDate";
        static readonly string spRunPEHoldingDetailsBackstop = "[PE].Cwcom_Backstop_Sync_sp_getPEHoldingSummaryDetails";
        static readonly string spRunPEHoldingDetailsBackstopNew = "[PE].Cwcom_Backstop_Sync_sp_getPEHoldingSummaryDetailsForNew";
        static readonly string spGetPEHoldingDetailsForWebsite = "[PE].Cwcom_Sync_sp_getSaveHoldingSummaryDetails";
        static readonly string azureSavePEHoldingDetailsLocalCurrency = "Sync_PE_sp_SaveHoldingSummaryLocalCurrencyAsXML";
        static readonly string azureSavePEHoldingDetailsUsd = "Sync_PE_sp_SaveHoldingSummaryUSDAsXML";
        static readonly string azureSaveIrrDetails = "Sync_PE_sp_SaveIRRHistoryAsXML";
        static readonly string azureSaveEventSummary = "Sync_PE_sp_SaveHoldingEventSummaryAsXML";
        /* Fund Returns */
        static readonly string spRunPEFundReturnsBackstop = "[PE].Cwcom_Backstop_Sync_sp_getPEFundsReturnsForNew";
        static readonly string spSavePEFundReturnsAsXml = "Cwcom_Backstop_Sync_sp_getPEFundsReturnsForNew";
        /* Transaction */
        static readonly string spGetHoldingTransactionBackstop = "[PE].Cwcom_Backstop_Sync_sp_getHoldingTransaction";
        static readonly string spSaveHoldingTransactionAsXml = "Sync_PE_sp_SaveHoldingTransactionAsXML";

        /* Performance summary Materialize view rebuild */
        static readonly string spPerformanceSummaryRebuildSync = "Sync_PE_sp_PerformanceSummaryRebuildLastOneYearSync";
        static readonly string spPerformanceSummaryRebuildForNewHoldingArrivedSync = "Sync_PE_sp_PerformanceSummaryNewlyArrivedHoldingRebuildSync";


        /* Performance summary Materialize view rebuild */
        static readonly string spPartnershipSummaryRebuildSync = "Sync_PE_sp_PartnershipSummaryRebuildSync";

        //static readonly string azureSavePerformanceSummaryRebuildSync = "Sync_PE_sp_SavePortfolioSummaryAsXML";
        static readonly string viewForProducts = "SELECT * FROM [PE].v_Cwcom_Sync_ProductDetails ORDER BY ProductName";
        static readonly string viewForProductsForNew = "SELECT * FROM [PE].v_Cwcom_Sync_ProductDetailsForNewPartnership ORDER BY ProductName";
        static readonly string viewSingleVintageYear = "SELECT DISTINCT ProductId,ProductName,cAsOfDate DateAs FROM v_Cwcom_Sync_PE_HoldingSingleVintageYearDetails ORDER BY ProductName";

        /* Portfolio Exposure summary Materialize view rebuild */
        static readonly string spPortfolioExposureSummaryRebuildSync = "Sync_PE_sp_PortfolioExposureSummaryRebuildSync";
        //static readonly string spGetSaveForPortfolioExposureSummary = "Cwcom_Sync_PE_sp_PortfolioExposureSummaryDetails";
        //static readonly string azureSavePortfolioExposureSummarySync = "Sync_PE_sp_SavePortfolioExposureSummaryAsXML";

        /* PE Single Holdings Vintage Year IRR */
        static readonly string azureUpdateIrrForSingleHoldingVintageYear = "Sync_PE_sp_UpdateIrrForSingleHoldingVintageYear";

        /* Update PartnershipMaster Mirror table for new Partnership entries maintains */
        static readonly string updatePartnershipMaster = "EXEC PE.Cwcom_Sync_sp_SavePartnershipMasterMirror";

        /* Docs sync */
        static readonly string portfolioDataRebuild = "Sync_PE_sp_GetClientReportsRebuildSync";

        /* Get Azure VM logs summary */
        static readonly string savebackstopCallDetailsSummary = "EXEC [Cliffwater].usp_PE_SaveBackstopCallDetailsSummary";

        private static readonly string spEmergingMarkets = "Cwcom_PE_sp_GetEmergingFundIrrTvpi_Sync";

        static JsonSerializeObject array = null;

        #region "Public Methods"

        public static void RunHoldingDataSync()
        {
            _logManagerEvent.Splitter();
            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding Data Sync process Started..."));

            try
            {
                using (StreamReader file = File.OpenText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "Config.json")))
                {
                    array = JsonConvert.DeserializeObject<JsonSerializeObject>(file.ReadToEnd());
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "JSON configuration read"));
                    LoadProductBackstop();
                    LoadNewUpdatedPartnership();
                    HoldingTransactionDataSync();
                    BackstopService.BackstopToDbForProduct();
                    SyncBackstopForHoldingDetailsLastOneYear();
                    SyncBackstopForHoldingDetailsForNew();
                    SyncAzureGetHoldingDetails();
                    SyncBackstopFundReturnsForLastOneYear();
                    SyncAzureSavePortfolioSummary();
                    SyncAzureSavePortfolioSummaryHistoryForNew();
                    SyncAzureSavePortfolioExposureSummary();
                    SyncAzureUpdateIrr();
                    SyncAzureSavePartnershipSummary();
                    UpdateNewPartnershipToMirrorTable();
                    PeClientReportUpdate();
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-Holding Data Sync process main Method!"));
            }
            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding Data Sync process Completed..!"));
            _logManagerEvent.Splitter();
        }

        #endregion

        private static void HoldingTransactionDataSync()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PA Holding Transaction Summary sync initialized..."));
                SqlParameter[] paramsArray =
                {
                    new SqlParameter("@DateAs",array.HoldingAsOfDate)
                };
                DataSet backstopdetailssDB = DataAccess.GetResultAsDataSet(paramsArray, spGetHoldingTransactionBackstop, "", System.Reflection.MethodBase.GetCurrentMethod().Name + "- PA Holding Transaction Summary data from Backstop");
                if (backstopdetailssDB.Tables.Count > 0)
                {
                    StringWriter objStr = new StringWriter();
                    backstopdetailssDB.Tables[0].WriteXml(objStr);
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "XML parsing for PA Holding Transaction Summary)"));
                    SqlParameter[] paramsArraySave =
                    {
                        new SqlParameter("@XMLData",objStr.ToString())
                    };
                    // var s = ;                   
                    int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spSaveHoldingTransactionAsXml, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "- PA Holding Transaction Summary stored into CW Azure DB");
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, result > 0 ? "PA Holding Transaction Summary stored into CW Azure DB - Rows " + result : "PA Holding Transaction Summary details not stored.!"));
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-PA Holding Transaction Summary data sync!"));
            }
        }

        private static void LoadProductBackstop()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Product masters details Re-Load sync initialized..."));

                SqlParameter[] paramsArray =
                { 
                    // Load all Holding from Backstop to PE DB if '0' mean
                    new SqlParameter("@DateAs",array.HoldingAsOfDate),
                    new SqlParameter("@ProductID","0")
                    
                };
                DataSet productDetailsDB = DataAccess.GetResultAsDataSet(paramsArray, spGetProductBackstop, "", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Product masters details from Backstop");
                if (productDetailsDB.Tables.Count > 0)
                {
                    if (productDetailsDB.Tables[0].Rows.Count > 0)
                    {
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Product masters details count been Loaded from Backstop :=>" + productDetailsDB.Tables[0].Rows.Count));
                    }
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-Product masters details Re-Load sync!"));
            }
        }

        private static void LoadNewUpdatedPartnership()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding masters details Re-Load sync initialized..."));

                SqlParameter[] paramsArray =
                { 
                    // Load all Holding from Backstop to PE DB if '0' mean
                    new SqlParameter("@ProductID","0"),
                    new SqlParameter("@DateAs",array.HoldingAsOfDate)
                };
                DataSet holdingDetailsDB = DataAccess.GetResultAsDataSet(paramsArray, spGetHoldingBackstop, "", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Holding masters details from Backstop");
                if (holdingDetailsDB.Tables.Count > 0)
                {
                    StringWriter objStr = new StringWriter();
                    holdingDetailsDB.Tables[0].WriteXml(objStr);
                    StringWriter objStrSec = new StringWriter();
                    holdingDetailsDB.Tables[1].WriteXml(objStrSec);
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "XML parsing for Holding Master details"));
                    SqlParameter[] paramsArraySave =
                    {
                        new SqlParameter("@HoldingMasterInfo",objStr.ToString()),
                        new SqlParameter("@StrategyMasterInfo",objStrSec.ToString()),
                        new SqlParameter("@AsOfDate",array.HoldingAsOfDate)
                    };
                    int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spSaveHoldingmatserInfoSaveAsXml, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Holding summary XML data Stored into CW Azure DB");
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, result > 0 ? "Holding Masters XML data Stored into CW Azure DB - Rows " + holdingDetailsDB.Tables[0].Rows.Count : " Holding summary details not stored.!"));

                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-Holding masters details Re-Load sync!"));
            }
        }

        private static void SyncFromAzureToLocalSaveBalancesDetails()
        {
            try
            {

                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding Balances save from Azure to Local DB-Load sync initialized..."));

                SqlParameter[] paramsArray =
                { 
                    // Load all Holding from Backstop to PE DB if '0' mean
                    new SqlParameter("@ProductID","0"),
                    new SqlParameter("@DateAs",array.HoldingAsOfDate)
                };
                DataSet holdingBalanceDetailsDB = DataAccess.GetResultAsDataSet(paramsArray, spGetHoldingBalanceFromAzure, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Holding Balances save from Azure to Local DB - Get the data from Azure");
                if (holdingBalanceDetailsDB.Tables.Count > 0)
                {
                    StringWriter objStr = new StringWriter();
                    holdingBalanceDetailsDB.Tables[0].WriteXml(objStr);
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding Balances save from Azure to Local DB as XML - Intialized"));
                    SqlParameter[] paramsArraySave =
                {
                    new SqlParameter("@HoldingBalanceDetails",objStr.ToString()),
                    new SqlParameter("@AsOfDate",array.HoldingAsOfDate)
                };
                    int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spGetHoldingBalances, "", System.Reflection.MethodBase.GetCurrentMethod().Name + " - Holding Balances save from Azure to Local DB - Local DB Save procedure Called...!");
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding Balances save from Azure to Local DB - Successfully"));
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Holding Balances save from Azure to Local DB - Not Successfully "));
            }
        }

        private static void SyncBackstopForHoldingDetailsLastOneYear()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PA Holding Summary for last one year of all existing to backstop sync initialized..."));
                using (var conn = new SqlConnection(DataAccess.connectionStringAzure))
                {

                    var recentQuarter = conn.Query<string>(viewDateForRecent, commandTimeout: 360000).ToList();
                    SqlParameter[] paramsArray = { new SqlParameter("@DateAs", recentQuarter[0]) };
                    DataAccess.GetResultAsDataSet(paramsArray, spRunPEHoldingDetailsBackstop, "", System.Reflection.MethodBase.GetCurrentMethod().Name + "- PA Holding Summary for last one year of all existing to backstop data from Backstop");
                }
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PA Holding Summary for last one year of all existing to backstop sync Completed"));
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-Holding Summary for last one year of all existing to backstop Re-Load sync!"));
            }
        }

        private static void SyncBackstopForHoldingDetailsForNew()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PA Holding Summary history for newly arrived fund in backstop sync initialized..."));
                using (var conn = new SqlConnection(DataAccess.connectionStringAzure))
                {
                    var recentQuarter = conn.Query<string>(viewDateForRecent, commandTimeout: 360000).ToList();
                    SqlParameter[] paramsArray = { new SqlParameter("@DateAs", recentQuarter[0]) };
                    DataAccess.GetResultAsDataSet(paramsArray, spRunPEHoldingDetailsBackstopNew, "", System.Reflection.MethodBase.GetCurrentMethod().Name + "- PA HoldingSummary history for newly arrived fund in backstop data from Backstop");
                }
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PA Holding Summary history for newly arrived fund in backstop sync Completed"));
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-Summary history for newly arrived fund in backstop Re-Load sync!"));
            }
        }
        private static void SyncBackstopFundReturnsForLastOneYear()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PE Fund Returns for last one year of all existing to backstop sync Initialized..."));
                using (var conn = new SqlConnection(DataAccess.connectionStringAzure))
                {
                    var recentQuarter = conn.Query<string>(viewDateForRecent, commandTimeout: 360000).ToList();
                    SqlParameter[] paramsArray = { new SqlParameter("@DateAs", recentQuarter[0]) };
                    var getDetails = DataAccess.GetResultAsDataSet(paramsArray, spRunPEFundReturnsBackstop, "", System.Reflection.MethodBase.GetCurrentMethod().Name + "- PE Fund Returns for last one year of all existing to backstop data from Backstop");
                    if (getDetails.Tables.Count > 0)
                    {
                        if (getDetails.Tables[0].Rows.Count > 0)
                        {
                            StringWriter objStr = new StringWriter();
                            getDetails.Tables[0].WriteXml(objStr);
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "XML parsing for PE Fund Returns)"));
                            SqlParameter[] paramsArraySave =
                                                 {
                                                new SqlParameter("@XMLData",objStr.ToString())
                                                 };
                            // var s = ;                   
                            int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spSavePEFundReturnsAsXml, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "-PE Fund Returns stored into CW Azure DB");
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, result > 0 ? "PE Fund Returns stored into CW Azure DB - Rows " + result : "PE Fund Returns details not stored.!"));
                        }
                    }
                }
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PE Fund Returns for last one year of all existing to backstop sync Completed"));
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-PE Fund Returns for last one year of all existing to backstop Re-Load sync!"));
            }
        }

        private static void SyncAzureGetHoldingDetails()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PA Holding Summary Save to Azure sync initialized..."));
                SqlParameter[] paramsArray =
                {
                    new SqlParameter("@DateAs",array.HoldingAsOfDate)
                };
                DataSet ds = DataAccess.GetResultAsDataSet(paramsArray, spGetPEHoldingDetailsForWebsite, "", System.Reflection.MethodBase.GetCurrentMethod().Name + "- PA Holding Summary Save to Azure");
                List<AsyncDbCall> list = new List<AsyncDbCall>();
                list.Add(new AsyncDbCall() { spName = azureSavePEHoldingDetailsLocalCurrency, DTable = ds.Tables[0], Mesage = "Sync to CWCOM Azure DB - Save PE Holding summary with Local Currency details" });
                list.Add(new AsyncDbCall() { spName = azureSavePEHoldingDetailsUsd, DTable = ds.Tables[1], Mesage = "Sync to CWCOM Azure DB - Save PE Holding summary with USD($-Conversion #'s) details" });
                list.Add(new AsyncDbCall() { spName = azureSaveIrrDetails, DTable = ds.Tables[2], Mesage = "Sync to Azure DB - Save PE Holding IRR History details" });
                list.Add(new AsyncDbCall() { spName = azureSaveEventSummary, DTable = ds.Tables[3], Mesage = "Sync to Azure DB - Save PE Holding Significant Events & Monitoring summary details" });

                Parallel.ForEach(list, SyncAzureSaveHoldingDetails);
                GC.SuppressFinalize(ds);
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-Holding masters details Re-Load sync!"));
            }
        }

        private static void SyncAzureSaveHoldingDetails(AsyncDbCall list)
        {
            try
            {
                StringWriter objStr = new StringWriter();
                list.DTable.WriteXml(objStr);
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, list.Mesage + " - Intialized"));
                SqlParameter[] paramsArraySave =
                {
                    new SqlParameter("@HoldingDetails",objStr.ToString()),
                    new SqlParameter("@AsOfDate",array.HoldingAsOfDate)
                };
                int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, list.spName, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + " - " + list.Mesage);
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, result > 0 ? list.Mesage + " Completed - Rows " + list.DTable.Rows.Count : list.Mesage + " -Not Stored"));
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - " + list.Mesage));
            }
        }

        private static void SyncAzureSavePortfolioSummary()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Cwcom - Sync - Performance Summary Rebuild to PE DB Materialized view Table initialized..."));
                DateTime start = DateTime.Now;
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Parallel Execution start :- " + start));

                using (var conn = new SqlConnection(DataAccess.connectionStringCoin2))
                {
                    string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                    var listProduct = conn.Query<ProductDetails>(viewForProducts, commandTimeout: 360000).ToList();

                    Parallel.For(0, listProduct.Count, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, i =>
                    {


                        SqlParameter[] paramsArray =
                        {
                            new SqlParameter("@ProductId",listProduct[i].ProductId),
                            new SqlParameter("@DateAs",listProduct[i].DateAs)
                        };

                        DataSet ds = DataAccess.GetResultAsDataSet(paramsArray, spPerformanceSummaryRebuildSync, "azure", methodName + "- Cwcom - Sync - Performance Summary Rebuild to Azure Materialized view Table");
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), methodName, (i + 1) + ") Inception date is :- " + listProduct[i].InceptionDate + " - Date As Of :-" + listProduct[i].DateAs + " - Product Name :-" + listProduct[i].ProductName));

                        RebuildLogs(ds, methodName);
                        GC.SuppressFinalize(ds);
                    });
                }


                DateTime end = DateTime.Now;
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Parallel Execution End :- " + end));
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Parallel Execution Time for execution :- " + end.Subtract(start)));
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Performance Summary Rebuild Materialized view Table!"));
            }
        }

        private static void SyncAzureSavePortfolioSummaryHistoryForNew()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Cwcom - Sync - Performance Summary Rebuild to PE DB Materialized view Table initialized..."));
                DateTime start = DateTime.Now;
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Parallel Execution start :- " + start));

                using (var conn = new SqlConnection(DataAccess.connectionStringCoin2))
                {
                    string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                    var listProduct = conn.Query<ProductDetails>(viewForProductsForNew, commandTimeout: 360000).ToList();
                    Parallel.For(0, listProduct.Count, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, i =>
                    {


                        SqlParameter[] paramsArray =
                        {
                            new SqlParameter("@ProductId",listProduct[i].ProductId),
                            new SqlParameter("@DateAs",listProduct[i].DateAs)
                        };

                        DataSet ds = DataAccess.GetResultAsDataSet(paramsArray, spPerformanceSummaryRebuildForNewHoldingArrivedSync, "azure", methodName + "- Cwcom - Sync - Performance Summary Rebuild to Azure Materialized view Table");
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), methodName, (i + 1) + ") Inception date is :- " + listProduct[i].InceptionDate + " - Date As Of :-" + listProduct[i].DateAs + " - Product Name :-" + listProduct[i].ProductName));

                        RebuildLogs(ds, methodName);
                        GC.SuppressFinalize(ds);
                    });
                }


                DateTime end = DateTime.Now;
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Parallel Execution End :- " + end));
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Parallel Execution Time for execution :- " + end.Subtract(start)));
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Performance Summary Rebuild Materialized view Table!"));
            }
        }

        private static void SyncAzureSavePartnershipSummary()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Cwcom - Sync - Performance Summary Rebuild to PE DB Materialized view Table initialized..."));
                DateTime start = DateTime.Now;
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Parallel Execution start :- " + start));

                using (var conn = new SqlConnection(DataAccess.connectionStringCoin2))
                {
                    string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                    var listProduct = conn.Query<ProductDetails>(viewForProducts, commandTimeout: 360000).ToList();

                    Parallel.For(0, listProduct.Count, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, i =>
                    {


                        SqlParameter[] paramsArray =
                        {
                            new SqlParameter("@ProductId",listProduct[i].ProductId),
                            new SqlParameter("@DateAs",listProduct[i].DateAs)
                        };

                        DataSet ds = DataAccess.GetResultAsDataSet(paramsArray, spPartnershipSummaryRebuildSync, "azure", methodName + "- Cwcom - Sync - Performance Summary Rebuild to Azure Materialized view Table");
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), methodName, (i + 1) + ") Inception date is :- " + listProduct[i].InceptionDate + " - Date As Of :-" + listProduct[i].DateAs + " - Product Name :-" + listProduct[i].ProductName));
                        //StringWriter objStr = new StringWriter();
                        //ds.Tables[0].WriteXml(objStr);
                        //_logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Cwcom - Sync - Performance Summary data to Save Azure Materialized view Table - Intialized..."));
                        //SqlParameter[] paramsArraySave = { new SqlParameter("@HoldingDetails",objStr.ToString()),
                        //                       new SqlParameter("@AsOfDate",array.HoldingAsOfDate)                                                                                                                        
                        //       };
                        //int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, azureSavePerformanceSummaryRebuildSync, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + " - Cwcom - Sync - Performance Summary data to Save Azure Materialized view Table - Process...");
                        RebuildLogs(ds, methodName);
                        GC.SuppressFinalize(ds);
                    });
                }


                DateTime end = DateTime.Now;
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Parallel Execution End :- " + end));
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Parallel Execution Time for execution :- " + end.Subtract(start)));
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Performance Summary Rebuild Materialized view Table!"));
            }
        }

        private static void SyncAzureSavePortfolioExposureSummary()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Cwcom - Sync - Portfolio Exposure Summary(Strategy,Sector,Geography,VintageYear) Rebuild to PE DB Materialized view Table initialized..."));
                DateTime start = DateTime.Now;
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Parallel Execution start :- " + start));

                using (var conn = new SqlConnection(DataAccess.connectionStringCoin2))
                {
                    string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                    var listProduct = conn.Query<ProductDetails>(viewForProducts, commandTimeout: 360000).ToList();

                    Parallel.For(0, listProduct.Count, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, i =>
                    {


                        SqlParameter[] paramsArray =
                        {
                            new SqlParameter("@ProductId",listProduct[i].ProductId),
                            new SqlParameter("@DateAs",listProduct[i].DateAs)
                        };

                        DataSet ds = DataAccess.GetResultAsDataSet(paramsArray, spPortfolioExposureSummaryRebuildSync, "azure", methodName + "- Cwcom - Sync -  Portfolio Exposure Summary Rebuild to save Local(PrivateEquity) Materialized view Table");
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), methodName, (i + 1) + ")  Inception date is :- " + listProduct[i].InceptionDate.ToString("MM/dd/yyyy") + " - Date As Of :-" + listProduct[i].DateAs.ToString("MM/dd/yyyy")));
                        RebuildLogs(ds, methodName);
                        GC.SuppressFinalize(ds);
                    });
                    DateTime end = DateTime.Now;
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Parallel Execution End :- " + end));
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Parallel Execution Elapsed Time for execution :- " + end.Subtract(start)));

                    //SqlParameter[] paramsArrayGetSave =
                    //    {
                    //        new SqlParameter("@ProductId","0"),
                    //        new SqlParameter("@DateAs",array.HoldingAsOfDate)
                    //    };
                    //DataSet dSset = DataAccess.GetResultAsDataSet(paramsArrayGetSave, spGetSaveForPortfolioExposureSummary, "", methodName + "- Cwcom - Sync -  Portfolio Exposure Summary Rebuild to save Local(PrivateEquity) Materialized view Table");
                    //StringWriter objStr = new StringWriter();
                    //dSset.Tables[0].WriteXml(objStr);
                    //_logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), methodName, "- Cwcom - Sync -  Portfolio Exposure Summary Rebuild to Save Azure Materialized view Table - Intialized..."));
                    //SqlParameter[] paramsArraySave = { new SqlParameter("@PEExposureDetails",objStr.ToString()),
                    //                            new SqlParameter("@AsOfDate",array.HoldingAsOfDate)                                                                                                                        
                    //            };
                    //int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, azureSavePortfolioExposureSummarySync, "azure", methodName + " - Cwcom - Sync -  Portfolio Exposure Summary data to Save Azure Materialized view Table - Process...");
                    //GC.SuppressFinalize(dSset);                 
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception -  Portfolio Exposure Summary Rebuild Materialized view Table!"));
            }
        }

        /// <summary>
        /// Update IRR for which product is having single holding for any vintage year
        /// </summary>
        private static void SyncAzureUpdateIrr()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Cwcom - Sync - IRR Update for Single holding for any vintage year process initialized..."));
                DateTime start = DateTime.Now;
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Parallel Execution start :- " + start));

                using (var conn = new SqlConnection(DataAccess.connectionStringAzure))
                {
                    string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                    var listProduct = conn.Query<ProductDetails>(viewSingleVintageYear, commandTimeout: 360000).ToList();

                    Parallel.For(0, listProduct.Count, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, i =>
                    {


                        SqlParameter[] paramsArray =
                        {
                            new SqlParameter("@ProductId",listProduct[i].ProductId),
                            new SqlParameter("@DateAs",listProduct[i].DateAs)
                        };

                        DataAccess.GetResultAsDataSet(paramsArray, azureUpdateIrrForSingleHoldingVintageYear, "azure", methodName + "- Cwcom - Sync -  IRR Update for Single holding for any vintage year process strated in Local DB");
                        //if (ds.Tables.Count > 0)
                        //{
                        //    if(ds.Tables[0].Rows.Count>0)
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), methodName, (i + 1) + ") Inception date is :- " + listProduct[i].InceptionDate + " - Date As Of :-" + listProduct[i].DateAs + " - Product Name :-" + listProduct[i].ProductName));
                        //}
                        //GC.SuppressFinalize(ds);
                    });
                    DateTime end = DateTime.Now;
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Parallel Execution End :- " + end));
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Parallel Execution Elapsed Time for execution :- " + end.Subtract(start)));
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception -  IRR Update for Single holding for any vintage year process End!"));
            }
        }

        private static void UpdateNewPartnershipToMirrorTable()
        {
            int count = 0;
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Cwcom - Sync - Partnership Master Mirror Table Updation Start"));

                using (var conn = new SqlConnection(DataAccess.connectionStringCoin2))
                {
                    count = conn.Execute(updatePartnershipMaster, commandTimeout: 360000);
                }
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Cwcom - Sync - Partnership Master Mirror Table Updation End"));
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception -  Partnership Master Mirror Table Updation!"));
            }
        }

        private static void PeClientReportUpdate()
        {
            int count = 0;
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Cwcom - Sync - PE Client reports Rebuild for Portfolio Page Start"));

                using (var conn = new SqlConnection(DataAccess.connectionStringAzure))
                {
                    count = conn.Execute(portfolioDataRebuild, commandTimeout: 360000);
                }
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Cwcom - Sync - PE Client reports Rebuild for Portfolio Page End"));
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Sync -  PE Client reports Rebuild for Portfolio Page!"));
            }
        }


        public static void SaveBackstopCallSummaryLogs()
        {
            try
            {

                using (var conn2 = new SqlConnection(DataAccess.connectionStringCoin2))
                {
                    using (var conn = new SqlConnection(DataAccess.connectionStringAzure))
                    {
                        var listSummary = conn2.Query<dynamic>(savebackstopCallDetailsSummary, commandTimeout: 360000).ToList();
                        foreach (var summary in listSummary)
                        {

                            conn.Execute("INSERT INTO Website_Admin_tbl_SyncServiceDetails(ServiceName,ProcedureName,TotalBackstopCalls,DownloadedBytes,ExecutionTime,ExecutedOn)"
                                            + " VALUES (@Service,@Procedure,@Calls,@BytesVal,@ExecutionTime,@ExecutedOn)",
                                            new
                                            {
                                                Service = summary.ServiceName,
                                                Procedure = summary.ProcedureName,
                                                Calls = summary.TotalBackstopCalls,
                                                BytesVal = summary.DownloadedBytes,
                                                ExecutionTime = summary.ExecutionTime,
                                                ExecutedOn = summary.ExecutedOn
                                            });

                        }
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Cwcom - Sync - Backstop SOAP API Call logs summary to Azure DB Completed. The Rows Count is " + listSummary.Count));
                    }
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Sync -  Save Backstop Call logs summary to Azure DB!"));
            }
        }
        // private void BackstopToDb()
        //{
        //    try
        //    {
        //        PortfolioService.BackstopPortfolioService_1_11 rs = new PortfolioService.BackstopPortfolioService_1_11();
        //        rs.Timeout = 36000000;
        //        rs.LoginInfo = new PortfolioService.LoginInfoType()
        //        {
        //            Username = BACKSTOP_USERNAME,
        //            Password = BACKSTOP_PASSWORD
        //        };
        //        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Ssl3 | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;

        //        rs.Url = BACKSTOP_URL + new Uri(rs.Url).PathAndQuery;
        //        using (var conn = new SqlConnection("Server=cliffwater.database.windows.net;Database=cwdev;User ID=thirumurugan;Password=UCcrqdt+"))
        //        {
        //            var SqlResult = conn.Query<dynamic>("SELECT PartnershipId,cInceptionDate InceptionDate,ProductId FROM tbl_PE_PartnershipMaster PM WHERE cInceptionDate!='01/01/1900' AND ProductId IN (SELECT ProductId FROM vtbl_BackstopDataSync) AND ProductId=1279685 AND PartnershipId IN (SELECT DISTINCT HoldingId FROM Cwcom_Sync_PE_tbl_PE_HoldingBalances WHERE cDate='09/30/2016')", commandTimeout:3600000).AsList();

        //            foreach (var item in SqlResult)
        //            {
        //                var SqlResultTransaction = conn.Query<dynamic>("SELECT MAX(cEffectiveDate) EffectiveDate FROM Cwcom_Sync_PE_tblHoldingTransactionSummary H, tbl_PE_PartnershipMaster P  WHERE  H.HoldingId=P.PartnershipId AND P.ProductId=@ProductId", new { ProductId = item.ProductId }, commandTimeout: 3600000);
        //                if(SqlResultTransaction!=null)
        //                { 
        //                reports = rs.getComprehensiveHoldingBalancesByHolding(new int?[] { Convert.ToInt32(item.PartnershipId) }, Convert.ToDateTime(item.InceptionDate), SqlResultTransaction.Any()? SqlResultTransaction.FirstOrDefault().EffectiveDate : Convert.ToDateTime("12/31/2017"), false,false,true);
        //                    if (reports.Count() > 0)
        //                    {
        //                       // conn.Execute("DELETE FROM Cwcom_Sync_PE_tbl_PE_HoldingBalances WHERE HoldingId=@HoldingId", new { HoldingId = Convert.ToInt32(item.PartnershipId) }, commandTimeout: 3600000);
        //                        var reportsSpecific = reports.OrderBy(x => x.holdingOpenedDate).ToList();
        //                        if (reportsSpecific.Count() > 0)
        //                        {
        //                            //var date = reportsSpecific[0].holdingOpenedDate; //Give you own DateTime
        //                            //int offset = 2, monthsInQtr = 3;

        //                            //var quarter = (date.Month + offset) / monthsInQtr; //To find which quarter 
        //                            //var totalMonths = quarter * monthsInQtr;
        //                            //var endDateInQtr = new DateTime(date.Year, totalMonths, DateTime.DaysInMonth(date.Year, totalMonths));
        //                            // conn.Execute("INSERT INTO Cwcom_Sync_PE_tbl_PE_HoldingBalances(HoldingId,cDate,BalanceAmount,PeriodForReturn) VALUES (@HoldingId,@Date,@BalanceAmount,@PeriodReturn)", new { HoldingId = reportsSpecific[0].holdingId, Date = endDateInQtr, BalanceAmount = reportsSpecific[0].amount, PeriodReturn = reportsSpecific[0].returnForPeriod });

        //                            var xml = new XElement("Root", from x in reportsSpecific
        //                                                           select new XElement("Holding",
        //                                                                      new XElement("HoldingId", x.holdingId),
        //                                                                      new XElement("BalanceDate", x.balanceDate),
        //                                                                      new XElement("HoldingAmount", x.amount),
        //                                                                      new XElement("ReturnForPeriod", x.returnForPeriod)

        //                                                              ));
        //                            conn.Query(@"EXEC Sync_PE_sp_SaveHoldingBalancesAsXML @xml", new {xml=xml.ToString() }, commandTimeout: 3600000);
        //                            //foreach (var Subitem in reportsSpecific)
        //                            //{
        //                            //    try
        //                            //    {

        //                            //        //conn.Execute("INSERT INTO Cwcom_Sync_PE_tbl_PE_HoldingBalances(HoldingId,cDate,BalanceAmount,PeriodForReturn) VALUES (@HoldingId,@Date,@BalanceAmount,@PeriodReturn)", new { HoldingId = Subitem.holdingId, Date = Subitem.balanceDate, BalanceAmount = Subitem.amount, PeriodReturn = Subitem.returnForPeriod },commandTimeout:3600000);
        //                            //        conn.Query($"EXEC Sync_PE_sp_SaveHoldingBalancesAsXML {}", commandTimeout: 3600000);
        //                            //    }
        //                            //    catch (Exception ex)
        //                            //    {

        //                            //    }

        //                            //}
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}

        private static void RebuildLogs(DataSet ds, string methodName)
        {
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[ds.Tables.Count - 1].Rows)
                {
                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["Logs"])))
                    {
                        string[] arr = { "@@" };
                        string[] splitLogs = Convert.ToString(row["Logs"]).Split(arr, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string strlog in splitLogs)
                        {
                            if (!strlog.ToLower().Contains("exception"))
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), methodName, strlog));
                            else
                                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), methodName, strlog));
                            //for (int i = 0; i < ds.Tables.Count - 1; i++)
                            //{

                            //}
                        }
                    }
                }
            }
        }
    }

    public class AsyncDbCall
    {
        public string spName { get; set; }

        public DataTable DTable { get; set; }

        public string Mesage { get; set; }
    }
}