﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using CwAzureApplications.DAL;
using CwAzureApplications.WebService.Service;
using System.IO.Compression;
using System.Threading;
using System.Text.RegularExpressions;

namespace CwAzureApplications.BAL
{
    
    public class AllFundDocsObj
    {
        private static string path = ConfigurationManager.AppSettings["HFFundDocs"];

        public static void CreateZip(int productId)
        {
            try
            {
                CreateDirectory(path);
                using (var connection = new SqlConnection(DataAccess.connectionStringAzure))
                {
                    var docsDetails = connection.Query<dynamic>("EXEC Website_HF_AllFundDocsNSJB @ProductId", new { ProductId = productId }).AsList();
                    foreach (var product in docsDetails.Select(x => new { x.ProductName }).Distinct())
                    {
                        string portfolioFolder = path + "\\" + product.ProductName;
                        CreateDirectory(portfolioFolder);
                        BackstopCrmService.ServiceCallMethod();
                        InvestedOrNonInvestedFundsFolder(docsDetails, portfolioFolder, "Invested");
                        InvestedOrNonInvestedFundsFolder(docsDetails, portfolioFolder, "Not Invested");
                    }
                }
            }
            catch(Exception ex)
            {
            }
        }

        private static void InvestedOrNonInvestedFundsFolder(List<dynamic> docsDetails, string portfolioFolder,string fundType)
        {
            string quotes = "\"\"";
            foreach (var fund in docsDetails.Where(x => x.Invested == fundType).Select(x => new { x.FundId, x.FundName }).Distinct())
            {
                //string investedFundFolder = portfolioFolder + "\\" + fundType + "\\" + fund.FundName.Replace(@"\", "-").Replace("/", "-").Replace(":", "").Replace(quotes, "'").Replace("“","'");
                string investedFundFolder = portfolioFolder + "\\" + fundType + "\\" + Validname(fund.FundName);
                CreateDirectory(investedFundFolder);
                foreach (var file in docsDetails.Where(x => x.FundId == fund.FundId))
                {
                    if (file.Type == "Document")
                    {
                        var binary = BackstopCrmService.GetBinaryDocumentWithServiceAccess(Convert.ToInt32(file.DocId));
                        //using (Stream files = File.OpenWrite(Path.Combine(investedFundFolder + "\\" + file.FileName.Replace(@"\", "-").Replace("/", "-").Replace(":", "").Replace(quotes, "'").Replace("“", "'"))))
                        using (Stream files = File.OpenWrite(Path.Combine(investedFundFolder + "\\" + Validname(file.FileName))))
                        {
                            files.Write(binary, 0, binary.Length);
                        }
                    }
                    else
                    {
                        File.WriteAllText(Path.Combine(investedFundFolder + "\\" + Validname(file.FileName)), file.Descn);
                    }
                }
                if(File.Exists(investedFundFolder + ".zip"))
                    File.Delete(investedFundFolder + ".zip");
                ZipFile.CreateFromDirectory(investedFundFolder, investedFundFolder + ".zip");
                Thread.Sleep(1000);
                System.IO.DirectoryInfo di = new DirectoryInfo(investedFundFolder);
                foreach (FileInfo file in di.EnumerateFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.EnumerateDirectories())
                {
                    dir.Delete(true);
                }
                if (Directory.Exists(investedFundFolder))
                    di.Delete(true);
                

            }
        }

        private static string Validname(string name)
        {
            return Regex.Replace(name, "[^a-zA-Z0-9% ._()]", string.Empty);
        }

        private static string CleanFileName(string fileName)
        {
            return Path.GetInvalidFileNameChars().Aggregate(fileName, (current, c) => current.Replace(c.ToString(), string.Empty));
        }

        private static string CleanFolder(string fileName)
        {
            return Path.GetInvalidPathChars().Aggregate(fileName, (current, c) => current.Replace(c.ToString(), string.Empty));
        }

        private static void CreateDirectory(string folder)
        {
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
        }
    }
}
