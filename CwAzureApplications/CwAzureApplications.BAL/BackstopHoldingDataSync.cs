﻿using CwAzureApplications.DAL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PrivateEquityReportCreation.ErrorLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CwAzureApplications.BAL
{
    public class BackstopHoldingDataSync
    {
        private static LogManager _logManagerEvent = new LogManager("Event");
        private static LogManager _logError = new LogManager("Error");
        private static readonly string spGetHoldingBackstop = "usp_cwcom_getHoldingsFromBackstop";
        private static readonly string spSaveHoldingmatserInfoSaveAsXml = "usp_SavePrivateEquityHoldingMasterAsXML";

        private static readonly string spGetHoldingSummaryFromBackstop = "PrivateEquity_usp_getPrivateEquityHoldingSummaryFromBackstop";
        private static readonly string spGetHoldingSummaryPreviousFromBackstop = "usp_cwcom_getPrivateEquityHoldingSummaryPreviousQuarterFromBackstop";
        private static readonly string spSaveHoldingSummaryInfoSaveAsXml = "usp_SavePrivateEquityHoldingSummaryAsXML";

        private static readonly string spGetHoldingHistoryFromBackstop = "PrivateEquity_usp_getPrivateEquityHoldingIrrHistoryFromBackstop";
        private static readonly string spSaveHoldingHistoryInfoSaveAsXml = "usp_SavePrivateEquityHoldingHistoryAsXML";

        private static readonly string spGetHoldingEventSummaryFromBackstop = "PrivateEquity_usp_getPrivateEquityHoldingEventSummaryFromBackstop";
        private static readonly string spSaveHoldingEventSummaryInfoSaveAsXml = "usp_SavePrivateEquityHoldingEventSummaryAsXML";

        private static readonly string spGetHoldingSummaryUsdFromBackstop = "usp_cwcom_getPerformanceSummaryHistoryBackstop";
        private static readonly string spSaveHoldingSummaryUsdSaveAsXml = "usp_SavePrivateEquityHoldingSummaryUSDAsXML";

        private static readonly string spGetHoldingTransactionBackstop = "usp_cwcom_getTransactionFromBackstop";
        private static readonly string spSaveHoldingTransactionAsXml = "usp_SavePrivateEquityHoldingTransactionAsXML";

        // All newly added holding & All AsOfDate
        private static readonly string spGetAllHoldingEventSummaryFromBackstop = "PrivateEquity_usp_getPrivateEquityAllHoldingEventSummaryFromBackstop";
        private static readonly string spGetAllHoldingHistoryFromBackstop = "PrivateEquity_usp_getAllHoldingHistoryFromBackstop";

        // All Product rebuild
        private static readonly string spRunPartnershipSummaryRebuild = "usp_paPartnershipSummaryRebuildSync";
        private static readonly string spRunPortfolioIrrHistoryRebuild = "usp_paPortfolioIrrTvpiRebuildSync";
        private static readonly string spRunPerformanceSummaryRebuild = "usp_paPerformanceSummaryRebuildSync";
        private static readonly string spRunPortfolioExposureRebuild = "usp_paPortfolioExposureIrrRebuildSync";

        private static readonly string spRunPerformanceSummaryLastYears = "usp_cwcom_getPerformanceSummaryLast10YearsFromBackstop";
        private static readonly string spRunSavePerformanceSummaryLastTenYears = "usp_SavePrivateEquityHoldingSummaryLast10YearsAsXML";
        static JsonSerializeObject array = null;

        #region "Public Methods"
        public static void RunHoldingDataSync()
        {
            _logManagerEvent.Splitter();
            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding Data Sync process Started..."));

            try
            {
                using (StreamReader file = File.OpenText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "Config.json")))
                {

                    array = JsonConvert.DeserializeObject<JsonSerializeObject>(file.ReadToEnd());
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "JSON configuration read"));
                    LoadNewUpdatedPartnership();
                    HoldingSummaryDataSync();
                    HoldingSummaryPreviousQtrSync();
                    HoldingTransactionDataSync();                   
                }
            }
            catch(Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-Holding Data Sync process main Method!"));
            }
            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding Data Sync process Completed..!"));
            _logManagerEvent.Splitter();
        }
        public static void RunHoldingHistorySync()
        {
            _logManagerEvent.Splitter();
            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding History Sync process Started..."));

            try
            {
                using (StreamReader file = File.OpenText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "Config.json")))
                {
                    array = JsonConvert.DeserializeObject<JsonSerializeObject>(file.ReadToEnd());
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "JSON configuration read"));
                    HoldingSummaryDollarConversionDataSync();
                    HoldingHistoryDataSync();
                    HoldingEventSummaryDataSync();
                    HoldingHistoryDataForNewlyAddedPartnershipSync();
                    HoldingSummaryEventHistoryForNewPartnershipSync();
                    HoldingPerformanceSummaryLastTenYears();
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-Holding History Sync process main Method!"));
            }
            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding History Sync process Completed..!"));
            _logManagerEvent.Splitter();
        }
        public static void RebuildOptmizationTable()
        {
            _logManagerEvent.Splitter();
            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "****************Post Sync - Rebuild all Holding Data to move Optmize Table(Perforamance Summary & IRR History Performance...*******************"));
            try
            {
                using (StreamReader file = File.OpenText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "Config.json")))
                {
                    array = JsonConvert.DeserializeObject<JsonSerializeObject>(file.ReadToEnd());
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "JSON configuration read"));                    
                    RebuildPartnershipSummary();
                    RebuildPortfolioIrrHistory();
                    //_logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Partnership Summary rebuild run again to update the IRR & TVPI"));                    
                    RebuildPartnershipSummary();
                    RebuildPerformanceSummary();
                    RebuildPortfolioExposureIrr();
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Post Sync - Rebuild all Holding Data process main Method!"));
            }
            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "****************Post Sync - Rebuild all Holding Data process Completed..!*******************"));
            _logManagerEvent.Splitter();
        }
        #endregion

        #region "Private Methods"
        private static void LoadNewUpdatedPartnership()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding masters details Re-Load sync initialized..."));

                SqlParameter[] paramsArray = { 
                                                 // Load all Holding from Backstop to PE DB if '0' mean
                                                 new SqlParameter("@ProductID","0")  ,
                                            new SqlParameter("@DateAs",array.HoldingAsOfDate)                                                                         
                                         };
                DataSet holdingDetailsDB = DataAccess.GetResultAsDataSet(paramsArray, spGetHoldingBackstop, "", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Holding masters details from Backstop");
                if (holdingDetailsDB.Tables.Count > 0)
                {
                    StringWriter objStr = new StringWriter();
                    holdingDetailsDB.Tables[0].WriteXml(objStr);
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "XML parsing for Holding Master details"));
                    SqlParameter[] paramsArraySave = { new SqlParameter("@HoldingMasterInfo",objStr.ToString()),
                                                         new SqlParameter("@AsOfDate",array.HoldingAsOfDate)                                                                                                                        
                                         };
                    int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spSaveHoldingmatserInfoSaveAsXml, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Holding summary XML data Stored into CW Azure DB");
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, result > 0 ? "Holding Masters XML data Stored into CW Azure DB - Rows " + holdingDetailsDB.Tables[0].Rows.Count : " Holding summary details not stored.!"));
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-Holding masters details Re-Load sync!"));
            }
        }
        
        private static void HoldingSummaryDataSync()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding Summary data sync initialized..."));
                SqlParameter[] paramsArray = { 
                                            new SqlParameter("@DateAs",array.HoldingAsOfDate)                                                                         
                                         };
                DataSet activityDetailsDB = DataAccess.GetResultAsDataSet(paramsArray, spGetHoldingSummaryFromBackstop, "", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Holding Summary Data from Backstop");
                if (activityDetailsDB.Tables.Count > 0)
                {
                    StringWriter objStr = new StringWriter();
                    activityDetailsDB.Tables[0].WriteXml(objStr);
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "XML parsing for Summary details"));
                    SqlParameter[] paramsArraySave = { new SqlParameter("@HoldingSummaryInfo",objStr.ToString()),
                                                         new SqlParameter("@AsOfDate",array.HoldingAsOfDate)                                                                                                                        
                                         };
                    int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spSaveHoldingSummaryInfoSaveAsXml, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Holding summary XML data Stored into CW Azure DB");
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, result > 0 ? "Holding summary XML data Stored into CW Azure DB - Rows" + activityDetailsDB.Tables[0].Rows.Count : "Holding summary details not stored.!"));
                }
            }
            catch(Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-Holding Summary data sync!"));
            }
        }        
        private static void HoldingSummaryPreviousQtrSync()
        {
            DateTime firstDayOfTheMonth = new DateTime(Convert.ToDateTime(array.HoldingAsOfDate).AddMonths(-3).Year, Convert.ToDateTime(array.HoldingAsOfDate).AddMonths(-3).Month, 1);
            string previousQtr = firstDayOfTheMonth.AddMonths(1).AddDays(-1).ToString("MM/dd/yyyy");
            string asOfDate = previousQtr;
            try
            {                
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding Summary data sync initialized...(The Previous Date is for " + asOfDate+")"));
                SqlParameter[] paramsArray = { 
                                            new SqlParameter("@DateAs",asOfDate)                                                                         
                                         };
                DataSet activityDetailsDB = DataAccess.GetResultAsDataSet(paramsArray, spGetHoldingSummaryPreviousFromBackstop, "", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Holding Summary Data from Backstop (The Previous Date is for " + asOfDate + ")");
                if (activityDetailsDB.Tables.Count > 0)
                {
                    StringWriter objStr = new StringWriter();
                    activityDetailsDB.Tables[0].WriteXml(objStr);
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "XML parsing for Summary details (The Previous Date is for " + asOfDate + ")"));
                    SqlParameter[] paramsArraySave = { new SqlParameter("@HoldingSummaryInfo",objStr.ToString()),
                                                         new SqlParameter("@AsOfDate",asOfDate)                                                                                                                        
                                         };
                    int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spSaveHoldingSummaryInfoSaveAsXml, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Holding summary XML data Stored into CW Azure DB (The Previous Date is for " + asOfDate + ")");
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, result > 0 ? "Holding summary XML data Stored into CW Azure DB - Rows " + activityDetailsDB.Tables[0].Rows.Count : " Holding summary details not stored.! (The Previous Date is for " + asOfDate + ")"));
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-Holding Summary data sync!"));
            }
        }        

        private static void HoldingSummaryDollarConversionDataSync()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding Summary History data with USD(dollar converted Value) sync initialized..."));
                SqlParameter[] paramsArray = { 
                                            new SqlParameter("@DateAs",array.HoldingAsOfDate)                                                                         
                                         };
                DataSet backstopdetailssDB = DataAccess.GetResultAsDataSet(paramsArray, spGetHoldingSummaryUsdFromBackstop, "", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Holding Summary History data with USD(dollar converted Value) data from Backstop");
                if (backstopdetailssDB.Tables.Count > 0)
                {
                    StringWriter objStr = new StringWriter();
                    backstopdetailssDB.Tables[0].WriteXml(objStr);
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "XML parsing for Holding Summary History data with USD(dollar converted Value)"));
                    SqlParameter[] paramsArraySave = { new SqlParameter("@XMLData",objStr.ToString())                                                                                                                                                                                 
                                         };
                    // var s = ;                   
                    int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spSaveHoldingSummaryUsdSaveAsXml, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Holding Summary History data with USD(dollar converted Value) stored into CW Azure DB");
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, result > 0 ? "Holding Summary History data with USD(dollar converted Value) Stored into CW Azure DB - Rows " + result : "Holding Summary History data with USD(dollar converted Value) details not stored.!"));
                }

            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-Holding Summary History data with USD(dollar converted Value) data sync!"));
            }
        }

        private static void HoldingTransactionDataSync()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PA Holding Transaction Summary sync initialized..."));
                SqlParameter[] paramsArray = { 
                                            new SqlParameter("@DateAs",array.HoldingAsOfDate)                                                                         
                                         };
                DataSet backstopdetailssDB = DataAccess.GetResultAsDataSet(paramsArray, spGetHoldingTransactionBackstop, "", System.Reflection.MethodBase.GetCurrentMethod().Name + "- PA Holding Transaction Summary data from Backstop");
                if (backstopdetailssDB.Tables.Count > 0)
                {
                    StringWriter objStr = new StringWriter();
                    backstopdetailssDB.Tables[0].WriteXml(objStr);
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "XML parsing for PA Holding Transaction Summary)"));
                    SqlParameter[] paramsArraySave = { new SqlParameter("@XMLData",objStr.ToString())                                                                                                                                                                                 
                                         };
                    // var s = ;                   
                    int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spSaveHoldingTransactionAsXml, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "- PA Holding Transaction Summary stored into CW Azure DB");
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, result > 0 ? "PA Holding Transaction Summary stored into CW Azure DB - Rows " + result : "PA Holding Transaction Summary details not stored.!"));
                }

            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-PA Holding Transaction Summary data sync!"));
            }
        }

        private static void HoldingHistoryDataSync()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding IRR History data sync initialized..."));
                SqlParameter[] paramsArray = { 
                                            new SqlParameter("@DateAs",array.HoldingHistoryAsOfDate)                                                                         
                                         };
                DataSet activityDetailsDB = DataAccess.GetResultAsDataSet(paramsArray, spGetHoldingHistoryFromBackstop, "", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Holding IRR History Data from Backstop");
                if (activityDetailsDB.Tables.Count > 0)
                {
                    StringWriter objStr = new StringWriter();
                    activityDetailsDB.Tables[0].WriteXml(objStr);
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "XML parsing for IRR History details"));
                    SqlParameter[] paramsArraySave = { new SqlParameter("@HoldingHistoryInfo",objStr.ToString()),
                                                         new SqlParameter("@AsOfDate",array.HoldingHistoryAsOfDate)
                                                                                                                        
                                         };
                    // var s = ;                   
                    int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spSaveHoldingHistoryInfoSaveAsXml, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Holding IRR History XML data Stored into CW Azure DB");
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, result > 0 ? "IRR History XML data Stored into CW Azure DB - Rows" + activityDetailsDB.Tables[0].Rows.Count : "Holding IRR History details not stored.!"));
                }

            }
            catch(Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-Holding IRR History data sync!"));
            }
        }

        private static void HoldingHistoryDataForNewlyAddedPartnershipSync()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding IRR History data for newly added partnership sync initialized..."));
                SqlParameter[] paramsArray = { 
                                            new SqlParameter("@DateAs",array.HoldingHistoryAsOfDate)                                                                         
                                         };
                DataSet activityDetailsDB = DataAccess.GetResultAsDataSet(paramsArray, spGetAllHoldingHistoryFromBackstop, "", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Holding IRR History Data for newly added partnership from Backstop");
                if (activityDetailsDB.Tables.Count > 0)
                {
                    StringWriter objStr = new StringWriter();
                    activityDetailsDB.Tables[0].WriteXml(objStr);
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "XML parsing for IRR History details for newly added partnership"));
                    SqlParameter[] paramsArraySave = { new SqlParameter("@HoldingHistoryInfo",objStr.ToString()),
                                                         new SqlParameter("@AsOfDate",array.HoldingHistoryAsOfDate)
                                                                                                                        
                                         };
                    // var s = ;                   
                    int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spSaveHoldingHistoryInfoSaveAsXml, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Holding IRR History for newly added partnership as a XML data Stored into CW Azure DB");
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, result > 0 ? "IRR History for newly added partnership as a XML data Stored into CW Azure DB - Rows " + activityDetailsDB.Tables[0].Rows.Count : " Holding IRR History details for newly added partnership not stored.!"));
                }

            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-Holding IRR History data sync!"));
            }
        }

        private static void HoldingSummaryEventHistoryForNewPartnershipSync()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding Event & Monitoring Summary data for new partnership sync initialized..."));
                //JObject jobject = JObject.Parse(File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory+ "Config.json")));

                SqlParameter[] paramsArray = { 
                                            new SqlParameter("@DateAs",array.HoldingEventSummaryAsOfDate)                                                                         
                                         };
                DataSet activityDetailsDB = DataAccess.GetResultAsDataSet(paramsArray, spGetAllHoldingEventSummaryFromBackstop, "", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Holding Event & Monitoring Summary Data for new partnership from Backstop");
                if (activityDetailsDB.Tables.Count > 0)
                {
                    StringWriter objStr = new StringWriter();
                    activityDetailsDB.Tables[0].WriteXml(objStr);
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "XML parsing for Event & Monitoring Summary details for new partnership"));
                    SqlParameter[] paramsArraySave = { new SqlParameter("@HoldingEventSummaryInfo",objStr.ToString()),
                                                         new SqlParameter("@AsOfDate",array.HoldingEventSummaryAsOfDate)
                                                                                                                        
                                         };
                    // var s = ;                   
                    int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spSaveHoldingEventSummaryInfoSaveAsXml, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Event & Monitoring Summary for new partnership");
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, result > 0 ? "Event & Monitoring Summary XML data for new partnership Stored into CW Azure DB - Rows" + activityDetailsDB.Tables[0].Rows.Count : "Event & Monitoring Summary details not stored.!"));
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-Holding Event & Monitoring Summary data for new partnership sync!"));
            }
        }

        private static void HoldingEventSummaryDataSync()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding Event & Monitoring Summary data sync initialized..."));
                //JObject jobject = JObject.Parse(File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory+ "Config.json")));

                SqlParameter[] paramsArray = { 
                                            new SqlParameter("@DateAs",array.HoldingEventSummaryAsOfDate)                                                                         
                                         };
                DataSet activityDetailsDB = DataAccess.GetResultAsDataSet(paramsArray, spGetHoldingEventSummaryFromBackstop, "", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Holding Event & Monitoring Summary Data from Backstop");
                if (activityDetailsDB.Tables.Count > 0)
                {
                    StringWriter objStr = new StringWriter();
                    activityDetailsDB.Tables[0].WriteXml(objStr);
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "XML parsing for Event & Monitoring Summary details"));
                    SqlParameter[] paramsArraySave = { new SqlParameter("@HoldingEventSummaryInfo",objStr.ToString()),
                                                         new SqlParameter("@AsOfDate",array.HoldingEventSummaryAsOfDate)
                                                                                                                        
                                         };
                    // var s = ;                   
                    //DataSet ds = DataAccess.GetResultAsDataSet(paramsArraySave, spSaveHoldingEventSummaryInfoSaveAsXml, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Event & Monitoring Summary");
                   // int result = 0;
                    int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spSaveHoldingEventSummaryInfoSaveAsXml, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Event & Monitoring Summary");
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, result > 0 ? "Event & Monitoring Summary XML data Stored into CW Azure DB - Rows" + activityDetailsDB.Tables[0].Rows.Count : "Event & Monitoring Summary details not stored.!"));
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-Holding Event & Monitoring Summary data sync!"));
            }
        }

       
        private static void RebuildPartnershipSummary()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Rebuild Portfolio performance summary sync initialized..."));
                SqlParameter[] paramsArray = {new SqlParameter("@ProductId","0"), 
                                            new SqlParameter("@DateAs",array.HoldingAsOfDate)                                                                         
                                         };
                
                    DataSet ds = DataAccess.GetResultAsDataSet(paramsArray,spRunPartnershipSummaryRebuild, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Rebuild performance summary for all portfolio to be processing......");
                    // This is for logs
                    RebuildLogs(ds, System.Reflection.MethodBase.GetCurrentMethod().Name);
                
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Rebuild performance summary for all portfolio Completed...!"));


            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-Holding IRR History data sync!"));
            }
        }

        private static void RebuildPortfolioIrrHistory()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Rebuild Portfolio IRR &TVPI History performance sync initialized..."));
                SqlParameter[] paramsArray = {new SqlParameter("@ProductId","0"), 
                                            new SqlParameter("@DateAs",array.HoldingAsOfDate)                                                                         
                                         };
                DataSet ds = DataAccess.GetResultAsDataSet(paramsArray, spRunPortfolioIrrHistoryRebuild, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Rebuild IRR &TVPI History performance for all portfolio to be processing......");
                // This is for logs
                RebuildLogs(ds, System.Reflection.MethodBase.GetCurrentMethod().Name);
               _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Rebuild IRR &TVPI History performance for all Portfolio Completed...!"));


            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-Rebuild IRR &TVPI History Performance for all portfolio data sync!"));
            }
        }

        private static void RebuildPerformanceSummary()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "******** Post Sync=>  Rebuild Portfolio performance summary sync initialized...*********"));
                SqlParameter[] paramsArray = {new SqlParameter("@ProductId","0"), 
                                            new SqlParameter("@DateAs",array.HoldingAsOfDate)                                                                         
                                         };
                DataSet ds = DataAccess.GetResultAsDataSet(paramsArray, spRunPerformanceSummaryRebuild, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Processing for all portfolio......");
                // This is for logs
                RebuildLogs(ds, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Post Sync=>  Rebuild Portfolio performance summary sync Completed...!"));


            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception- Post Sync=>  Rebuild Portfolio performance summary sync!"));
            }
        }

        private static void RebuildPortfolioExposureIrr()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "******** Post Sync=>  Rebuild Portfolio Exposure sync initialized...*********"));
                SqlParameter[] paramsArray = {new SqlParameter("@ProductId","0"), 
                                            new SqlParameter("@DateAs",array.HoldingAsOfDate)                                                                         
                                         };
                DataSet ds = DataAccess.GetResultAsDataSet(paramsArray, spRunPortfolioExposureRebuild, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Processing for all portfolio(Exposure)......");
                // This is for logs
                RebuildLogs(ds, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Post Sync=>  Rebuild Portfolio Exposure sync Completed...!"));


            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception- Post Sync=>  Rebuild Portfolio Exposure sync!"));
            }
        }

        private static void RebuildLogs(DataSet ds,string methodName)
        {
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[ds.Tables.Count - 1].Rows)
                {
                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["Logs"])))
                    {
                        string[] arr = { "@@" };
                        string[] splitLogs = Convert.ToString(row["Logs"]).Split(arr, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string strlog in splitLogs)
                        {
                            if (!strlog.ToLower().Contains("exception"))
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), methodName, strlog));
                            else
                                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), methodName, strlog));
                            //for (int i = 0; i < ds.Tables.Count - 1; i++)
                            //{

                            //}
                        }
                    }
                }
            }
        }

        //spRunPerformanceSummaryLastYears
        private static void HoldingPerformanceSummaryLastTenYears()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Holding Performance summary for Last 10 years all quarter data sync initialized..."));
                SqlParameter[] paramsArray = { 
                                            new SqlParameter("@DateAs",array.HoldingHistoryAsOfDate)                                                                         
                                         };
                DataSet activityDetailsDB = DataAccess.GetResultAsDataSet(paramsArray, spRunPerformanceSummaryLastYears, "", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Holding Performance summary for Last 10 years from Backstop");
                if (activityDetailsDB.Tables.Count > 0)
                {
                    StringWriter objStr = new StringWriter();
                    activityDetailsDB.Tables[0].WriteXml(objStr);
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "XML parsing for IRR History details"));
                    SqlParameter[] paramsArraySave = { new SqlParameter("@XMLData",objStr.ToString())                                                         
                                                                                                                        
                                         };
                    // var s = ;                   
                    int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spRunSavePerformanceSummaryLastTenYears, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Holding IRR History XML data Stored into CW Azure DB");
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, result > 0 ? "IRR History XML data Stored into CW Azure DB - Rows -" + activityDetailsDB.Tables[0].Rows.Count : "Holding IRR History details not stored.!"));
                }

            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception-Holding IRR History data sync!"));
            }
        }
        #endregion
    }
    
    public class JsonSerializeObject
    {
        public string HoldingAsOfDate { get; set; }
        public string HoldingHistoryAsOfDate { get; set; }
        public string HoldingHistoryAllOrSingleDate { get; set; }
        public string HoldingEventSummaryAsOfDate { get; set; }
        public string HoldingEventSummaryAllOrSingleDate { get; set; }
        public string IsDataSync = "true";
        public string ExcelIrrPath { get; set; }
    }
}
