﻿using CwAzureApplications.DAL;
using CwAzureApplications.WebService.Entities;
using CwAzureApplications.WebService.Service;
using PrivateEquityReportCreation.ErrorLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace CwAzureApplications.BAL
{
    public class BackstopCrmAccessActivities
    {
        #region "Variables"
        private static readonly string spGetActivityFromBackstop = "[PE].Cwcom_Backstop_Sync_sp_GetActivityFromBackstop";
        private static readonly string spSaveActivityInfoSaveAsXml = "usp_Save_4_ActivityInfoSaveAsXML";
        private static readonly string spGetActvityIdType = "usp_Get_4_ActvityIdType";
        private static readonly string spSaveActvityInfoRefresh = "usp_Save_4_ActivityInfoRefresh";
        private static readonly string spDashboardDataRebuild = "usp_DashboardMaterializedViewRebuildSync";
        private static readonly string spPortfolioDataRebuild = "Sync_PE_sp_GetClientReportsRebuildSync";
        private static readonly string spDocsMissingDetails = "Website_Docs_GetDueDiligenceSiblingForMissing_Sync";
        private static readonly string spPAQuarterlySummary = "Website_Admin_PE_GetQuarterlySummaryComparison_Sync";
        private static readonly string spFundOptimization = "usp_OptimizeAzureDBMasterDataNew";
        
        #endregion
        #region "Logs object"
        private static LogManager _logManagerEvent = new LogManager("Event");
        private static LogManager _logError = new LogManager("Error");
        #endregion
        public static void ActivityDetailsUpdates()
        {
            try
            {
                _logManagerEvent.Splitter();
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "******      Activity Details DB sync Started...     ******"));
                SqlParameter[] paramsArray = { new SqlParameter("@ActivityId","0") ,
                                            new SqlParameter("@Dateasof","03/31/2016")                                                                         
                                         };
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Activity fetch from Backstop"));
                DataSet activityDetailsDB = DataAccess.GetResultAsDataSet(paramsArray, spGetActivityFromBackstop, "", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Activity fetch from Backstop");
                if (activityDetailsDB.Tables.Count > 0)
                {
                    StringWriter objStr = new StringWriter();
                    activityDetailsDB.Tables[0].WriteXml(objStr);
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Activity Details DB sync & converting as XML to save DB"));
                    SqlParameter[] paramsArraySave = { new SqlParameter("@ActivityInfo",objStr.ToString())                                                                                    
                                         };
                    // var s = ;
                    int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spSaveActivityInfoSaveAsXml, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name);
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Activity Details are saved into DB"));
                }
                else
                {
                    _logManagerEvent.LogWarningMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "No Activity details from Backstop"));
                }
                SqlParameter[] paramsArrayActivity = { new SqlParameter("@DocID",0)
                                                                                                                    
                                         };
                DataSet dtActivity = DataAccess.GetResultAsDataSet(paramsArrayActivity, spGetActvityIdType, "azure");
                if (dtActivity.Tables.Count > 0)
                    if (dtActivity.Tables.Count > 0)
                    {
                        if (dtActivity.Tables.Count > 0)
                        {
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Activity Details DB sync for new docs/Calls/Meeting HTML Description...."));
                            BackstopCrmService.GetActivityDescriptionDetails(dtActivity.Tables[0]);
                        }
                    }



            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Activity Details DB sync failed"));
            }
        }
        public static void Activity()
        {
            BackstopCrmService.GetActivityDescription();
        }
        public static void ActivityDetailsRelationRefresh()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "DB sync with existing Activity details & update data"));
                SqlParameter[] paramsArraySave = { new SqlParameter("@DocId","0")                                                                                    
                                         };
                // var s = ;
                int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spSaveActvityInfoRefresh, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "******      Activity Details DB sync Finished...     ******"));

            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Activity Details Relation refresh sync failed"));
            }
        }

        public static void OptmizeFundsTableRun()
        {
            _logManagerEvent.Splitter();
            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "******      Fund Optmization Started...     ******"));
            try
            {
                SqlParameter[] paramsArray = { new SqlParameter("@DateAs","0"),                                                                                                                   
                                         };
                DataSet optimizeDB = DataAccess.GetResultAsDataSet(paramsArray, spFundOptimization, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name + "- Fund Optmization process");
                if (optimizeDB.Tables.Count > 0)
                {
                    RebuildLogs(optimizeDB, System.Reflection.MethodBase.GetCurrentMethod().Name);
                }
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "******      Fund Optmization Ended...     ******"));
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Fund Optmization sync failed"));
            }
            _logManagerEvent.Splitter();
        }


        public static void DashboardDataRebuild()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Dashboard data rebuild to Materialized view table DB sync Started...."));
                SqlParameter[] paramsArraySave = { new SqlParameter("@Email","0")                                                                                    
                                         };
                // var s = ;
                int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spDashboardDataRebuild, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Dashboard data rebuild to Materialized view table DB sync Finished...."));
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Dashboard HF & PE client reports data rebuild sync failed"));
            }

        }
        public static void AdminMissingDocsDataRebuild()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Admin Docs missing Materilizaed(tblDocsMissingDetails) View Table for all Funds sync Started...."));
                SqlParameter[] paramsArraySave = { new SqlParameter("@ReportType","0")                                                                                    
                                         };
                // var s = ;
                int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spDocsMissingDetails, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Admin Docs missing Materilizaed(tblDocsMissingDetails) View Table for all Funds sync Finished...."));
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Admin Docs missing Materilizaed(tblDocsMissingDetails) View Table for all Funds sync failed"));
            }

        }
        public static void PAQuarterlyComparisonDataRebuild()
        {
            try
            {
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Admin Quarterly Summary Comparison Data Rebuild for all PE Funds sync Started...."));
                using (var conn = new SqlConnection(DataAccess.connectionStringAzure))
                {
                    string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                    var listProduct = conn.Query<dynamic>("EXEC Website_PE_Portfolio_GetEditPortfoilioHoldingsRecentDates", commandTimeout: 360000).ToList();
                    foreach (var list in listProduct)
                    {
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Admin Quarterly Summary Comparison Data Rebuild for all PE Funds sync Finished for the As Of Date " + list.Dates.ToString()));
                        SqlParameter[] paramsArraySave = { new SqlParameter("@ProductIDs",""),
                                                     new SqlParameter("@Dateasof",list.Dates),
                                                     new SqlParameter("@WithoutProduct","0")                                                                                 
                                         };
                        int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spPAQuarterlySummary, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name);
                    }
                }
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Admin Quarterly Summary Comparison Data Rebuild for all PE Funds sync Finished...."));
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Admin Quarterly Summary Comparison Data Rebuild for all PE Funds sync Failed"));
            }

        }
        //public static void PortfolioClientReportRebuild()
        //{
        //    try
        //    {
        //        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Dashboard data rebuild to Materialized view table DB sync Started...."));
        //        SqlParameter[] paramsArraySave = { new SqlParameter("@Email","0")                                                                                    
        //                                 };
        //        // var s = ;
        //        int result = DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spPortfolioDataRebuild, "azure", System.Reflection.MethodBase.GetCurrentMethod().Name);
        //        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Dashboard data rebuild to Materialized view table DB sync Findished...."));
        //    }
        //    catch (Exception ex)
        //    {
        //        _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
        //        _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Dashboard HF & PE client reports data rebuild sync failed"));
        //    }

        //}

        private static void RebuildLogs(DataSet ds, string methodName)
        {
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[ds.Tables.Count - 1].Rows)
                {
                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["Logs"])))
                    {
                        string[] arr = { "^^" };
                        string[] splitLogs = Convert.ToString(row["Logs"]).Split(arr, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string strlog in splitLogs)
                        {
                            if (!strlog.ToLower().Contains("exception"))
                                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), methodName, strlog));
                            else
                                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), methodName, strlog));
                            //for (int i = 0; i < ds.Tables.Count - 1; i++)
                            //{

                            //}
                        }
                    }
                }
            }
        }
    }
}
