﻿using System;
using System.Collections.Generic;
using CwAzureApplications.BAL.Model;

namespace CwAzureApplications.BAL.Email
{
    public interface IEmailProvider
    {
        Tuple<bool, string> SendUMBPowerStationReport(UMBReportType report, List<CCLFUploadRequest> reportFiles);
    }
}