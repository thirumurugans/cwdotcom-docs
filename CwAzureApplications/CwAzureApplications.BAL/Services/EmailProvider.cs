﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CwAzureApplications.BAL.Model;
using CwAzureApplications.BAL.Email;

namespace UMBImportScrape.Email
{

    public class EmailProvider : IEmailProvider
    {
        private readonly IBuildEmailMessage _buildEmailMessage;
        private readonly IEmailSender _emailSender;

        public EmailProvider(IBuildEmailMessage buildEmailMessage, IEmailSender emailSender)
        {
            _buildEmailMessage = buildEmailMessage;
            _emailSender = emailSender;
        }

        public Tuple<bool, string> SendUMBPowerStationReport(UMBReportType report, List<CCLFUploadRequest> reportFiles)
        {
            var emailMessage = _buildEmailMessage.UMBPowerStationReportMessage(report, reportFiles);
            return _emailSender.SendEmail(emailMessage);
        }
    }
}
