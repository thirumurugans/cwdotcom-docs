﻿using CwAzureApplications.DAL;
using Dapper;

using OfficeOpenXml;
using OfficeOpenXml.Style;
using PrivateEquityReportCreation.ErrorLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;


namespace CwAzureApplications.BAL
{
    public class PAFundIrrTvpiExcelBuild
    {
        private readonly static LogManager logManagerEvent = new LogManager("Event");
        private readonly static LogManager logError = new LogManager("Error");
        private readonly static string excelFund = ConfigurationManager.AppSettings["CompositeIRRTVPI"];
        private readonly static string impersonateUser = ConfigurationManager.AppSettings["ImpersonateUser"];
        private readonly static string impersonatePass = ConfigurationManager.AppSettings["ImpersonatePass"];
        private readonly static string impersonateDomain = ConfigurationManager.AppSettings["ImpersonateDomain"];
        static int count = 1;
        static int total = 0;

        public static void SyncCreateExcelForIrrs()
        {
            try
            {
                Directory.CreateDirectory(excelFund);
            }
            catch (Exception ex)
            {
                logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void SyncCreateExcelForIrr()
        {
            count = 1;
            total = 0;
            logManagerEvent.Splitter();
            logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Fund IRR & TVPI Excel generate Sync process Started... "));
            logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, excelFund));
            //DeleteDirectory(excelFund);
            CreateAllFolderToCopy();
            using (var connection = new SqlConnection(DataAccess.connectionStringAzure))
            {
                //WHERE FundId IN (1316791,1316059)
                string query = "SELECT *  FROM vSync_PE_CompositeIrrTvpi  ORDER BY AsOfDate DESC,FundName ASC";
                //string query = "SELECT *  FROM vSync_PE_CompositeIrrTvpiRecentUpdatedQuarterFunds WHERE AsOfDate='06/30/2021'  ORDER BY AsOfDate DESC,FundName ASC";
                var helpDocsArticlesResult = connection.Query<dynamic>(query).AsList();
                total = helpDocsArticlesResult.Count;
                logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Fund IRR & TVPI Excel generate Sync process Total Count... " + total));
                foreach (var val in helpDocsArticlesResult)
                {
                    try
                    {
                        connection.Execute("Sync_PE_sp_FundCompositeIrrTvpi @FundId,@DateAs", new { FundId = val.FundId, DateAs = val.AsOfDate }, commandTimeout: 200000);
                        CreateFundIrrExcelFile(val.FundId.ToString(), val.AsOfDate.ToString("MM/dd/yyyy"));
                    }
                    catch (Exception ex)
                    {
                        logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                        logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "(" + count + " of " + total + ") Failed for FundId :-" + val.FundId));
                    }
                    count++;
                }
                CreateAllFolderToCopy();
            }
        }
        private static void DeleteDirectory(string targetdir)
        {
            //using (var imp = new Impersonation(impersonateDomain, impersonateUser, impersonatePass))
            //{

                try
                {
                    string[] files = Directory.GetFiles(targetdir);
                    string[] dirs = Directory.GetDirectories(targetdir);
                    foreach (string file in files)
                    {
                        try
                        {
                            File.SetAttributes(file, FileAttributes.Normal);
                            File.Delete(file);
                        }
                        catch (Exception ex)
                        {
                            logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                            logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Cannot delete the File -" + file));
                        }
                    }

                    foreach (string dir in dirs)
                    {
                        try
                        {
                            DeleteDirectory(dir);
                        }
                        catch (Exception ex)
                        {
                            logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                            logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Cannot delete the Sub Directory -" + dir));
                        }
                    }

                    Directory.Delete(targetdir, false);
                }
                catch (Exception ex)
                {
                    logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                    logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Cannot delete the Main Directory -" + targetdir));
                }
           // }
        }
        private static void CreateFundIrrExcelFile(string fundId, string dateasof)
        {
            // Byte[] byteVal = null;
            //string fundId = "1315681";//1315681,1388147
            //string fundName = "Advent International GPE VII";
            //string dateasof = "12/31/2017";
            string sFileName = string.Empty;

            using (var conn = new SqlConnection(DataAccess.connectionStringAzure))
            {

                using (ExcelPackage package = new ExcelPackage())
                {
                    List<PAIrrTvpi> peIrrList = conn.Query<PAIrrTvpi>("EXEC Website_PE_Admin_GetFundForExcelIrrTvpi @FundId,@DateAs", new
                    {
                        FundId = fundId,
                        DateAs = dateasof
                    }, commandTimeout: 36000).AsList();
                    if (peIrrList.Count > 0)
                    {
                        try
                        {
                            string folder = excelFund + (peIrrList.FirstOrDefault().Analyst == string.Empty ? "No Analyst" : peIrrList.FirstOrDefault().Analyst);
                            CreateFolder(folder);

                            sFileName = folder + @"\" + peIrrList.FirstOrDefault().Qtr + " " + peIrrList.FirstOrDefault().FundName.Replace("/", "-").Replace(@"\", "-") + ".xlsx";
                            ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Composite IRR & TVPI");
                            worksheet.Cells["A1"].Value = "Asset Class";
                            worksheet.Cells["B1"].Value = "Strategy";
                            worksheet.Cells["C1"].Value = "Analyst";
                            worksheet.Cells["D1"].Value = "IRR";
                            worksheet.Cells["E1"].Value = "Quarterly Change in IRR";
                            worksheet.Cells["F1"].Value = "TVPI";
                            worksheet.Cells["G1"].Value = "Quarterly Change in TVPI";
                            worksheet.Cells["H1"].Value = "DPI";
                            worksheet.Cells["I1"].Value = "Quarterly Change in DPI";
                            worksheet.Cells["J1"].Value = "EffectiveDate";
                            worksheet.Cells["K1"].Value = "Fair Value";
                            worksheet.Cells["L1"].Value = "Quarterly Contributions";
                            worksheet.Cells["M1"].Value = "Quarterly Distributions";
                            worksheet.Cells["N1"].Value = "Fair Value % using Cont./Dist.Change";
                            int i = 0;
                            foreach (var single in peIrrList.Where(x => x.RowNumber > 4).OrderByDescending(x => x.RowNumber))
                            {
                                worksheet.Cells["A" + (i + 2)].Value = single.AssetClass;
                                worksheet.Cells["B" + (i + 2)].Value = single.Strategy;
                                worksheet.Cells["C" + (i + 2)].Value = single.Analyst;
                                worksheet.Cells["D" + (i + 2)].Value = single.IRR == string.Empty ? 0 : Convert.ToDouble(single.IRR) / 100;
                                worksheet.Cells["D" + (i + 2) + ":D" + (i + 2)].Style.Numberformat.Format = single.IRR == "0" ? "_(* #,##0_);_(* (#,##0);_(* " + "-" + "_);_(@_)" : "0.00%";
                                if (i == 0)
                                {
                                    worksheet.Cells["E" + (i + 2)].Value = 0;
                                    worksheet.Cells["E" + (i + 2) + ":E" + (i + 2)].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* " + "-" + "_);_(@_)";
                                }
                                else
                                {
                                    worksheet.Cells["E" + (i + 2)].Formula = "=IF((D" + (i + 2) + "-D" + (i + 1) + ")=0,0,(D" + (i + 2) + "-D" + (i + 1) + "))";
                                    worksheet.Cells["E" + (i + 2) + ":E" + (i + 2)].Style.Numberformat.Format = "0.00%";
                                }
                                worksheet.Cells["F" + (i + 2)].Value = single.TVPI == string.Empty ? 0 : Convert.ToDouble(single.TVPI);
                                worksheet.Cells["F" + (i + 2) + ":F" + (i + 2)].Style.Numberformat.Format = single.TVPI == "0" ? "_(* #,##0_);_(* (#,##0);_(* " + "-" + "_);_(@_)" : "0.00x";
                                if (i == 0)
                                {
                                    worksheet.Cells["G" + (i + 2)].Value = 0;
                                    worksheet.Cells["G" + (i + 2) + ":G" + (i + 2)].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* " + "-" + "_);_(@_)";
                                }
                                else
                                {
                                    worksheet.Cells["G" + (i + 2)].Formula = "=IF((F" + (i + 2) + "-F" + (i + 1) + ")=0,0,(F" + (i + 2) + "-F" + (i + 1) + ")/(F" + (i + 2) + "))/100";
                                    worksheet.Cells["G" + (i + 2) + ":G" + (i + 2)].Style.Numberformat.Format = "0.00%";
                                }
                                worksheet.Cells["H" + (i + 2)].Value = single.DPI == string.Empty ? 0 : Convert.ToDouble(single.DPI);
                                worksheet.Cells["H" + (i + 2) + ":H" + (i + 2)].Style.Numberformat.Format = single.TVPI == "0" ? "_(* #,##0_);_(* (#,##0);_(* " + "-" + "_);_(@_)" : "0.00x";
                                if (i == 0)
                                {
                                    worksheet.Cells["I" + (i + 2)].Value = 0;
                                    worksheet.Cells["I" + (i + 2) + ":I" + (i + 2)].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* " + "-" + "_);_(@_)";
                                }
                                else
                                {
                                    worksheet.Cells["I" + (i + 2)].Formula = "=IF((H" + (i + 2) + "-H" + (i + 1) + ")=0,0,(H" + (i + 2) + "-H" + (i + 1) + ")/(H" + (i + 2) + "))/100";
                                    worksheet.Cells["I" + (i + 2) + ":I" + (i + 2)].Style.Numberformat.Format = "0.00%";
                                }
                                worksheet.Cells["J" + (i + 2)].Value = single.EffectiveDate;
                                worksheet.Cells["J" + (i + 2)].Style.Numberformat.Format = "MM/DD/YYYY";
                                worksheet.Cells["K" + (i + 2)].Value = single.FairValue;
                                worksheet.Cells["K" + (i + 2)].Style.Numberformat.Format = "$#,##0_);[Red]($#,##0)";
                                worksheet.Cells["L" + (i + 2)].Value = single.QuarterlyContribution;
                                worksheet.Cells["L" + (i + 2)].Style.Numberformat.Format = "$#,##0_);[Red]($#,##0)";
                                worksheet.Cells["M" + (i + 2)].Value = single.QuarterlyDistribution;
                                worksheet.Cells["M" + (i + 2)].Style.Numberformat.Format = "$#,##0_);[Red]($#,##0)";
                                if (i == 0)
                                {
                                    worksheet.Cells["N" + (i + 2)].Value = 0;
                                    worksheet.Cells["N" + (i + 2) + ":N" + (i + 2)].Style.Numberformat.Format = "_(* #,##0_);_(* (#,##0);_(* " + "-" + "_);_(@_)";
                                }
                                else
                                {
                                    worksheet.Cells["N" + (i + 2)].Formula = "=IF(K" + (i + 1) + "=0,0,((K" + (i + 2) + "-K" + (i + 1) + ")-(L" + (i + 2) + "-M" + (i + 2) + "))/K" + (i + 1) + ")";
                                    worksheet.Cells["N" + (i + 2) + ":N" + (i + 2)].Style.Numberformat.Format = "0.00%";
                                }

                                i++;
                            }
                            using (var cells = worksheet.Cells["A1:N1"])
                            {
                                cells.Style.Font.Bold = true;
                                cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            }
                            using (var cells = worksheet.Cells[1, 1, peIrrList.Count + 1, 14])
                            {
                                cells.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                cells.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                cells.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                cells.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            }
                            worksheet.Cells.AutoFitColumns();
                            worksheet.View.FreezePanes(2, 1);
                            var given = Convert.ToDateTime(dateasof).AddMonths(-3);

                            var result =
                                given.Date
                                    .AddMonths(3 - (given.Month - 1) % 3)
                                    .AddDays(-given.Day);
                            PEPortfolioInvestmentsExport(fundId, conn, package, 0, dateasof, "Investments-Q" + ((Convert.ToDateTime(dateasof)).Month / 3) + "" + Convert.ToDateTime(dateasof).Year, peIrrList.Count, worksheet);
                            PEPortfolioInvestmentsExport(fundId, conn, package, 0, result.ToString("MM/dd/yyyy"), "Investments-Q" + (result.Month / 3) + "" + result.Year, peIrrList.Count - 1, worksheet);
                            //var givenPre = result.AddMonths(-3);

                            //var resultPre =
                            //    givenPre.Date
                            //        .AddMonths(3 - (givenPre.Month - 1) % 3)
                            //        .AddDays(-givenPre.Day);
                            //PEPortfolioInvestmentsExport(fundId, conn, package, 0, resultPre.ToString("MM/dd/yyyy"), "Investments-Q" + (resultPre.Month / 3) + "" + resultPre.Year, peIrrList.Count-2,worksheet);
                            //var givenPrePrePre = resultPre.AddMonths(-3);

                            //var resultPrePre =
                            //    givenPrePrePre.Date
                            //        .AddMonths(3 - (givenPrePrePre.Month - 1) % 3)
                            //        .AddDays(-givenPrePrePre.Day);
                            //PEPortfolioInvestmentsExport(fundId, conn, package, 0, resultPrePre.ToString("MM/dd/yyyy"), "Investments-Q" + (resultPrePre.Month / 3) + "" + resultPrePre.Year, peIrrList.Count-3,worksheet);
                            CreateIrrTvpiCashflows(conn, package, peIrrList, worksheet, (peIrrList.Where(x => x.RowNumber > 4).Count() + 5), fundId);
                            sFileName = sFileName.Replace("&", " and ");
                            //using (var imp = new Impersonation(impersonateDomain, impersonateUser, impersonatePass))
                            //{
                            package.SaveAs(new FileInfo(sFileName));
                            //}

                            logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PA Fund Excel saved to :-(" + count + " of " + total + ") " + sFileName.Replace("&", " and ")));
                            //if (peIrrList[0].Analyst != string.Empty)
                            //{
                            //    UploadToAzure(sFileName,fundId,dateasof);
                            //}
                        }
                        catch (Exception ex)
                        {
                            logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                            logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "(" + count + " of " + total + ") Failed for FundId :-" + fundId));

                        }
                    }
                    else
                        logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "(" + count + " of " + total + ") Failed for FundId :-" + fundId));
                    //return File(package.GetAsByteArray(), GlobalAppSettings.ExcelContentType, sFileName.Replace("/", "-").Replace(@"\", "-").Replace("&", " and "));

                }
            }
        }

        private static void CreateAllFolderToCopy()
        {
            //using (var imp = new Impersonation(impersonateDomain, impersonateUser, impersonatePass))
            //{
                if (Directory.Exists(excelFund))
                {
                    var getFiles = Directory.GetFiles(excelFund, "*.xlsx", SearchOption.AllDirectories).OrderByDescending(x => (new FileInfo(x.ToString()).Name).Replace("4Q -", "").Replace("3Q -", "").Replace("2Q -", "").Replace("1Q -", ""));
                    CreateFolder(excelFund + "All\\");
                    foreach (var file in getFiles)
                    {
                        string filename = new FileInfo(file).Name;
                        try
                        {
                            File.Copy(file, excelFund + "All\\" + filename);
                        }
                        catch (Exception ex)
                        {
                            logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                            logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "All folder excel copying filed for => " + filename));
                        }
                    }

                }
            //}
        }

        //private void UploadToAzure(string filePath,string fundId,string dateasof)
        //{

        //    try
        //    {
        //        CloudStorageAccount storageAccount = CloudStorageAccount.Parse(Settings.AzureConnectionString);
        //        logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PE Holding Excel Upload to Azure => " + targetFileName));

        //        if (File.Exists(filePath))
        //        {
        //            var targetFileName = new FileInfo(filePath).Name;
        //            //logger.Debug($"Uploading file for docid {file.DocId} to {targetFileName}...");
        //            CloudFileClient fileClient = storageAccount.CreateCloudFileClient();
        //            var share = fileClient.GetShareReference(Settings.UploadFolder);
        //            var cloudFile = share.GetRootDirectoryReference().GetFileReference(targetFileName);
        //            cloudFile.Properties.ContentDisposition = "attachment; filename=" + targetFileName;
        //            try
        //            {
        //                cloudFile.Delete();
        //            }
        //            catch
        //            {
        //            }
        //            cloudFile.UploadFromFile(filePath);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
        //        logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "PE Holding Excel Upload => Exception on Upload to Azure"));
        //    }
        //}


        public static void CreateFolder(string folder)
        {
            //using (var imp = new Impersonation(impersonateDomain, impersonateUser, impersonatePass))
            //{
                if (!Directory.Exists(folder))
                {
                    DirectorySecurity sec = new DirectorySecurity();// Directory.GetAccessControl(folder);
                    //logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Failed to "));
                    // Using this instead of the "Everyone" string means we work on non-English systems.
                    SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                    sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.FullControl | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                    //sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                    // Directory.SetAccessControl(folder, sec);
                    Directory.CreateDirectory(folder, sec);
                    Directory.SetAccessControl(folder, sec);
                }

            //}

        }
        static List<PortfolioHoldingInvestment> portfolioHoldingInvestments = new List<PortfolioHoldingInvestment>();
        public static void PEPortfolioInvestmentsExport(string fundId, IDbConnection connection, ExcelPackage package, int holdingID, string dateasof, string excelTab, int firstSheetBottomRow, ExcelWorksheet firstWorsheet)
        {

            portfolioHoldingInvestments.Clear();
            portfolioHoldingInvestments = connection.Query<PortfolioHoldingInvestment>("EXEC Website_PE_Fund_GetPortfoilioInvestmentExport @HoldingId,@DateasOf,@FundId", new
            {
                HoldingId = holdingID,
                DateasOf = dateasof,
                FundId = fundId
            }, commandTimeout: 700).AsList();
            if (portfolioHoldingInvestments.Count > 0)
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(excelTab);
                string dateasofCurr;
                string dateasofPrev;
                if (portfolioHoldingInvestments.Count > 0)
                {
                    dateasofCurr = portfolioHoldingInvestments.OrderByDescending(o => o.DateasofCurr).ToList()[0].DateasofCurr.ToString();
                    dateasofPrev = portfolioHoldingInvestments.OrderByDescending(o => o.DateasofPrev).ToList()[0].DateasofPrev.ToString();
                }
                else
                {
                    dateasofCurr = Convert.ToDateTime(dateasof).ToString();
                    DateTime prevQuarter = Convert.ToDateTime(dateasof).AddMonths(-3);
                    int prevQuarterDays = ((prevQuarter.Month == 3) || (prevQuarter.Month == 12)) ? 31 : 30;
                    dateasofPrev = new DateTime(prevQuarter.Year, prevQuarter.Month, prevQuarterDays).ToString();
                }
                worksheet.Cells["A1"].Value = "Company";
                worksheet.Cells["B1"].Value = "Industry";
                worksheet.Cells["C1"].Value = "Country";
                worksheet.Cells["D1"].Value = "Public/Private";
                worksheet.Cells["E1"].Value = "Date Invested";
                worksheet.Cells["F1"].Value = "Total Invested";
                worksheet.Cells["G1"].Value = "Current Cost";
                worksheet.Cells["H1"].Value = "Fair Value " + Convert.ToDateTime(dateasofPrev).ToString("MM/dd");
                worksheet.Cells["I1"].Value = "Fair Value " + Convert.ToDateTime(dateasofCurr).ToString("MM/dd");
                worksheet.Cells["J1"].Value = "Fair Value Difference";
                worksheet.Cells["K1"].Value = "Fair Value/Cost";
                worksheet.Cells["L1"].Value = "% Fair Value";
                worksheet.Cells["M1"].Value = "% Fair Value Difference";
                worksheet.Cells["N1"].Value = "% Total Invested Cost";
                worksheet.Cells["O1"].Value = "% Current Cost";
                for (int i = 0; i < portfolioHoldingInvestments.Count; i++)
                {
                    worksheet.Cells["A" + (i + 2)].Value = portfolioHoldingInvestments[i].Entity;
                    worksheet.Cells["B" + (i + 2)].Value = portfolioHoldingInvestments[i].Industry;
                    worksheet.Cells["C" + (i + 2)].Value = portfolioHoldingInvestments[i].Country;
                    worksheet.Cells["D" + (i + 2)].Value = portfolioHoldingInvestments[i].Private;
                    if (portfolioHoldingInvestments[i].InvestmentDate == string.Empty)
                        worksheet.Cells["E" + (i + 2)].Value = "";
                    else
                    {
                        worksheet.Cells["E" + (i + 2)].Value = Convert.ToDateTime(portfolioHoldingInvestments[i].InvestmentDate);
                        worksheet.Cells["E" + (i + 2)].Style.Numberformat.Format = "MM/DD/YYYY";
                    }
                    worksheet.Cells["F" + (i + 2)].Value = Convert.ToDecimal(portfolioHoldingInvestments[i].Invested.Replace(",", ""));
                    worksheet.Cells["F" + (i + 2)].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* " + "-" + "??_);_(@_)";
                    worksheet.Cells["G" + (i + 2)].Value = Convert.ToDecimal(portfolioHoldingInvestments[i].RemainingCost.Replace(",", ""));
                    worksheet.Cells["G" + (i + 2)].Style.Numberformat.Format = "_(* #,##0.00_);_(* (#,##0.00);_(* " + "-" + "??_);_(@_)";
                    if (portfolioHoldingInvestments[i].FairValuePrev.Replace(",", "") == "-")
                    {
                        worksheet.Cells["H" + (i + 2)].Value = portfolioHoldingInvestments[i].FairValuePrev.Replace(",", "");
                    }
                    else
                    {
                        worksheet.Cells["H" + (i + 2)].Value = Convert.ToDecimal(portfolioHoldingInvestments[i].FairValuePrev.Replace(",", ""));
                    }

                    if (portfolioHoldingInvestments[i].FairValue.Replace(",", "") == "-")
                    {
                        worksheet.Cells["I" + (i + 2)].Value = portfolioHoldingInvestments[i].FairValue.Replace(",", "");
                    }
                    else
                    {
                        worksheet.Cells["I" + (i + 2)].Value = Convert.ToDecimal(portfolioHoldingInvestments[i].FairValue.Replace(",", ""));
                    }

                    if (portfolioHoldingInvestments[i].FairValueDiff.Replace(",", "") == "-")
                    {
                        worksheet.Cells["J" + (i + 2)].Value = portfolioHoldingInvestments[i].FairValueDiff.Replace(",", "");
                    }
                    else
                    {
                        worksheet.Cells["J" + (i + 2)].Value = Convert.ToDecimal(portfolioHoldingInvestments[i].FairValueDiff.Replace(",", ""));
                    }
                    worksheet.Cells["K" + (i + 2)].Value = portfolioHoldingInvestments[i].moic;
                    worksheet.Cells["L" + (i + 2)].Value = Convert.ToDecimal(portfolioHoldingInvestments[i].TotalPercent.Replace("%", "")) / 100;
                    worksheet.Cells["M" + (i + 2)].Formula = "IF(AND(ISNUMBER(H" + (i + 2) + "),ISNUMBER(I" + (i + 2) + ")),(I" + (i + 2) + "-H" + (i + 2) + ")/H" + (i + 2) + ",0)";
                    worksheet.Cells["M" + (i + 2)].Style.Numberformat.Format = "0.00%";
                    worksheet.Cells["H" + (i + 2) + ":I" + (i + 2)].Style.Numberformat.Format = "#,##0";
                    //worksheet.Cells["G" + (i + 2)].Style.Numberformat.Format = "#,##0.00";
                    // worksheet.Cells["F" + (i + 2)].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells["L" + (i + 2)].Style.Numberformat.Format = "0.00%";
                    worksheet.Cells["E" + (i + 2) + ":K" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells["N" + (i + 2)].Formula = "=IFERROR(G" + (i + 2) + "/F" + (portfolioHoldingInvestments.Count + 2) + ",0)";
                    worksheet.Cells["N" + (i + 2)].Style.Numberformat.Format = "0.00%";
                    worksheet.Cells["O" + (i + 2)].Formula = "=IFERROR(G" + (i + 2) + "/G" + (portfolioHoldingInvestments.Count + 2) + ",0)";
                    worksheet.Cells["O" + (i + 2)].Style.Numberformat.Format = "0.00%";
                }
                worksheet.Cells["H" + (portfolioHoldingInvestments.Count + 2)].Formula = "=SUM(H2:H" + (portfolioHoldingInvestments.Count + 1) + ")";
                worksheet.Cells["I" + (portfolioHoldingInvestments.Count + 2)].Formula = "=SUM(I2:I" + (portfolioHoldingInvestments.Count + 1) + ")";
                worksheet.Cells["J" + (portfolioHoldingInvestments.Count + 2)].Formula = "=SUM(J2:J" + (portfolioHoldingInvestments.Count + 1) + ")";

                worksheet.Cells["L" + (portfolioHoldingInvestments.Count + 2)].Formula = "=SUM(L2:L" + (portfolioHoldingInvestments.Count + 1) + ")";
                worksheet.Cells["L" + (portfolioHoldingInvestments.Count + 2)].Style.Numberformat.Format = "0.00%";
                worksheet.Cells["A" + (portfolioHoldingInvestments.Count + 2)].Value = "Total";
                using (var cells = worksheet.Cells["A1:O1"])
                {
                    cells.Style.Font.Bold = true;
                    cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                }
                using (var cells = worksheet.Cells["A" + (portfolioHoldingInvestments.Count + 2) + ":O" + (portfolioHoldingInvestments.Count + 2)])
                {
                    cells.Style.Font.Bold = true;
                    //cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                }
                using (var cells = worksheet.Cells["H" + (portfolioHoldingInvestments.Count + 2) + ":J" + (portfolioHoldingInvestments.Count + 2)])
                {

                    cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                }
                using (var cells = worksheet.Cells[1, 1, portfolioHoldingInvestments.Count + 2, 13])
                {
                    cells.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    cells.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    cells.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    cells.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                }

                worksheet.Cells.AutoFitColumns();
                worksheet.View.FreezePanes(2, 1);

            }

        }

        private static void CreateIrrTvpiCashflows(IDbConnection conn, ExcelPackage package, List<PAIrrTvpi> peIrrList, ExcelWorksheet firstWorksheet, int firstsheetLast, string fundIds)
        {
            foreach (var lastfour in peIrrList.Where(x => x.RowNumber <= 4).OrderBy(x => x.RowNumber))
            {
                List<TransactionCashflow> peTrxn = conn.Query<TransactionCashflow>("EXEC Sync_PE_Admin_getFundCompositeIrrTvpiPATransaction @FundId,@Dateasof", new
                {
                    FundId = fundIds.Contains(",") ? fundIds : lastfour.FundId.ToString(),
                    Dateasof = lastfour.EffectiveDate
                }, commandTimeout: 600).AsList();
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(lastfour.EffectiveDate.ToString("yyyy-MM-dd"));
                worksheet.Cells["A1"].Value = "Contribution(A)";
                worksheet.Cells["B1"].Value = "Distribution(B)";
                worksheet.Cells["C1"].Value = "Cash Flows(B-A)";
                worksheet.Cells["D1"].Value = "Effective Date";
                worksheet.Cells["E1"].Value = "TransactionType";
                worksheet.Cells["F1"].Value = "TransactionTypeName";
                worksheet.Cells["H1"].Value = "Contribution :";
                worksheet.Cells["H2"].Value = "Distribution :";
                worksheet.Cells["H3"].Value = "Fair Value :";
                worksheet.Cells["H4"].Value = "Net IRR :";
                worksheet.Cells["H5"].Value = "TVPI :";
                worksheet.Cells["H6"].Value = "DPI :";

                worksheet.Cells["H8"].Value = "Quarterly Contribution :";
                worksheet.Cells["H9"].Value = "Quarterly Distribution :";
                //worksheet.Cells["H1"].Style.Font.Bold = false;
                //worksheet.Cells["H2"].Style.Font.Bold = false;
                //worksheet.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                for (int i = 0; i < peTrxn.Count; i++)
                {
                    worksheet.Cells["A" + (i + 2)].Value = peTrxn[i].Contribution;
                    worksheet.Cells["B" + (i + 2)].Value = peTrxn[i].Distribution;
                    worksheet.Cells["C" + (i + 2)].Formula = "=(B" + (i + 2) + "-A" + (i + 2) + ")";
                    worksheet.Cells["D" + (i + 2)].Value = peTrxn[i].EffectiveDate;
                    worksheet.Cells["E" + (i + 2)].Value = peTrxn[i].TransactionType;
                    worksheet.Cells["F" + (i + 2)].Value = peTrxn[i].TransactionTypeName;
                    worksheet.Cells["D" + (i + 2)].Style.Numberformat.Format = "MM/DD/YYYY";
                    worksheet.Cells["A" + (i + 2) + ":A" + (i + 2)].Style.Numberformat.Format = peTrxn[i].Contribution != 0 ? "$#,##0_);[Red]($#,##0)" : "_(* #,##0_);_(* (#,##0);_(* " + " -" + "_);_(@_)";
                    worksheet.Cells["B" + (i + 2) + ":B" + (i + 2)].Style.Numberformat.Format = peTrxn[i].Distribution != 0 ? "$#,##0_);[Red]($#,##0)" : "_(* #,##0_);_(* (#,##0);_(* " + " -" + "_);_(@_)";
                    worksheet.Cells["C" + (i + 2) + ":C" + (i + 2)].Style.Numberformat.Format = (peTrxn[i].Distribution - peTrxn[i].Contribution) != 0 ? "$#,##0_);[Red]($#,##0)" : "_(* #,##0_);_(* (#,##0);_(* " + " -" + "_);_(@_)";
                }
                worksheet.Cells["I1"].Formula = "=SUM(A2:A" + (peTrxn.Count + 1) + ")";
                worksheet.Cells["I2"].Formula = "=SUM(B2:B" + (peTrxn.Count) + ")";
                worksheet.Cells["I3"].Formula = "=B" + (peTrxn.Count + 1);
                worksheet.Cells["I1"].Style.Numberformat.Format = peTrxn.Sum(x => x.Contribution) != 0 ? "$#,##0_);[Red]($#,##0)" : "_(* #,##0_);_(* (#,##0);_(* " + " -" + "_);_(@_)";
                worksheet.Cells["I2"].Style.Numberformat.Format = peTrxn.Sum(x => x.Distribution) != 0 ? "$#,##0_);[Red]($#,##0)" : "_(* #,##0_);_(* (#,##0);_(* " + " -" + "_);_(@_)";
                worksheet.Cells["I3"].Style.Numberformat.Format = "$#,##0_);[Red]($#,##0)";
                worksheet.Cells["I4"].Formula = "=ROUND(XIRR(C2:C" + (peTrxn.Count + 1) + ",D2:D" + (peTrxn.Count + 1) + ")*100,2)/100";
                worksheet.Cells["I4"].Style.Numberformat.Format = "0.00%";
                worksheet.Cells["I5"].Formula = "=ROUND((I2+I3)/I1,2)";
                worksheet.Cells["I6"].Formula = "=ROUND(I2/I1,2)";
                var given = Convert.ToDateTime(lastfour.EffectiveDate).AddMonths(-3);

                var resultPrevious =
                    given.Date
                        .AddMonths(3 - (given.Month - 1) % 3)
                        .AddDays(-given.Day);
                worksheet.Cells["I8"].Formula = "=SUMIF(D2:D" + (peTrxn.Count).ToString() + ",\">\"&DATE(" + resultPrevious.Year + "," + resultPrevious.Month + "," + resultPrevious.Day + "),A2:A" + (peTrxn.Count).ToString() + ")";
                worksheet.Cells["I8"].Style.Numberformat.Format = "$#,##0_);[Red]($#,##0)";
                //DateTime previousQuarter = ;
                worksheet.Cells["I9"].Formula = "=SUMIF(D2:D" + (peTrxn.Count).ToString() + ",\">\"&DATE(" + resultPrevious.Year + "," + resultPrevious.Month + "," + resultPrevious.Day + "),B2:B" + (peTrxn.Count).ToString() + ")";
                worksheet.Cells["I9"].Style.Numberformat.Format = "$#,##0_);[Red]($#,##0)";
                //First sheet Last 1 year all quarters  manual update
                firstWorksheet.Cells["A" + firstsheetLast].Value = lastfour.AssetClass;
                firstWorksheet.Cells["B" + firstsheetLast].Value = lastfour.Strategy;
                firstWorksheet.Cells["C" + firstsheetLast].Value = lastfour.Analyst;
                firstWorksheet.Cells["D" + firstsheetLast].Formula = "'" + lastfour.EffectiveDate.ToString("yyyy-MM-dd") + "'!I4";
                firstWorksheet.Cells["D" + firstsheetLast].Style.Numberformat.Format = "0.00%";
                firstWorksheet.Cells["E" + firstsheetLast].Formula = "=IF((D" + firstsheetLast + "-D" + (firstsheetLast - 1) + ")=0,0,(D" + firstsheetLast + "-D" + (firstsheetLast - 1) + "))";
                firstWorksheet.Cells["E" + firstsheetLast].Style.Numberformat.Format = "0.00%";
                firstWorksheet.Cells["F" + firstsheetLast].Formula = "'" + lastfour.EffectiveDate.ToString("yyyy-MM-dd") + "'!I5";

                firstWorksheet.Cells["F" + firstsheetLast].Style.Numberformat.Format = "0.00x";
                firstWorksheet.Cells["G" + firstsheetLast].Formula = "=IF((F" + firstsheetLast + "-F" + (firstsheetLast - 1) + ")=0,0,(F" + firstsheetLast + "-F" + (firstsheetLast - 1) + ")/(F" + firstsheetLast + "))/100";
                firstWorksheet.Cells["G" + firstsheetLast].Style.Numberformat.Format = "0.00%";

                firstWorksheet.Cells["H" + firstsheetLast].Formula = "'" + lastfour.EffectiveDate.ToString("yyyy-MM-dd") + "'!I6";

                firstWorksheet.Cells["H" + firstsheetLast].Style.Numberformat.Format = "0.00x";
                firstWorksheet.Cells["I" + firstsheetLast].Formula = "=IF((H" + firstsheetLast + "-H" + (firstsheetLast - 1) + ")=0,0,(H" + firstsheetLast + "-H" + (firstsheetLast - 1) + ")/(H" + firstsheetLast + "))/100";
                firstWorksheet.Cells["I" + firstsheetLast].Style.Numberformat.Format = "0.00%";

                firstWorksheet.Cells["J" + firstsheetLast].Value = lastfour.EffectiveDate;
                firstWorksheet.Cells["J" + firstsheetLast].Style.Numberformat.Format = "MM/DD/YYYY";
                firstWorksheet.Cells["K" + firstsheetLast].Formula = "'" + lastfour.EffectiveDate.ToString("yyyy-MM-dd") + "'!I3";
                firstWorksheet.Cells["K" + firstsheetLast].Style.Numberformat.Format = "$#,##0_);[Red]($#,##0)";
                firstWorksheet.Cells["L" + firstsheetLast].Formula = "'" + lastfour.EffectiveDate.ToString("yyyy-MM-dd") + "'!I8";
                firstWorksheet.Cells["L" + firstsheetLast].Style.Numberformat.Format = "$#,##0_);[Red]($#,##0)";
                // First sheet Update
                firstWorksheet.Cells["M" + firstsheetLast].Formula = "'" + lastfour.EffectiveDate.ToString("yyyy-MM-dd") + "'!I9";
                firstWorksheet.Cells["M" + firstsheetLast].Style.Numberformat.Format = "$#,##0_);[Red]($#,##0)";
                firstWorksheet.Cells["N" + firstsheetLast].Formula = "=IF(K" + (firstsheetLast - 1) + "=0,0,((K" + (firstsheetLast) + "-K" + (firstsheetLast - 1) + ")-(L" + (firstsheetLast) + "-M" + (firstsheetLast) + "))/K" + (firstsheetLast - 1) + ")";
                firstWorksheet.Cells["N" + firstsheetLast].Style.Numberformat.Format = "0.00%";
                firstsheetLast--;
                using (var cells = worksheet.Cells["A1:F1"])
                {
                    cells.Style.Font.Bold = true;
                    cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                }
                using (var cells = worksheet.Cells[1, 1, peTrxn.Count + 1, 6])
                {
                    cells.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    cells.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    cells.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    cells.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                }
                worksheet.Cells.AutoFitColumns();
                worksheet.View.FreezePanes(2, 1);
            }
        }
    }
}