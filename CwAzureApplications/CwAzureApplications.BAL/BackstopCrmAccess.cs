﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CwAzureApplications.WebService;
using CwAzureApplications.WebService.Service;
using CwAzureApplications.WebService.Entities;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using PrivateEquityReportCreation.ErrorLog;
using System.Xml.Linq;
using CwAzureApplications.DAL;



namespace CwAzureApplications.BAL
{

    public class BackstopCrmAccess
    {
        #region Variable Declarations
        private static readonly string spGetDocumentDetails = "usp_Get_4_DocumentInfo";      
        private static readonly string spSaveDocsXmlString = "usp_Save_4_DocsInfoSaveAsXML";
        private static readonly string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
        private static readonly string docBaseUrl = ConfigurationManager.AppSettings["DocumentBaseUrl"];
       // private static readonly string docBaseUrl = ConfigurationManager.AppSettings["DocumentBaseUrlTemp"];
        private static readonly string configReportRunTime = ConfigurationManager.AppSettings["DailyRunSchedule"];
        
        private static int backstopRetryCount = 0;
        private static Int64? DocId = 0;
        private static string _fileName = string.Empty;
        

        #endregion

        #region Object Declarations
        private static LogManager _logManagerEvent = new LogManager("Event");
        private static LogManager _logError = new LogManager("Error");
        #endregion

        public static void GetRecentDocDetails()
        {
            //Console.WriteLine("End Sub Called...");
            _logManagerEvent.Splitter();
            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Process Initiated to download the Document from Backstop"));
            DocId = 0;
            _fileName = string.Empty;
            List<BackstopCrmEntity> listOfDocsForUpload = new List<BackstopCrmEntity>();
            string runReportTime = DateTime.Now.ToString("hh:mm tt");
           // _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Start"));
            if (true)//Constants.isRunningFlag==false && runReportTime.ToLower() == configReportRunTime.ToLower())
            {              
                Constants.isRunningFlag = true;                        
                try
                {                    
                    SqlParameter[] paramsArray = { new SqlParameter("@DocID",0) ,                                                                                                                    
                                         };
                    int i = 1;
                    DataSet docDetailsDB = DataAccess.GetResultAsDataSet(paramsArray, spGetDocumentDetails,"azure", System.Reflection.MethodBase.GetCurrentMethod().Name+"-Previous History Data from DB");
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Get the Previously stored document details"));
                    List<object> docDetailsList = BackstopCrmService.GetServiceAuthentication(docDetailsDB.Tables[0]);

                    //foreach (var res in ((List<BackstopCrmEntity>)docDetailsList[0]))

                    if (docDetailsList.Count > 0 && docDetailsDB.Tables.Count > 0)
                    {
                        //HashSet<string> sentIDss = new HashSet<string>(files.Select(x => new Alphaleonis.Win32.Filesystem.FileInfo(x.ToString()).Name.ToLower()));
                        //HashSet<string> sentIDss = new HashSet<string>(files.Select(x => x.ToLower().ToString()));
                        //var result1 = docDetailsList.Where
                        //                (x => !sentIDss.Contains
                        //                    (Path.Combine(docBaseUrl + x.CreatedDate.Year + @"\" + x.CreatedDate.ToString("MMM") + @"\" + x.CreatedDate.Day + @"\")+
                        //                    x.FileName.ToLower().Replace("/", "-"))
                        // );
                        if (((List<BackstopCrmEntity>)docDetailsList[0]).Count>1 )
                        {
                            var listAllDocsInfo = ((List<BackstopCrmEntity>)docDetailsList[0]).Where(x => x.DocID == 0).ToList();

                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "The Recently Uploaded Count is:->" + ((List<BackstopCrmEntity>)docDetailsList[0]).Where(x => x.DocID != 0).Count().ToString()));

                            SqlParameter[] paramsArraySave = { new SqlParameter("@DocsInfo",listAllDocsInfo[0].xmlDocumentId) ,
                                            new SqlParameter("@DocsActivityInfo",listAllDocsInfo[0].xmlActivityTagsId),
                                            new SqlParameter("@DocsCompanyIdInfo",listAllDocsInfo[0].xmlCompanyId),   
                                            new SqlParameter("@DocsFundIdInfo",listAllDocsInfo[0].xmlFundId)                                             
                                         };
                           // var s = ;
                            DataAccess.CallProcedureForExecuteNonQuery(paramsArraySave, spSaveDocsXmlString,"azure", System.Reflection.MethodBase.GetCurrentMethod().Name);
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "The Document related details Saved into DB:->" + ((List<BackstopCrmEntity>)docDetailsList[0]).Where(x => x.DocID != 0).Count().ToString()));
                            var lnqdocCreate = ((List<BackstopCrmEntity>)docDetailsList[0]).Where(x => x.DocID != 0).ToList();
                            //foreach (var res in lnqdocCreate)
                            //{
                            //    //string currentDirectory = Path.Combine(docBaseUrl + res.CreatedDate.Year + @"\" + res.CreatedDate.ToString("MMM") + @"\" + res.CreatedDate.Day + @"\");
                            //    //if (!Directory.Exists(currentDirectory))
                            //    //    Directory.CreateDirectory(currentDirectory);                                
                            //    //FileCreation(res, res.FileName, currentDirectory);
                            //    FileCreationInList(res, res.FileName,i);
                            //    i++;
                            //}
                            //listOfDocsForUpload = lnqdocCreate;
                        }
                        else
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "No documents details recently updated"));


                    }
                    else
                        _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "No documents recently updated"));

                }

                catch (Exception ex)
                {
                    _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                    _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - The Document ID is -" + DocId + " and File Name is - " + _fileName + ""));

                }
                finally
                {
                    Constants.isRunningFlag = false;
                    _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "The Document Download process Completed..!"));
                    _logManagerEvent.Splitter();  
                }
            }
            AzureUploader.UploadAllFilesUsingBytes(listOfDocsForUpload);
        }


        private static void FileCreation(WebService.Entities.BackstopCrmEntity res, string filename, string currentDirectory)
        {
            try
            {
                if (!File.Exists(Path.Combine(currentDirectory + filename).Replace("\\", @"\")))
                {
                    var binary = BackstopCrmService.GetBinaryDocument(Convert.ToInt32(res.DocID));
                    

                    try
                    {
                        //This is for when File path is more than 260 characters - Default windows
                        using (Stream file = File.OpenWrite(Path.Combine(currentDirectory + filename).Replace("\\", @"\")))
                        {
                            file.Write(binary, 0, binary.Length);
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, " Downloaded Successfully(The file size is - " + Convert.ToInt64((binary.Length / 1024)) + " KB). The Doc ID is " + res.DocID + " and File Name is - " + res.FileNameOverride + ""));
                        }
                    }
                    catch (Exception ex)
                    {
                        //This is for when File path is more than 260 characters - Opensource library
                        using (Stream file = Alphaleonis.Win32.Filesystem.File.OpenWrite(Path.Combine(currentDirectory + filename).Replace("\\", @"\")))
                        {
                            file.Write(binary, 0, binary.Length);
                            _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Downloaded Successfully(The file size is - " + Convert.ToInt64((binary.Length / 1024)) + " KB - Due to File Path Too Long Error, App used AlphaFs Library) ->The Doc ID is " + res.DocID + " and File Name is - " + res.FileNameOverride + ""));
                        }

                    }
                    

                }
                backstopRetryCount = 0;
            }
            catch (TimeoutException tx)
            {
                if (backstopRetryCount <= 3)
                {
                    _logError.LogException(tx, System.Reflection.MethodBase.GetCurrentMethod().Name);
                    _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Timed out for the Document ID is - " + DocId + " and File Name is - " + res.FileNameOverride + " - Retry ->" + backstopRetryCount));
                    backstopRetryCount++;
                    FileCreation(res, filename, currentDirectory);
                    
                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - The Document ID is -" + DocId + " and File Name is - " + res.FileNameOverride + ""));
            }
        }
        private static void FileCreationInList(WebService.Entities.BackstopCrmEntity res, string filename,int count)
        {
            try
            {
                //if (!File.Exists(Path.Combine(currentDirectory + filename).Replace("\\", @"\")))
                //{
                var binary = BackstopCrmService.GetBinaryDocument(Convert.ToInt32(res.DocID));
                /* //This is for when File path is more than 260 characters - Default windows
                 using (Stream file = File.OpenWrite(Path.Combine(currentDirectory + filename).Replace("\\", @"\")))
                 {
                     file.Write(binary, 0, binary.Length);
                     _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, " Downloaded Successfully(The file size is - " + Convert.ToInt64((binary.Length / 1024)) + " KB). The Doc ID is " + res.DocID + " and File Name is - " + res.FileNameOverride + ""));
                 }*/
                res.DocBinary = binary;
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, " "+count+"- Downloaded Successfully(The file size is - " + Convert.ToInt64((binary.Length / 1024)) + " KB). The Doc ID is " + res.DocID + " and File Name is - " + res.FileNameOverride + ""));
                //}
                backstopRetryCount = 0;
            }
            catch (TimeoutException tx)
            {
                if (backstopRetryCount <= 3)
                {
                    _logError.LogException(tx, System.Reflection.MethodBase.GetCurrentMethod().Name);
                    _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Timed out for the Document ID is - " + DocId + " and File Name is - " + res.FileNameOverride + " - Retry ->" + backstopRetryCount));
                    backstopRetryCount++;
                    FileCreationInList(res, filename,count);

                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - The Document ID is -" + DocId + " and File Name is - " + res.FileNameOverride + ""));
            }
        }

    }
}
