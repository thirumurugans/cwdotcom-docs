﻿using CwAzureApplications.BAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UmbTrialBalanceUpload
{
    public class UmbTrialBalanceGenerate
    {
        public static void CallUmbTrialBalanceGenerate()
        {
            UmbTrialBalDataRawExcel.GenerateExcelwithUmbTrialBalData();
        }
    }
}
