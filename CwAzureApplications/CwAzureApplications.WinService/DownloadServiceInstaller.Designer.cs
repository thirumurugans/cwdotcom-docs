﻿namespace CwAzureApplications.WinService
{
    partial class DownloadServiceInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.downloadDocsserviceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.downloadDocsserviceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // downloadDocsserviceProcessInstaller
            // 
            this.downloadDocsserviceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.downloadDocsserviceProcessInstaller.Password = null;
            this.downloadDocsserviceProcessInstaller.Username = null;
            // 
            // downloadDocsserviceInstaller
            // 
            this.downloadDocsserviceInstaller.Description = "Funds Activity Documents gets  download from Backstop";
            this.downloadDocsserviceInstaller.DisplayName = "Cliffwater - Backstop - Download Document Service";
            this.downloadDocsserviceInstaller.ServiceName = "DownloadDocument";
            this.downloadDocsserviceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // DownloadServiceInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.downloadDocsserviceProcessInstaller,
            this.downloadDocsserviceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller downloadDocsserviceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller downloadDocsserviceInstaller;
    }
}