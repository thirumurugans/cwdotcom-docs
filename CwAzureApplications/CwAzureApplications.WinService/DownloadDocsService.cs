﻿//using CwAzureApplications.WebService.Entities;
using PrivateEquityReportCreation.ErrorLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace CwAzureApplications.WinService
{
    public partial class DownloadDocsService : ServiceBase
    {
        private System.Timers.Timer timer = null;
        
        private static readonly string configReportRunTime = ConfigurationManager.AppSettings["DailyRunSchedule"];
        private static readonly string configReportRunDay = ConfigurationManager.AppSettings["DailyRunScheduleDay"];
        private LogManager _logManagerEvent = new LogManager("Event");
        private LogManager _logError = new LogManager("Error");
        private DownloadDocsServiceObj download = null;
        public DownloadDocsService()
        {
            InitializeComponent();
            timer = new System.Timers.Timer();
            timer.Interval = Convert.ToInt64(ConfigurationManager.AppSettings["SchedulerTimeInterval"]);
            timer.Elapsed += new ElapsedEventHandler(TimerElapsed);
        }

        protected override void OnStart(string[] args)
        {
            //Constants.isRunningFlag = false;
            download = new DownloadDocsServiceObj();
            //download.GetDocuments();
            timer.Enabled = true;
            timer.Start();
        }
        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            timer.Stop();
            try
            {
                //string runReportTime = DateTime.Now.DayOfWeek.ToString() + "-" + DateTime.Now.ToString("hh:mm tt");
                var zone = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
                var utcNow = DateTime.UtcNow;
                var pacificNow = TimeZoneInfo.ConvertTimeFromUtc(utcNow, zone);
                string runReportTime = pacificNow.ToString("hh:mm tt");
                string runReportDay = pacificNow.DayOfWeek.ToString().ToLower();              
                //if (Constants.isRunningFlag == false && configReportRunDay.ToLower().Contains(runReportDay) && runReportTime.ToLower() == configReportRunTime.ToLower())
                    download.GetDocuments();
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            finally
            {
                timer.Start();
            }
        }

        protected override void OnStop()
        {
            this.timer.Dispose();
        }
    }
}
