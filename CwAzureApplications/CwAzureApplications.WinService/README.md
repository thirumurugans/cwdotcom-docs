
**Cliffwater - Backstop - Download Document Service**
=================
**Installed Path**
-------------  
@Azure VM Server as **"Task Scheduler"** in 
	
C:\Thiru\Sync Deploy\Docs Sync\Live\CwAzureApplications.WinService.exe

-------------  
**Log File Path**  
-------------  
@Azure VM Server in 

C:\Thiru\Logs\CW Docs Sync\Event Logs\
C:\Thiru\Logs\CW Docs Sync\Error Logs\

------------  
**Execution Frequency**
-------------  
Runs every weekday (Monday to Friday) at 05:00 PM PST (Tuesday to Saturday - 01:00 AM UST - Azure VM Time).
 
-------------  
**Procedures Called**
-------------  
**usp_Get_4_DocumentInfo** 

@Azure Server in "cw" database

BackstopCrmAccess.GetRecentDocDetails

**usp_Save_4_DocsInfoSaveAsXML** 

@Azure Server in "cw" database

BackstopCrmAccess.GetRecentDocDetails

**[PE].usp_Get_4_ActivityFromBackstop** 

@Azure VM Server in "coin" database

BackstopCrmAccessActivities.ActivityDetailsUpdates();

**usp_Save_4_ActivityInfoSaveAsXML** 

@Azure Server in "cw" database

BackstopCrmAccessActivities.ActivityDetailsUpdates();

**usp_Get_4_ActvityIdType** 

@Azure Server in "cw" database

BackstopCrmAccessActivities.ActivityDetailsUpdates();

**usp_Save_4_ActivityInfoRefresh** 

@Azure Server in "cw" database

BackstopCrmAccessActivities.ActivityDetailsRelationRefresh();

**usp_DashboardMaterializedViewRebuildSync** 

@Azure Server in "cw" database

BackstopCrmAccessActivities.DashboardDataRebuild();

**Website_Docs_GetDueDiligenceSiblingForMissing_Sync** 

@Azure Server in "cw" database

BackstopCrmAccessActivities.AdminMissingDocsDataRebuild();

**AzureUploader.UploadLiqAltAllFiles();** 

@Azure Server in "cw" database

SELECT FundId,FundName, NewFileName from vDocsManagerResearchFilters_LiqAlt ORDER BY FundName

**AzureUploader.UploadLiqAltDocsToAzure();** 

@Azure Server in "cw" database

INSERT INTO tblDocsLiqAlt(FundId,cLiqFileName,cAsOfDate) VALUES (@FundId,@LiqFileName,@AsOfDate)

**usp_AlertForNewProfileReport** 

@Azure Server in "cw" database

AzureUploader.EmailTriggerForNewDocsPrivateAssetsFocusList();
