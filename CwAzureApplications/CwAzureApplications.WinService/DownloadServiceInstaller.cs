﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace CwAzureApplications.WinService
{
    [RunInstaller(true)]
    public partial class DownloadServiceInstaller : System.Configuration.Install.Installer
    {
        public DownloadServiceInstaller()
        {
            InitializeComponent();
        }
    }
}
