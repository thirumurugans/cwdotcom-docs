
**Cliffwater - Backstop - PE Holding Data Sync**
=================
**Installed Path**
-------------  
@Azure VM Server as **"Task Scheduler"** in 
	
C:\Thiru\Sync Deploy\PE cw Fund Sync\Live\CwAzurePEHoldingDataSync.WinService.exe

-------------  
**Log File Path**  
-------------  
@Azure VM Server in 

C:\Thiru\Logs\CW PE Holding Data Sync\Event Logs\
C:\Thiru\Logs\CW PE Holding Data Sync\Error Logs\

------------  
**Execution Frequency**
-------------  
Runs every weekday (Monday to Friday) at 06:00 PM PST (Tuesday to Saturday - 02:00 AM UST - Azure VM Time).
 
-------------  
**Procedures Called**
-------------  
**PrivateEquityExcelIrr.CreateExcelWithIrr();** 

@Azure Server in "cw" database

SELECT DISTINCT ProductId,ProductName,AsOfDate DateAs FROM [dbo].[GenerateQuarterDateRange](3,default) ORDER BY ProductName ASC,AsOfDate DESC

**Sync_PE_sp_GetPortfolioTransactionForExcelIrrRebuildSync** 

@Azure Server in "cw" database

PrivateEquityExcelIrr.CreateExcel();

**PAFundIrrTvpiExcelBuild.SyncCreateExcelForIrr();** 

@Azure Server in "cw" database

SELECT * FROM vSync_PE_CompositeIrrTvpi  ORDER BY AsOfDate DESC,FundName ASC

**Sync_PE_sp_FundCompositeIrrTvpi** 

@Azure Server in "cw" database

PAFundIrrTvpiExcelBuild.SyncCreateExcelForIrr();

**Website_PE_Admin_GetFundForExcelIrrTvpi** 

@Azure Server in "cw" database

PAFundIrrTvpiExcelBuild.CreateFundIrrExcelFile();

**Website_PE_Fund_GetPortfoilioInvestmentExport** 

@Azure Server in "cw" database

PAFundIrrTvpiExcelBuild.PEPortfolioInvestmentsExport();

**Sync_PE_Admin_getFundCompositeIrrTvpiPATransaction** 

@Azure Server in "cw" database

PAFundIrrTvpiExcelBuild.CreateIrrTvpiCashflows();
