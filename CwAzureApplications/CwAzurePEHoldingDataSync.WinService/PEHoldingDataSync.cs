﻿using CwAzureApplications.BAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace CwAzurePEHoldingDataSync.WinService
{
    public partial class PEHoldingDataSync : ServiceBase
    {
        private System.Timers.Timer timer = null;
        PEHoldingDataSyncObj datasync = null;
        private static readonly string configReportRunTime = ConfigurationManager.AppSettings["DailyRunSchedule"];
        private static readonly string configReportRunDay = ConfigurationManager.AppSettings["DailyRunScheduleDay"];
        static JsonSerializeObject array = null;
        public PEHoldingDataSync()
        {
            InitializeComponent();
            timer = new System.Timers.Timer();
            timer.Interval = Convert.ToInt64(ConfigurationManager.AppSettings["SchedulerTimeInterval"]);
            timer.Elapsed += new ElapsedEventHandler(TimerElapsed);
        }

        protected override void OnStart(string[] args)
        {
            Settings.IsRunningFlag = false;
            datasync = new PEHoldingDataSyncObj();
            //download.GetDocuments();
            timer.Enabled = true;
            timer.Start();
            
        }
        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            timer.Stop();
            try
            {
                using (StreamReader file = File.OpenText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "Config.json")))
                {
                    array = JsonConvert.DeserializeObject<JsonSerializeObject>(file.ReadToEnd());
                }
                //string runReportTime = DateTime.Now.DayOfWeek.ToString() + "-" + DateTime.Now.ToString("hh:mm tt");
                var zone = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
                var utcNow = DateTime.UtcNow;
                var pacificNow = TimeZoneInfo.ConvertTimeFromUtc(utcNow, zone);
                string runReportTime = pacificNow.ToString("hh:mm tt");
                string runReportDay = pacificNow.DayOfWeek.ToString().ToLower();
                if (Settings.IsRunningFlag == false && configReportRunDay.ToLower().Contains(runReportDay) && runReportTime.ToLower() == configReportRunTime.ToLower() && array.IsDataSync == "true")
                {
                    Settings.IsRunningFlag = true;
                    datasync.HoldingDataSync();
                    Settings.IsRunningFlag = false;
                }
            }
            catch (Exception ex)
            {
               // _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                Settings.IsRunningFlag = false;
            }
            finally
            {
                timer.Start();
            }
        }
        protected override void OnStop()
        {
            
        }
    }
}
