﻿using CwAzureApplications.BAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CwAzurePEHoldingDataSync.WinService
{
    public class PEHoldingDataSyncObj
    {
        public void HoldingDataSync()
        {           
            ////BackstopHoldingDataSync.RunHoldingDataSync();

            PEHoldingBackstopSync.RunHoldingDataSync();
            ////PrivateEquityExcelIrr.BdcsWebsiteSync();
            PrivateEquityExcelIrr.CreateExcelWithIrr();
            PEHoldingBackstopSync.SaveBackstopCallSummaryLogs();
            PAFundIrrTvpiExcelBuild.SyncCreateExcelForIrr();
            ////PAFundIrrTvpiExcelBuild.CreateFundIrrExcelFile();
        }       
    }
}
