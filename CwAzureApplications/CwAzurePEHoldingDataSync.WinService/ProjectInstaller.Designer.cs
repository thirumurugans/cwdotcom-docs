﻿namespace CwAzurePEHoldingDataSync.WinService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DataSyncProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
            this.DataSyncInstaller1 = new System.ServiceProcess.ServiceInstaller();
            // 
            // DataSyncProcessInstaller1
            // 
            this.DataSyncProcessInstaller1.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.DataSyncProcessInstaller1.Password = null;
            this.DataSyncProcessInstaller1.Username = null;
            // 
            // DataSyncInstaller1
            // 
            this.DataSyncInstaller1.Description = "Private Equity Holding Backstop Data  Sync";
            this.DataSyncInstaller1.DisplayName = "Cliffwater - Backstop - PE Holding Data Sync";
            this.DataSyncInstaller1.ServiceName = "HoldingDataSync";
            this.DataSyncInstaller1.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.DataSyncProcessInstaller1,
            this.DataSyncInstaller1});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller DataSyncProcessInstaller1;
        private System.ServiceProcess.ServiceInstaller DataSyncInstaller1;
    }
}