﻿using CwAzureApplications.BAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace CwAzurePEHoldingFileUpload.WinService
{
    public partial class PeHoldingFileUpload : ServiceBase
    {
         private System.Timers.Timer timer = null;
        
        private static readonly string configReportRunTime = ConfigurationManager.AppSettings["DailyRunSchedule"];
        private static readonly string configReportRunDay = ConfigurationManager.AppSettings["DailyRunScheduleDay"];
        private readonly static string queuedFiles = ConfigurationManager.AppSettings["PEUploadQueuedPath"];
        
        public PeHoldingFileUpload()
        {
            InitializeComponent();
            timer = new System.Timers.Timer();
            timer.Interval = Convert.ToInt64(ConfigurationManager.AppSettings["SchedulerTimeInterval"]);
            timer.Elapsed += new ElapsedEventHandler(TimerElapsed);
        }
        

        protected override void OnStart(string[] args)
        {
            Settings.IsRunningFlag = false;
            //PeUploadHoldingFileToAzure.PeUploadHoldingMain();
            //download.GetDocuments();
            timer.Enabled = true;
            timer.Start();
        }
        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            timer.Stop();
            try
            {
                
                //string runReportTime = DateTime.Now.DayOfWeek.ToString() + "-" + DateTime.Now.ToString("hh:mm tt");
                string runReportTime = DateTime.Now.ToString("hh:mm tt");
                string runReportDay = DateTime.Now.DayOfWeek.ToString().ToLower();
                //DirectoryInfo info = new DirectoryInfo(queuedFiles);
               
                //List<string> excelFileExtensions = new List<string> { ".xlsx", ".xlsm" };
                //var allFiles = (from f in info.GetFiles() orderby f.CreationTime ascending select f).ToList();
                if (Settings.IsRunningFlag == false && configReportRunDay.ToLower().Contains(runReportDay))//&& allFiles.Count>0)//&& runReportTime.ToLower() == configReportRunTime.ToLower())
                {
                    Settings.IsRunningFlag = true;
                    PeHoldingFileUploadObj.UploadPeHoldings();
                    Settings.IsRunningFlag = false;
                }
            }
            catch (Exception ex)
            {
                // _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                Settings.IsRunningFlag = false;
            }
            finally
            {
                timer.Start();
            }
        }
        protected override void OnStop()
        {
        }
    }
}
