
**Cliffwater - Backstop - PE Holding Data Sync**
=================
**Installed Path**
-------------  
@Azure VM Server as **"Task Scheduler"** in 
	
C:\Thiru\Sync Deploy\PE cw Fund Sync\Live\CwAzurePEHoldingDataSync.WinService.exe

-------------  
**Log File Path**  
-------------  
@Azure VM Server in 

C:\Thiru\Logs\CW PE Holding Data Sync\Event Logs\
C:\Thiru\Logs\CW PE Holding Data Sync\Error Logs\

------------  
**Execution Frequency**
-------------  
Runs every weekday (Monday to Friday) at 06:00 PM PST (Tuesday to Saturday - 02:00 AM UST - Azure VM Time).
 
-------------  
**Procedures Called**
-------------  
 