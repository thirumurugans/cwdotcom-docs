﻿namespace CwAzurePEHoldingFileUpload.WinService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PeHoldingExcelUploadProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.PeHoldingExcelUploadInstaller1 = new System.ServiceProcess.ServiceInstaller();
            // 
            // PeHoldingExcelUploadProcessInstaller
            // 
            this.PeHoldingExcelUploadProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.PeHoldingExcelUploadProcessInstaller.Password = null;
            this.PeHoldingExcelUploadProcessInstaller.Username = null;
            // 
            // PeHoldingExcelUploadInstaller1
            // 
            this.PeHoldingExcelUploadInstaller1.Description = "Upload PE Excel data to Azure DB & Docs file to Azure, it picks from COIN2 Share " +
    "folder";
            this.PeHoldingExcelUploadInstaller1.DisplayName = "Cliffwater - Azure - PE Holding Excel Data Upload Sync";
            this.PeHoldingExcelUploadInstaller1.ServiceName = "PeHoldingExcelUpload";
            this.PeHoldingExcelUploadInstaller1.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.PeHoldingExcelUploadProcessInstaller,
            this.PeHoldingExcelUploadInstaller1});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller PeHoldingExcelUploadProcessInstaller;
        private System.ServiceProcess.ServiceInstaller PeHoldingExcelUploadInstaller1;
    }
}