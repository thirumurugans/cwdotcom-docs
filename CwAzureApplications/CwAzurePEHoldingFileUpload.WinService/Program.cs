﻿using CwAzureApplications.BAL;
using PrivateEquityReportCreation.ErrorLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace CwAzurePEHoldingFileUpload.WinService
{
    class Program
    {
        private readonly static LogManager logManagerEvent = new LogManager("Event");
        private readonly static LogManager logError = new LogManager("Error");
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static void Main(string[] args)
        {
            //ServiceBase[] ServicesToRun;
            //ServicesToRun = new ServiceBase[] 
            //{ 
            //    new PeHoldingFileUpload() 
            //};
            //ServiceBase.Run(ServicesToRun);           
            PeHoldingFileUploadObj.UploadPeHoldings();           
            //var files=Directory.GetFiles(@"\\cwcoin2\reports\Upload\Files\PE_Upload").Select(x=>x.ToString());
        }
    }
}
