﻿using CwAzureApplications.BAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CwAzurePEHoldingHistorySync.WinService
{
    public class PEHoldingHistoryObj
    {
        string str = "R:\\PA CompositeIrrTvpi\\";
        public void HoldingHistorySync()
        {
            //BackstopHoldingDataSync.RunHoldingHistorySync();
            //BackstopHoldingDataSync.RebuildOptmizationTable();
            //ExcelIrr.CreateExcelWithIrr();
            DeleteDirectory(str);
        }
        private static void DeleteDirectory(string targetdir)
        {
            //using (var imp = new Impersonation(impersonateDomain, impersonateUser, impersonatePass))
            //{

            try
            {
                string[] files = Directory.GetFiles(targetdir);
                string[] dirs = Directory.GetDirectories(targetdir);
                foreach (string file in files)
                {
                    try
                    {
                        File.SetAttributes(file, FileAttributes.Normal);
                        File.Delete(file);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }

                foreach (string dir in dirs)
                {
                    try
                    {
                        DeleteDirectory(dir);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }

                Directory.Delete(targetdir, false);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            // }
        }
    }
}
