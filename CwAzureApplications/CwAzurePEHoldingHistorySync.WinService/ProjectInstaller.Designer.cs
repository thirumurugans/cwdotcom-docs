﻿namespace CwAzurePEHoldingHistorySync.WinService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.historyDataSyncProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
            this.historyDataSyncInstaller1 = new System.ServiceProcess.ServiceInstaller();
            // 
            // historyDataSyncProcessInstaller1
            // 
            this.historyDataSyncProcessInstaller1.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.historyDataSyncProcessInstaller1.Password = null;
            this.historyDataSyncProcessInstaller1.Username = null;
            // 
            // historyDataSyncInstaller1
            // 
            this.historyDataSyncInstaller1.Description = "Private Equity Holding History Backstop Data  Sync";
            this.historyDataSyncInstaller1.DisplayName = "Cliffwater - Backstop - PE Holding History Sync";
            this.historyDataSyncInstaller1.ServiceName = "HoldingHistoryDataSync";
            this.historyDataSyncInstaller1.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.historyDataSyncProcessInstaller1,
            this.historyDataSyncInstaller1});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller historyDataSyncProcessInstaller1;
        private System.ServiceProcess.ServiceInstaller historyDataSyncInstaller1;
    }
}