﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using CwAzureApplications.BAL;
using System.Timers;
using System.IO;
using Newtonsoft.Json;

namespace CwAzurePEHoldingHistorySync.WinService
{
    public partial class PEHoldingHistorySync : ServiceBase
    {
         private System.Timers.Timer timer = null;
        PEHoldingHistoryObj datasync = null;
        private static readonly string configReportRunTime = ConfigurationManager.AppSettings["DailyRunSchedule"];
        private static readonly string configReportRunDay = ConfigurationManager.AppSettings["DailyRunScheduleDay"];
        static JsonSerializeObject array = null;
        public PEHoldingHistorySync()
        {
            InitializeComponent();
            timer = new System.Timers.Timer();
            timer.Interval = Convert.ToInt64(ConfigurationManager.AppSettings["SchedulerTimeInterval"]);
            timer.Elapsed += new ElapsedEventHandler(TimerElapsed);
        }

        protected override void OnStart(string[] args)
        {
            Settings.IsRunningFlag = false;
            datasync = new PEHoldingHistoryObj();
            //download.GetDocuments();
            timer.Enabled = true;
            timer.Start();
            
        }
        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            timer.Stop();
            try
            {
                using (StreamReader file = File.OpenText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "Config.json")))
                {
                    array = JsonConvert.DeserializeObject<JsonSerializeObject>(file.ReadToEnd());
                }
                //string runReportTime = DateTime.Now.DayOfWeek.ToString() + "-" + DateTime.Now.ToString("hh:mm tt");
                string runReportTime = DateTime.Now.ToString("hh:mm tt");
                string runReportDay = DateTime.Now.DayOfWeek.ToString().ToLower();
                if (Settings.IsRunningFlag == false && configReportRunDay.ToLower().Contains(runReportDay) && runReportTime.ToLower() == configReportRunTime.ToLower() && array.IsDataSync == "true")
                {
                    Settings.IsRunningFlag = true;
                    datasync.HoldingHistorySync();
                    Settings.IsRunningFlag = false;
                }
            }
            catch (Exception ex)
            {
               // _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                Settings.IsRunningFlag = false;
            }
            finally
            {
                timer.Start();
            }
        }
    }
}
