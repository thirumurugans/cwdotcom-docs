﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CwAzureApplications.BAL;

namespace CwNsjbFundDocs
{
    public partial class AllFundDocs : Form
    {
        public AllFundDocs()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AllFundDocsObj.CreateZip(Convert.ToInt32(txtProductId.Text));
        }
    }
}
