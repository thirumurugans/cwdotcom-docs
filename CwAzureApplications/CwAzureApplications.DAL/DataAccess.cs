﻿using PrivateEquityReportCreation.ErrorLog;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


namespace CwAzureApplications.DAL
{
    public class DataAccess
    {

        public static readonly string connectionStringAzure=ConfigurationManager.AppSettings["ConnectionString"];
        public static readonly string connectionStringCoin2 = ConfigurationManager.AppSettings["ConnectionStringCoin2"];
        static LogManager _logManagerEvent = new LogManager("Event");
        static LogManager _logError = new LogManager("Error");
        public static DataSet GetResultAsDataSet(SqlParameter[] paramsArray, string sPName, string dbConnection, string methodName = "", string logSpName = "")
        {
            using (DataSet dsColumns = new DataSet())
            {
                using (SqlConnection sqlConnection = new SqlConnection(dbConnection=="azure"?connectionStringAzure:connectionStringCoin2))
                {
                    try
                    {
                        sqlConnection.Open();
                        using (SqlCommand sqlCommand = new SqlCommand(sPName, sqlConnection))
                        {
                            sqlCommand.CommandType = CommandType.StoredProcedure;
                            sqlCommand.Parameters.AddRange(paramsArray);
                            sqlCommand.CommandTimeout = 1800000;
                            using (SqlDataAdapter dAdapter = new SqlDataAdapter(sqlCommand))
                            {
                                dAdapter.Fill(dsColumns);
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), methodName + " -> " + System.Reflection.MethodBase.GetCurrentMethod().Name, " Exception"));
                        _logError.LogException(exc, methodName + " -> " + System.Reflection.MethodBase.GetCurrentMethod().Name);
                        //CreateDbLog(methodName + " -> " + System.Reflection.MethodBase.GetCurrentMethod().Name);
                    }
                    finally
                    {
                        sqlConnection.Close();
                    }
                }
                return dsColumns;
            }
        }

        public static int CallProcedureForExecuteNonQuery(SqlParameter[] paramsArray, string sPName, string dbConnection, string methodName = "", string logSpName = "")
        {
            int result = 0;
            using (SqlConnection sqlConnection = new SqlConnection(dbConnection == "azure" ? connectionStringAzure : connectionStringCoin2))
            {
                try
                {
                    sqlConnection.Open();
                    using (SqlCommand sqlCommand = new SqlCommand(sPName, sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.CommandTimeout = 1800000;
                        sqlCommand.Parameters.AddRange(paramsArray);
                        result = sqlCommand.ExecuteNonQuery();

                    }
                }
                catch (Exception exc)
                {
                    _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), methodName + " -> " + System.Reflection.MethodBase.GetCurrentMethod().Name, " Exception"));
                    _logError.LogException(exc, methodName + " -> " + System.Reflection.MethodBase.GetCurrentMethod().Name);
                    //CreateDbLog(methodName + " -> " + System.Reflection.MethodBase.GetCurrentMethod().Name);

                }
                finally
                {
                    sqlConnection.Close();
                }
            }

            return result;


        }
       
    }
}
