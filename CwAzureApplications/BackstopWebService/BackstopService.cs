﻿using Dapper;
using PrivateEquityReportCreation.ErrorLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;


namespace BackstopWebService
{
    public class BackstopService
    {
        private static string ConnString = ConfigurationManager.AppSettings["ConnectionString"];
        private static string BACKSTOP_USERNAME = ConfigurationManager.AppSettings["UserName"];
        private static string BACKSTOP_PASSWORD = ConfigurationManager.AppSettings["Password"];
        private static string BACKSTOP_URL = ConfigurationManager.AppSettings["BackstopUrl"];
        private static LogManager _logManagerEvent = new LogManager("Event");
        private static LogManager _logError = new LogManager("Error");

        static BackstopPortfolioService.ComprehensiveHoldingBalanceInformation_1_4[] reports;
        public static void BackstopToDbForProduct()
        {
            string message = string.Empty;
            try
            {
                BackstopPortfolioService.BackstopPortfolioService_1_12 rs = new BackstopPortfolioService.BackstopPortfolioService_1_12();
                rs.Timeout = 36000000;
                rs.LoginInfo = new BackstopPortfolioService.LoginInfoType()
                {
                    Username = BACKSTOP_USERNAME,
                    Password = BACKSTOP_PASSWORD
                };
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Ssl3 | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;

                rs.Url = BACKSTOP_URL + new Uri(rs.Url).PathAndQuery;
                int i = 0;
                using (var conn = new SqlConnection(ConnString))
                {
                    //var SqlResult = conn.Query<dynamic>("SELECT PartnershipId,cInceptionDate InceptionDate,ProductId FROM tbl_PE_PartnershipMaster PM WHERE cInceptionDate!='01/01/1900' AND ProductId IN (SELECT ProductId FROM vtbl_BackstopDataSync) AND ProductId=1319717 AND PartnershipId IN (SELECT DISTINCT HoldingId FROM Cwcom_Sync_PE_tbl_PE_HoldingBalances WHERE cDate='09/30/2016')", commandTimeout: 3600000).AsList();
                    var SqlResult = conn.Query<dynamic>("SELECT MIN(cInceptionDate) InceptionDate,ProductId FROM tbl_PE_PartnershipMaster PM WHERE cInceptionDate!='01/01/1900' AND ProductId IN (SELECT ProductId FROM vtbl_BackstopDataSync) GROUP BY ProductId", commandTimeout: 3600000).AsList();

                    foreach (var item in SqlResult)
                    {
                        //var SqlResultTransaction = conn.Query<dynamic>("SELECT MAX(cEffectiveDate)  EffectiveDate FROM Cwcom_Sync_PE_tblHoldingTransactionSummary H, tbl_PE_PartnershipMaster P WHERE H.HoldingId=P.PartnershipId AND P.ProductId=@ProductId", new { ProductId = item.ProductId }, commandTimeout: 3600000);
                        var SqlResultTransaction = conn.Query<dynamic>("SELECT * FROM vBalanceReportedMaxDate WHERE ProductId=@ProductId", new { ProductId = item.ProductId }, commandTimeout: 3600000).ToList();
                        if (SqlResultTransaction != null)
                        {
                            reports = rs.getComprehensiveHoldingBalances(new int?[] { Convert.ToInt32(item.ProductId) }, Convert.ToDateTime(item.InceptionDate), SqlResultTransaction.Any() ? SqlResultTransaction.FirstOrDefault().EffectiveDate : Convert.ToDateTime("12/31/2017"), true, false, false);
                            DateTime endDate = SqlResultTransaction.Any() ? SqlResultTransaction.FirstOrDefault().EffectiveDate : Convert.ToDateTime("09/30/2020");
                            BackstopCallLogs("C# SOAP API- BackstopPortfolioService_1_12", "getComprehensiveHoldingBalances(login,startDate,endDate,bool)", "The Product Id is - " + Convert.ToInt32(item.ProductId) + ", StartDate - " + Convert.ToDateTime(item.InceptionDate) + ", EndDate is - " + endDate.ToShortDateString(), endDate, GetBytes(reports).Length, "C# Holding Balance", "Get the Holding balance details from Backstop for the Product of " + Convert.ToInt32(item.ProductId), Convert.ToInt32(item.ProductId));
                            if (reports.Count() > 0)
                            {
                                // conn.Execute("DELETE FROM Cwcom_Sync_PE_tbl_PE_HoldingBalances WHERE HoldingId=@HoldingId", new { HoldingId = Convert.ToInt32(item.PartnershipId) }, commandTimeout: 3600000);
                                var reportsSpecific = reports.OrderBy(x => x.holdingOpenedDate).ToList();
                                if (reportsSpecific.Count() > 0)
                                {
                                    //var date = reportsSpecific[0].holdingOpenedDate; //Give you own DateTime
                                    //int offset = 2, monthsInQtr = 3;

                                    //var quarter = (date.Month + offset) / monthsInQtr; //To find which quarter 
                                    //var totalMonths = quarter * monthsInQtr;
                                    //var endDateInQtr = new DateTime(date.Year, totalMonths, DateTime.DaysInMonth(date.Year, totalMonths));
                                    // conn.Execute("INSERT INTO Cwcom_Sync_PE_tbl_PE_HoldingBalances(HoldingId,cDate,BalanceAmount,PeriodForReturn) VALUES (@HoldingId,@Date,@BalanceAmount,@PeriodReturn)", new { HoldingId = reportsSpecific[0].holdingId, Date = endDateInQtr, BalanceAmount = reportsSpecific[0].amount, PeriodReturn = reportsSpecific[0].returnForPeriod });
                                    var xml = new XElement("Root", from x in reportsSpecific
                                                                   select new XElement("Holding",
                                                                              new XElement("HoldingId", x.holdingId),
                                                                              new XElement("BalanceDate", x.balanceDate),
                                                                              new XElement("HoldingAmount", x.amount),
                                                                              new XElement("ReturnForPeriod", x.returnForPeriod)

                                                                      ));
                                    conn.Query(@"EXEC Sync_PE_sp_SaveHoldingBalancesAsXML @xml", new { xml = xml.ToString() }, commandTimeout: 3600000);
                                    i++;
                                    //foreach (var Subitem in reportsSpecific)
                                    //{
                                    //    try
                                    //    {

                                    //        //conn.Execute("INSERT INTO Cwcom_Sync_PE_tbl_PE_HoldingBalances(HoldingId,cDate,BalanceAmount,PeriodForReturn) VALUES (@HoldingId,@Date,@BalanceAmount,@PeriodReturn)", new { HoldingId = Subitem.holdingId, Date = Subitem.balanceDate, BalanceAmount = Subitem.amount, PeriodReturn = Subitem.returnForPeriod },commandTimeout:3600000);
                                    //        conn.Query($"EXEC Sync_PE_sp_SaveHoldingBalancesAsXML {}", commandTimeout: 3600000);
                                    //    }
                                    //    catch (Exception ex)
                                    //    {

                                    //    }

                                    //}
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // message = ex.Message} - {ex.InnerException}";
                //i++
                // BackstopToDb();
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Preliminary Dates update to the DB Table"));

            }
            finally
            {
                //MessageBox.Show(message == string.Empty ? "Successfully Updated" : message);
            }
        }

        private static byte[] GetBytes(object obj)
        {
            byte[] bytes = null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                bytes = ms.ToArray();
            }
            return bytes;
        }
        public static void BackstopCallLogs(string ServerPath, string ServerMethod, string InputFilter, DateTime AsOfDate, int ByteLength, string ProcedureName, string Description, int ID)
        {
            using (var conn2 = new SqlConnection(ConfigurationManager.AppSettings["ConnectionStringCoin2"]))
            {
                conn2.Execute(@"EXEC Cwcom_Sync_usp_InsertBackstopCallLogs @ServerPath,@ServerMethod,@InputFilter,@InputJson,@AsOfDate,@ByteLength,@ProcedureName,@Description",
                                                                                               new
                                                                                               {
                                                                                                   ServerPath = ServerPath,
                                                                                                   ServerMethod = ServerMethod,
                                                                                                   InputFilter = InputFilter,
                                                                                                   InputJson = "",
                                                                                                   AsOfDate = AsOfDate.ToShortDateString(),
                                                                                                   ByteLength = ByteLength,
                                                                                                   ProcedureName = ProcedureName,
                                                                                                   Description = Description
                                                                                               });
                conn2.Execute(@"EXEC PE.Cwcom_Sync_usp_InsertLogs @ProcedureName,@Dateasof,@ID,@Status,@Description",
                                                                                          new
                                                                                          {
                                                                                              ProcedureName = ProcedureName,
                                                                                              Dateasof = AsOfDate,
                                                                                              ID = ID,
                                                                                              Status = "",
                                                                                              Description = Description
                                                                                          });
            }
        }
    }
}
