﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CwAzureApplications.WebService.Service
{
    public class BackstopAuthentication : IAuthenticator
    {
        private static readonly string userName = ConfigurationManager.AppSettings["UserName"];
        private static readonly string passWord = ConfigurationManager.AppSettings["Password"];
        public void Authenticate(IRestClient client, IRestRequest request)
        {
            
            
            var base64Token =
                Convert.ToBase64String(
                    Encoding.UTF8.GetBytes(userName + ":" + passWord.ToString()));
            var authHeader = "Basic " + base64Token;

            if (!IsHeaderSet(request, "Authorization"))
            {
                request.AddParameter("Authorization", authHeader, ParameterType.HttpHeader);
            }
            //if (ConnectionProperties.Token != null && !IsHeaderSet(request, "token"))
            //{
            //    request.AddParameter("token", "true", ParameterType.HttpHeader);
            //}
            if (!IsHeaderSet(request, "X-Backstop-Token-Purpose"))
            {
                request.AddParameter("X-Backstop-Token-Purpose", "API", ParameterType.HttpHeader);
            }
        }

        private static bool IsHeaderSet(IRestRequest request, string header)
        {
            return request.Parameters.Any(p => p.Name.Equals(header, StringComparison.OrdinalIgnoreCase));
        }
    }
}
