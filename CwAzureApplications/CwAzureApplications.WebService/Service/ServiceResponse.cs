﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace CwAzureApplications.WebService.Service
{
    public class ServiceResponse<T>
    {
        public HttpStatusCode StatusCode { get; set; }
        public string ErrorMessage { get; set; }
        public IDictionary<string, string> Headers { get; set; }
        public T Data { get; set; }
    }
}
