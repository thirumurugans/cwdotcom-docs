﻿using CwAzureApplications.WebService.BackstopCrm;
using CwAzureApplications.WebService.Entities;
using Newtonsoft.Json;
using PrivateEquityReportCreation.ErrorLog;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Xml.Linq;
using Microsoft.VisualBasic;
using Dapper;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace CwAzureApplications.WebService.Service
{
    public class BackstopCrmService
    {
        #region Variables
        private static readonly string serviceName = ConfigurationManager.AppSettings["ServiceName"];
        private static readonly string maxReceivedMsgSize = ConfigurationManager.AppSettings["MaxReceivedMsgSize"];
        private static readonly string maxBufferSize = ConfigurationManager.AppSettings["MaxBufferSize"];
        private static readonly string timeout = ConfigurationManager.AppSettings["Timeout"];
        private static readonly string userName = ConfigurationManager.AppSettings["UserName"];
        private static readonly string passWord = ConfigurationManager.AppSettings["Password"];
        private static readonly string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
        private static readonly string backstopUrl = ConfigurationManager.AppSettings["BackstopUrl"];
        private static int backstopRetryCount = 0;
        static LogManager _logManagerEvent = new LogManager("Event");
        static LogManager _logError = new LogManager("Error");
        #endregion

        #region Object Declaration
        public static BackstopCrmService_1_6PortTypeClient client = null;
        private static List<Entities.BackstopCrmEntity> docsDetails = null;
        private static List<object> docsTagsDetails = null;
        private static DocumentInformation[] docInfo = null;
        private static LoginInfoType login = null;

        #endregion

        #region "Webservice Call-Methods"
        public static List<object> GetServiceAuthentication(DataTable dTable)
        {
            try
            {
                ServiceCallMethod();
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Calling Backstop to get all Activity Tags Master Details..."));
               // var docInfo1 = client.getDocumentInformation(login, 67641647);
                docInfo = client.getAllDocuments(login);
                BackstopCallLogs("C# SOAP API- BackstopCrmService_1_6PortTypeClient", "getAllDocuments(login)", "Login", DateTime.Now, GetBytes(docInfo).Length, "C# All Docs", "Get all Docs Details from Backstop SOAP API, All Doc details Count is " + docInfo.Count(), 0);       
                //var tagsMaster = client.getAllActivityTags(login);
                //_logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Calling Backstop to get all Document Activity Tags Id Details..."));
               // var docactivitie=client.getAllDocumentActivityTags(login);
              //  var docis = docactivitie.ToList().Where(a => a.documentId == 68218999);
                var docActivity = client.getAllDocumentActivityTags(login);
                BackstopCallLogs("C# SOAP API- BackstopCrmService_1_6PortTypeClient", "getAllDocumentActivityTags(login)", "Login", DateTime.Now, GetBytes(docInfo).Length, "C# All Docs Activity", "Get all Docs Activity Tags Details from Backstop SOAP API, All Docs Activity details Count is " + docActivity.Count(), 0);       
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Calling Backstop to get all Document ID Details with File Name..."));

               
                var resultDocs = (from c in docInfo
                              join d in dTable.AsEnumerable().Select(x => new { DocID = x.Field<Int64?>("DocId") }) on c.id.Value equals d.DocID
                              into leftTable
                              from e in leftTable.DefaultIfEmpty()
                              select new
                              {
                                  c.id.Value,
                                  DocIdNull = e != null ? e.DocID : 0,
                                  filename = System.Net.HttpValidationHelpers.ContainsNonAsciiCharsDocs(c.filename.Replace("/", "-")),
                                  c.createdDate,
                                  c.author,
                                  c.title
                              }).Where(x => x.DocIdNull == 0).ToList();

                                 

                 docsDetails = new List<Entities.BackstopCrmEntity>();
                foreach (var res in resultDocs)
                {

                    int fileExtPos = res.filename.LastIndexOf(".");
                    string fileExtension = string.Empty;
                    if (fileExtPos >= 0)
                        fileExtension = res.filename.Substring(0, res.filename.LastIndexOf('.'));
                    string fileExtension1 = res.filename.Replace(fileExtension, "");
                   // string fileName = res.filename.Length > 150 ? (res.filename.Substring(0, 150) + res.Value) + fileExtension1 : res.filename;
                    string fileName = res.Value + fileExtension1;
                    docsDetails.Add(new Entities.BackstopCrmEntity()
                    {
                        DocID = res.Value,
                        FileName = fileName,
                        FileNameOverride=res.filename,
                        CreatedDate = res.createdDate,
                        Author = res.author,
                        Title = res.title

                    });
                }
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Client Entity Object mapping from Service entity..."));
                if (docsDetails.Any())
                {
                    // Document id,FileName Details
                    var xmlDocumentDetails = new XElement("Root",
                                                                       from doc in docsDetails
                                                                       select new XElement("Document",
                                                                                    new XElement("DocId", doc.DocID),
                                                                                      new XElement("Author", doc.Author),
                                                                                      new XElement("CreatedDate", doc.CreatedDate.ToString()),
                                                                                      new XElement("FileName", doc.FileName),                                                                                      
                                                                                      new XElement("Title", doc.Title),
                                                                                      new XElement("FileNameOverride", doc.FileNameOverride)
                                                                                  ));

                   
                    // Document ID ,ActivityTags Id
                    var queryActivityTagsXml = new XElement("Root", from res in
                                                                        ((from c in docActivity
                                                                          //join d in docsDetails on c.documentId equals d.DocID
                                                                          //into leftTable                                                                          
                                                                          select new
                                                                          {
                                                                              c.documentId,
                                                                              //DocIdNull = e != null ? e.DocID : 0,
                                                                              c.activityTagId
                                                                          }))//.Where(x => x.DocIdNull == 0))
                                                                    select new XElement("DocsActivityTagId",
                                                                       new XElement("DocId", res.documentId),
                                                                         new XElement("ActivityTagId", res.activityTagId)));

                    docsDetails.Add(new Entities.BackstopCrmEntity()
                    {
                        DocID = 0,
                        xmlFundId = "<Root/>",
                        xmlCompanyId = "<Root/>",
                        xmlActivityTagsId = queryActivityTagsXml.ToString(),
                        xmlDocumentId = xmlDocumentDetails.ToString()
                    });



                }

                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Completed->Client Entity Object mapping from Service Document Information & Activity Tags info entity"));
                //List<BackstopActivityTagsMaster> listTagsMaster = new List<BackstopActivityTagsMaster>();
                //foreach (var tags in tagsMaster)
                //{

                //    listTagsMaster.Add(new BackstopActivityTagsMaster() { ActvityTagsId = tags.id.Value, ActivityTagsName = tags.name });
                //    //SqlParameter[] paramsArray = { new SqlParameter("@ActivityTagId",tags.id.Value) ,
                //    //                                    new SqlParameter("@ActivityTagName",tags.name)                                                                                                                    
                //    //                     };
                //    //CallProcedureForExecuteNonQuery(paramsArray, "usp_Save_4_ActivityMasterDetails");
                //}
                docsTagsDetails = new List<object>();
                docsTagsDetails.Add(docsDetails);
                // docsTagsDetails.Add(listTagsMaster);
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Completed->Client Entity Object mapping from Service Activity Tags Master Details entity"));

                backstopRetryCount = 0;
            }
            catch (TimeoutException ex)
            {
                if (backstopRetryCount <= 3)
                {
                    backstopRetryCount++;
                    _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                    _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Timed out -> When fetch the data from Backstop->" + backstopRetryCount));
                    GetServiceAuthentication(dTable);


                }
            }
            catch (EndpointNotFoundException ex)
            {
                if (backstopRetryCount <= 3)
                {
                    backstopRetryCount++;
                    _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                    _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception - Timed out -> When fetch the data from Backstop->" + backstopRetryCount));
                    GetServiceAuthentication(dTable);


                }
            }
            catch (Exception ex)
            {
                _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception -> Backstop Service Call Exception->" + backstopRetryCount));
            }
            return docsTagsDetails;
        }

        public static void ServiceCallMethod()
        {
            var remoteAddress = new EndpointAddress(serviceName);
            var binding = new BasicHttpBinding(BasicHttpSecurityMode.Transport);
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Ssl3 | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
            binding.MaxReceivedMessageSize = int.Parse(maxReceivedMsgSize);
            binding.MaxBufferSize = int.Parse(maxBufferSize);
            binding.SendTimeout = new TimeSpan(0, int.Parse(timeout), 0);
            binding.ReceiveTimeout = new TimeSpan(0, int.Parse(timeout), 0);
            binding.MaxBufferPoolSize = int.Parse(maxBufferSize);
            client = new BackstopCrmService_1_6PortTypeClient(binding, remoteAddress);
            login = new LoginInfoType() { Username = userName, Password = passWord };
        }


        public static void GetActivityDescription()
        {
            ServiceCallMethod();
            var docsNotes = client.findNotesByDate(login, Convert.ToDateTime("06/30/2017"), Convert.ToDateTime("09/30/2017"));//.getAllNotes(login).Where(x => x.body != string.Empty && x.id == 11398527).ToList();
        }

        public static void GetActivityDescriptionDetails(DataTable dtActivity)
        {
            ServiceCallMethod();
            var docsNotes = client.getAllNotes(login).Where(x => x.body != string.Empty).ToList();//.Where(x => x.documentId == Convert.ToInt64("56047699")).ToList(); 

            BackstopCallLogs("C# SOAP API- BackstopCrmService_1_6PortTypeClient", "getAllNotes(login)", "Login", DateTime.Now, GetBytes(docsNotes).Length, "C# All Notes", "Get all notes from Backstop SOAP API, Note count is " + docsNotes.Count, 0);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string sqlIns = "UPDATE tbl4_AllActivityDetails SET cDescription=@Description,cIsDescUpdated=1 WHERE DocId=@ActivityId AND cType=@Type";
                SqlCommand cmdIns = new SqlCommand(sqlIns, connection);
                connection.Open();
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Notes HTML Description Update to DB"));
                foreach (var listNotes in docsNotes)
                {
                    try
                    {
                       // conn.Execute(@"EXEC Cwcom_Sync_usp_InsertBackstopCallLogs @ProcedureName,", new { ServerPath = "C# SOAP API- BackstopCrmService", ServerMethod = "GetBinaryDocument(DocId)", InputFilter = "The Doc ID is - " + docsDownFile.DocId, InputJson = "", AsOfDate = docsDownFile.CreatedDate, ID = docsDownFile.DocId, ByteLength = binary.Length, ProcedureName = "C# Method - UploadAllFilesUsingBytes()", Description = " Downloaded Successfully(The file size is - " + Convert.ToInt64((binary.Length / 1024)) + " KB). The Doc ID is " + docsDownFile.DocId + " and File Name is - " + docsDownFile.FileNameOverride + "" });
                       // conn.Execute(@"EXEC PE.Cwcom_Sync_usp_InsertLogs @ProcedureName,", new { ProcedureName = "C# Docs", Dateasof = docsDownFile.CreatedDate, ID = docsDownFile.DocId, Status = "", Description = "Downloaded Successfully(The file size is - " + Convert.ToInt64((binary.Length / 1024)) + " KB). The Doc ID is " + docsDownFile.DocId + " and File Name is - " + docsDownFile.FileNameOverride + "" });
                        cmdIns.Parameters.AddWithValue("@ActivityId", listNotes.id);
                        cmdIns.Parameters.AddWithValue("@Description", listNotes.body);
                        cmdIns.Parameters.AddWithValue("@Type", "Note");
                        cmdIns.CommandTimeout = 3600000;
                        cmdIns.ExecuteNonQuery();
                        cmdIns.Parameters.Clear();
                    }
                    catch(Exception ex)
                    {
                        _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                        _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception --> When fetching Notes HTML from Backstop Using Rest API, Doc Id->" + listNotes.id));
                    }
                }
                _logManagerEvent.LogInfoMessage(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Call/Meeting HTML Description Update to DB"));
                foreach (DataRow row in dtActivity.Rows)
                {
                    try
                    {
                        if (row["cType"].ToString() != "Document")
                        {
                            var docsMeeting = client.getMeetingInformation(login, new int?[] { Convert.ToInt32(row["ActivityId"]) }).ToList();
                            BackstopCallLogs("C# SOAP API- BackstopCrmService_1_6PortTypeClient", "getMeetingInformation(login," + Convert.ToInt32(row["ActivityId"]) + ")", "The Doc Id is - " + Convert.ToInt32(row["ActivityId"]), docsMeeting[0].createdDate, GetBytes(docsMeeting).Length, "C# " + row["cType"].ToString(), "Get the " + row["cType"].ToString() + " details from Backstop", Convert.ToInt32(row["ActivityId"]));
                            cmdIns.Parameters.AddWithValue("@ActivityId", docsMeeting[0].id);
                            cmdIns.Parameters.AddWithValue("@Description", docsMeeting[0].description);
                            cmdIns.Parameters.AddWithValue("@Type", Convert.ToString(row["cType"]));
                            cmdIns.CommandTimeout = 3600000;
                            cmdIns.ExecuteNonQuery();
                            cmdIns.Parameters.Clear();
                        }
                        else
                        {
                            var docsDetails = client.getDocumentInformation(login, Convert.ToInt32(row["ActivityId"]));
                            BackstopCallLogs("C# SOAP API- BackstopCrmService_1_6PortTypeClient", "getMeetingInformation(login," + Convert.ToInt32(row["ActivityId"]) + ")", "The Doc Id is - " + Convert.ToInt32(row["ActivityId"]), docsDetails.createdDate, GetBytes(docsDetails).Length, "C# " + row["cType"].ToString(), "Get the " + row["cType"].ToString() + " details from Backstop", Convert.ToInt32(row["ActivityId"]));
                            cmdIns.Parameters.AddWithValue("@ActivityId", docsDetails.id);
                            cmdIns.Parameters.AddWithValue("@Description", docsDetails.description);
                            cmdIns.Parameters.AddWithValue("@Type", "Document");
                            cmdIns.CommandTimeout = 3600000;
                            cmdIns.ExecuteNonQuery();
                            cmdIns.Parameters.Clear();

                        }
                    }
                    catch (Exception ex)
                    {
                         _logError.LogException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                        _logManagerEvent.LogException(string.Format("{0} - {1} - {2}", DateTime.Now.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception --> When fetching Call/Meeting HTML from Backstop Using Rest API, Doc Id->" + Convert.ToInt32(row["ActivityId"])));
                    }
                    finally
                    {
                       
                    }
                }
                connection.Close();
            }
        }

        

        public static byte[] GetBinaryDocumentWithServiceAccess(int docId)
        {            
            return GetBinaryDocument(docId);
        }
        public static byte[] GetBinaryDocument(int? docId)
        {
            var binary = client.getBinaryDocument(login, docId);
            return binary.documentBinary;
        }
        private static byte[] GetBytes(object obj)
        {
            byte[] bytes = null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                bytes = ms.ToArray();
            }
            return bytes;
        }
        public static void BackstopCallLogs(string ServerPath, string ServerMethod, string InputFilter, DateTime AsOfDate, int ByteLength, string ProcedureName, string Description, int ID)
        {
            using (var conn2 = new SqlConnection(ConfigurationManager.AppSettings["ConnectionStringCoin2"]))
            {
                conn2.Execute(@"EXEC Cwcom_Sync_usp_InsertBackstopCallLogs @ServerPath,@ServerMethod,@InputFilter,@InputJson,@AsOfDate,@ByteLength,@ProcedureName,@Description",
                                                                                               new
                                                                                               {
                                                                                                   ServerPath = ServerPath,
                                                                                                   ServerMethod = ServerMethod,
                                                                                                   InputFilter = InputFilter,
                                                                                                   InputJson = "",
                                                                                                   AsOfDate = AsOfDate.ToShortDateString(),
                                                                                                   ByteLength = ByteLength,
                                                                                                   ProcedureName = ProcedureName,
                                                                                                   Description = Description
                                                                                               });
                conn2.Execute(@"EXEC PE.Cwcom_Sync_usp_InsertLogs @ProcedureName,@Dateasof,@ID,@Status,@Description",
                                                                                          new
                                                                                          {
                                                                                              ProcedureName = ProcedureName,
                                                                                              Dateasof = AsOfDate,
                                                                                              ID = ID,
                                                                                              Status = "",
                                                                                              Description = Description
                                                                                          });
            }
        }

        private static RestClient _restClient;
        private static RestClient ServiceRestClient
        {
            get
            {
                _restClient.BaseUrl = new Uri(string.Concat(backstopUrl, '/', "backstop/rest"));
                return _restClient;
            }
            set { _restClient = value; }
        }

        protected static async Task<IRestResponse<T>> ExecuteTaskAsync<T>(IRestRequest request)
        {
            var client = ServiceRestClient;
            var restResponse = await client.ExecuteTaskAsync<T>(request);
            return restResponse;
        }

        private static async Task<ServiceResponse<BackstopDocument>> GetDocument(Int64? documentId)
        {
            ServiceResponse<BackstopDocument> serviceResponse=null;
            try
            {
                var request = new RestRequest("documents/" + documentId) { Method = Method.GET };
                request.AddHeader("Accept", "application/json; qs=.9");
                ServiceRestClient = new RestClient
                {
                    UserAgent = "BackstopSampleApp;Backstop-RequestSource=DocumentService",
                    Timeout = 360000,
                    Authenticator = new BackstopAuthentication()
                };
                var restResponse = await ExecuteTaskAsync<BackstopDocument>(request);
                var doc = JsonConvert.DeserializeObject<BackstopDocument>(restResponse.Content);
                serviceResponse = new ServiceResponse<BackstopDocument>
                {
                    StatusCode = restResponse.StatusCode,
                    ErrorMessage = restResponse.ErrorMessage,
                    Data = doc
                };

                
            }
            catch
            {
            }
            return serviceResponse;
        }
        #endregion
    }
}
