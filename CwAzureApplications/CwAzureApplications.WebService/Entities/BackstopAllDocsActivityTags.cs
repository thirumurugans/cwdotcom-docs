﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CwAzureApplications.WebService.Entities
{
    public class BackstopAllDocsActivityTags
    {
        public Int64? DocumentID { get; set; }
        public int? ActivityTagsId { get; set; }

    }
}
