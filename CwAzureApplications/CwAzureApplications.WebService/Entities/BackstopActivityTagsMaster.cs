﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CwAzureApplications.WebService.Entities
{
    public class BackstopActivityTagsMaster
    {
        public int? ActvityTagsId { get; set; }
        public string ActivityTagsName { get; set; }
    }
}
