﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CwAzureApplications.WebService.Entities
{
    public class BackstopDocsPartyRelation
    {
        public Int64? DocId { get; set; }
        public Int64? OtherId { get; set; }
        public string OtherType { get; set; }
    }
}
