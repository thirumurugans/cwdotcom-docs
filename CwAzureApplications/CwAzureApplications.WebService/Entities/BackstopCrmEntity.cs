﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CwAzureApplications.WebService.Entities
{
    public class BackstopCrmEntity
    {
        public Int64? DocID { get; set; }
        public byte[] DocBinary { get; set; }
        public string FileName { get; set; }
        public string FileNameOverride { get; set; }
        public string Author { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Title { get; set; }
        public string DocType { get; set; }
        public List<Link> LinkList { get; set; }
        public List<BackstopAllDocsActivityTags> DocTagsList { get; set; }
        public List<BackstopDocsPartyRelation> DocsPartyList { get; set; }
        public string xmlDocumentId { get; set; }
        public string xmlFundId { get; set; }
        public string xmlCompanyId { get; set; }
        public string xmlActivityTagsId { get; set; }
    }
}
