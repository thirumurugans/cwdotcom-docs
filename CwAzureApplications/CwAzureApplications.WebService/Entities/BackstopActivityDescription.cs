﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CwAzureApplications.WebService.Entities
{
    public class BackstopActivityDescription
    {
        public Int64? ActivityId { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
    }
}
