﻿using System.Text;
namespace System.Net
{
    public static class HttpValidationHelpers
    {
        public static string CheckBadHeaderNameChars(string name)
        {
            // First, check for absence of separators and spaces.
            if (IsInvalidMethodOrHeaderString(name))
            {
               // throw new ArgumentException(SR.net_WebHeaderInvalidHeaderChars, nameof(name));
            }

            // Second, check for non CTL ASCII-7 characters (32-126).
            if (ContainsNonAsciiChars(name))
            {
                //throw new ArgumentException(SR.net_WebHeaderInvalidHeaderChars, nameof(name));
            }
            return name;
        }

        public static string ContainsNonAsciiCharsDocs(string token)
        {
            StringBuilder _stringbuilder = new StringBuilder();
            for (int i = 0; i < token.Length; ++i)
            {
                if ((token[i] < 0x20) || (token[i] > 0x7e))
                {
                    _stringbuilder.Append("~");
                }
                else
                {
                    _stringbuilder.Append(token[i].ToString());
                }
            }
            return _stringbuilder.ToString();
        }
        public static bool ContainsNonAsciiChars(string token)
        {
            for (int i = 0; i < token.Length; ++i)
            {
                if ((token[i] < 0x20) || (token[i] > 0x7e))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsValidToken(string token)
        {
            if ((token.Length > 0)
               && !IsInvalidMethodOrHeaderString(token)
               && !ContainsNonAsciiChars(token))               
             return true;

            return false;
        }

        private static readonly char[] s_httpTrimCharacters = new char[] { (char)0x09, (char)0xA, (char)0xB, (char)0xC, (char)0xD, (char)0x20 };

        /// <summary>
        /// Throws on invalid header value chars.
        /// </summary>
        public static string CheckBadHeaderValueChars(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                // empty value is OK.
                return string.Empty;
            }

            // Trim spaces from both ends.
            value = value.Trim(s_httpTrimCharacters);

            // First, check for correctly formed multi-line value.
            // Second, check for absence of CTL characters.
            int crlf = 0;
            //StringBuilder stringbuilder = new StringBuilder();
            for (int i = 0; i < value.Length; ++i)
            {
                bool _bool = false;
                char c = (char)(0x000000ff & (uint)value[i]);
                switch (crlf)
                {
                    case 0:
                        if (c == '\r')
                        {
                            crlf = 1;
                            _bool = true;
                        }
                        else if (c == '\n')
                        {
                            // Technically this is bad HTTP, but we want to be permissive in what we accept.
                            // It is important to note that it would be a breaking change to reject this.
                            crlf = 2;
                            _bool = true;
                        }
                        else if (c == 127 || (c < ' ' && c != '\t'))
                        {
                            //throw new ArgumentException(SR.net_WebHeaderInvalidControlChars, nameof(value));
                            _bool = true;
                        }
                        break;

                    case 1:
                        if (c == '\n')
                        {
                            crlf = 2;
                            _bool = true;
                           
                        }
                        break;
                        //throw new ArgumentException(SR.net_WebHeaderInvalidCRLFChars, nameof(value));

                    case 2:
                        if (c == ' ' || c == '\t')
                        {
                            crlf = 0;
                            _bool = true;
                            
                        }
                        break;
                        //throw new ArgumentException(SR.net_WebHeaderInvalidControlChars, nameof(value));
                }

               
            }

            if (crlf != 0)
            {
               // throw new ArgumentException(SR.net_WebHeaderInvalidCRLFChars, nameof(value));
            }

            return "";
        }


        // Returns true if stringValue contains characters that cannot appear
        // in a valid method-verb or HTTP header.
        public static bool IsInvalidMethodOrHeaderString(string stringValue)
        {
            for (int i = 0; i < stringValue.Length; i++)
            {
                switch (stringValue[i])
                {
                    case '(':
                    case ')':
                    case '<':
                    case '>':
                    case '@':
                    case ',':
                    case ';':
                    case ':':
                    case '\\':
                    case '"':
                    case '\'':
                    case '/':
                    case '[':
                    case ']':
                    case '?':
                    case '=':
                    case '{':
                    case '}':
                    case ' ':
                    case '\t':
                    case '\r':
                    case '\n':
                    case '~':
                        return true;

                    default:
                        break;
                }
            }

            return false;
        }
    }
}